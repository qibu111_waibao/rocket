var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 所有窗口的基类，窗口在舞台的生命周期包括awake、openEffect、start、close、closeEffect、onDestroy这6个阶段
         * @author 袁浩
         *
         */
        var Window = (function (_super) {
            __extends(Window, _super);
            function Window() {
                var _this = _super.call(this) || this;
                _this.children = [];
                _this.delayChildrenCreated = true;
                return _this;
            }
            /**
             * 当添加到舞台时
             * @param event 事件对象
             */
            Window.prototype.onAddedToStage = function (event) {
                _super.prototype.onAddedToStage.call(this, event);
                how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_WINDOW_OPEN, this);
            };
            /**
             * 当从舞台移除时，推荐重写onDestroy
             * @param event 事件对象
             */
            Window.prototype.onRemovedFromStage = function (event) {
                this.removeCache();
                _super.prototype.onRemovedFromStage.call(this, event);
                how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_WINDOW_CLOSE, this);
            };
            /**
             * 当子项创建完成，推荐重写start
             */
            Window.prototype.childrenCreated = function () {
                this.awake();
                if (!this.openEffect() || !this.delayChildrenCreated) {
                    this.dispatchEventWith("childrenCreated");
                    this.start();
                }
            };
            Object.defineProperty(Window.prototype, "useFast", {
                get: function () {
                    return false;
                },
                enumerable: true,
                configurable: true
            });
            Window.prototype.addCache = function () {
                var _this = this;
                var sceneTexture = new egret.RenderTexture();
                sceneTexture.drawToTexture(how.Application["_guiLayer"]);
                this.sceneTexture = sceneTexture;
                how.Application["_app"].addChildAt(new egret.Bitmap(sceneTexture), 0);
                how.Application["_guiLayer"].visible = false;
                this.visible = false;
                egret.callLater(function () {
                    var cacheWindowTexture = Window.textureCache[egret.getQualifiedClassName(_this)];
                    _this.windowTexture = cacheWindowTexture;
                    if (!_this.windowTexture) {
                        var windowTexture = new egret.RenderTexture();
                        Window.textureCache[egret.getQualifiedClassName(_this)] = _this.windowTexture = windowTexture;
                        windowTexture.drawToTexture(_this, new egret.Rectangle(0, 0, parseInt(_this.width + ""), parseInt(_this.height + "")));
                    }
                    while (_this.numChildren) {
                        _this.children.push(_this.removeChildAt(0));
                    }
                    var windowBitmap = new egret.Bitmap(_this.windowTexture);
                    windowBitmap.anchorOffsetX = windowBitmap.width / 2;
                    windowBitmap.anchorOffsetY = windowBitmap.height / 2;
                    _this.addChild(windowBitmap);
                    egret.callLater(function () {
                        _this.visible = true;
                        _this.playOpenTwen();
                    }, _this);
                }, this);
            };
            Window.prototype.playOpenTwen = function () {
                this.scaleX = 0.9;
                this.scaleY = 0.9;
                this.alpha = 0.5;
                egret.Tween.get(this).
                    to({ scaleX: 1, scaleY: 1, alpha: 1 }, 100).
                    // to({ scaleX: 1.05, scaleY: 1.05 }, 100).
                    // to({ scaleX: 0.95, scaleY: 0.95 }, 100).
                    // to({ scaleX: 1, scaleY: 1 }, 100).
                    call(this.onOpenScaleComplete, this);
            };
            Window.prototype.removeCache = function () {
                if (this.sceneTexture) {
                    this.sceneTexture.dispose();
                    this.sceneTexture = null;
                }
                if (!how.Application["_guiLayer"].visible) {
                    how.Application["_app"].removeChildAt(0);
                    how.Application["_guiLayer"].visible = true;
                }
                if (this.useFast) {
                    this.removeChildren();
                    while (this.children.length) {
                        this.addChild(this.children.shift());
                    }
                }
            };
            Window.clearTextureCache = function () {
                // if(this.sceneTexture){
                //     this.sceneTexture.dispose();
                //     this.sceneTexture = null;
                // }
                for (var key in this.textureCache) {
                    var texture = this.textureCache[key];
                    texture.dispose();
                    delete this.textureCache[key];
                }
            };
            /**
            * 打开特效，子类可以继承并返回false来禁止播放打开特效
            * */
            Window.prototype.openEffect = function () {
                if (this.useFast) {
                    this.addCache();
                }
                else {
                    this.playOpenTwen();
                }
                // this.scaleX = 0.9;
                // this.scaleY = 0.9;
                // this.alpha = 1;
                // new how.behaviour.Tween(this, 200, {
                //     scaleY: 1, scaleX: 1, alpha: 1, onComplete: this.onOpenScaleComplete, thisObject: this
                // }).exec();
                return true;
            };
            /**
            * 关闭窗口
            * */
            Window.prototype.close = function () {
                if (!this.closeEffect()) {
                    how.Application.closeWindow(this);
                }
            };
            /**
             * 强制关闭窗口
             */
            Window.prototype.forceClose = function () {
                how.Application.closeWindow(this);
            };
            /**
            * 关闭特效，子类可以继承并返回null来禁止播放关闭特效
            * */
            Window.prototype.closeEffect = function () {
                egret.Tween.get(this).to({ scaleY: 0.9, scaleX: 0.9, alpha: 1 }, 200).call(this.onCloseScaleComplete, this);
                return true;
            };
            /**
             * 当打开特效完成
             */
            Window.prototype.onOpenScaleComplete = function () {
                var _this = this;
                egret.callLater(function () {
                    _this.alpha = _this.scaleX = _this.scaleY = 1;
                    if (_this.delayChildrenCreated) {
                        _this.dispatchEventWith("childrenCreated");
                        _this.start();
                    }
                }, this);
                this.removeCache();
            };
            /**
             * 当关闭特效完成
             */
            Window.prototype.onCloseScaleComplete = function () {
                how.Application.closeWindow(this);
            };
            return Window;
        }(module.View));
        Window.textureCache = {};
        module.Window = Window;
        __reflect(Window.prototype, "how.module.Window");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=Window.js.map