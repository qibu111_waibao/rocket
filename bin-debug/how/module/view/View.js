var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 界面
         * @author 袁浩
         *
         */
        var View = (function (_super) {
            __extends(View, _super);
            /**
             * 界面
             */
            function View() {
                var _this = _super.call(this) || this;
                /**
                 * 是否启动Update事件，默认关闭
                 */
                _this.useUpdate = false;
                /**
                 * 界面所需资源是否加载完成
                 */
                _this.isLoaded = false;
                /**
                 * 用于中间层验证，如果有视图中间层，请在其构造中把他的类路径push进来，否则会有个警告产生
                 * 例如： this.baseClasses.push("base.View")
                 */
                _this.baseClasses = ["how.module.Window", "how.module.Scene"];
                _this._allowReportChildren = egret.getDefinitionByName(egret.getQualifiedClassName(_this)).AllowReportChildren;
                _this.addEventListener(egret.Event.ADDED, _this.onChildAdded, _this);
                _this.addEventListener(egret.Event.REMOVED_FROM_STAGE, _this.onRemovedFromStage, _this);
                _this.setUpdate();
                _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddedToStage, _this);
                return _this;
            }
            Object.defineProperty(View.prototype, "allowReportChildren", {
                /**
                 * 允许项模块报告的子控件配置
                 * 需要手动定义一个名为AllowReportChildren的数组类型的静态属性
                 */
                get: function () {
                    return this._allowReportChildren;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(View.prototype, "resourceList", {
                /**
                * 子类重写此get函数来告诉底层本场景用到的资源组
                * 部分界面如果资源过多，需要等资源加载完成并显示到时候，就需要重写此属性并返回资源组
                * */
                get: function () {
                    return [];
                },
                enumerable: true,
                configurable: true
            });
            View.prototype.setUpdate = function () {
                if (this.useUpdate) {
                    this.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                    this.addEventListener(eui.UIEvent.RENDER, this.onRender, this);
                }
            };
            /**
             * 当添加到舞台时
             * @param event 事件对象
             */
            View.prototype.onAddedToStage = function (event) {
                this.stage.addEventListener(egret.Event.RESIZE, this.onStageResize, this);
                how.EventManager["getInstance"](this).on(how.Application.APPEVENT_BACK, this.onBack, this);
                how.EventManager["getInstance"](this).on(how.Application.APPEVENT_RESUME, this.onResume, this);
                how.EventManager["getInstance"](this).on(how.Application.APPEVENT_EXIT, this.onExit, this);
                this.removeEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
                if (this._allowReportChildren.length > 0) {
                    var children = this.findChildren(this);
                    for (var i = 0; i < children.length; i++) {
                        this.setChildReport(children[i]);
                    }
                }
            };
            View.prototype.onStageResize = function () {
            };
            View.prototype.onChildAdded = function (event) {
                this.setChildReport(event.target);
            };
            View.prototype.setChildReport = function (child) {
                var self = this;
                if (this.checkCanReportChildren(child)) {
                    var childRepport = function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        self.report.apply(this, args);
                    };
                    child["report"] = childRepport;
                }
            };
            /**
             * 检查子控件是否可以report
             * @param child 子控件
             */
            View.prototype.checkCanReportChildren = function (child) {
                for (var i = 0; i < this._allowReportChildren.length; i++) {
                    if (typeof this._allowReportChildren[i] == "string") {
                        this._allowReportChildren[i] = egret.getDefinitionByName(this._allowReportChildren[i]);
                    }
                    if (child instanceof this._allowReportChildren[i]) {
                        return true;
                    }
                }
                return false;
            };
            /**
             * 查找某个容器所有的子孙控件
             * @param parent 父容器
             * @param result 查询结果
             */
            View.prototype.findChildren = function (parent, result) {
                if (result === void 0) { result = []; }
                for (var i = 0; i < parent.numChildren; i++) {
                    var child = parent.getChildAt(i);
                    result.push(child);
                    if (egret.is(child, "egret.DisplayObjectContainer")) {
                        this.findChildren(child, result);
                    }
                }
                return result;
            };
            /**
             * 每次渲染都会调用
             */
            View.prototype.onRender = function () {
            };
            /**
             * 拉伸UI至铺满舞台
             */
            View.prototype.stretch = function () {
                this.left = this.right = this.top = this.bottom = 0;
            };
            View.prototype.onExit = function (event) {
                if (!this.onApplicationQuit()) {
                    event.preventDefault();
                }
            };
            View.prototype.onResume = function (event) {
                this.onApplicationFocus();
            };
            View.prototype.onPause = function (event) {
                this.onApplicationPause();
            };
            /**
             * 在应用退出之前调用，如果返回false可以阻止应用退出
             */
            View.prototype.onApplicationQuit = function () {
                return true;
            };
            /**
             * 应用得到焦点
             */
            View.prototype.onApplicationFocus = function () {
            };
            /**
             * 应用失去焦点
             */
            View.prototype.onApplicationPause = function () {
            };
            /**
             * 按下返回键
             */
            View.prototype.onBack = function () {
            };
            /**
            * 执行控制层的方法
            */
            View.prototype.report = function (request) {
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
            };
            /**
            * 脚本初始化完成
            * */
            View.prototype.awake = function () {
            };
            /**
             * 模块初始化完成
             */
            View.prototype.onModuleInit = function () {
            };
            View.prototype.onEnterFrame = function () {
                this.update();
            };
            /**
             * 每帧都会调用
             */
            View.prototype.update = function () {
            };
            /**
             * 当子项创建完成，推荐重写start
             */
            View.prototype.childrenCreated = function () {
                this.dispatchEventWith("childrenCreated");
                this.awake();
                this.start();
            };
            /**
             * 当从舞台移除时，推荐重写onDestroy
             * @param event 事件对象
             */
            View.prototype.onRemovedFromStage = function (event) {
                this.destroyResources();
                how.HttpManager.getInstance().cancelThis(this);
                this.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                egret.Tween.removeTweens(this);
                how.EventManager["getInstance"](this).off(how.Application.APPEVENT_BACK, this.onBack, this);
                how.EventManager["getInstance"](this).off(how.Application.APPEVENT_RESUME, this.onResume, this);
                how.EventManager["getInstance"](this).off(how.Application.APPEVENT_EXIT, this.onExit, this);
                this.stage.removeEventListener(egret.Event.RESIZE, this.onStageResize, this);
                this.onDestroy();
            };
            /**
             * 销毁所用资源，由resourceList指定
             */
            View.prototype.destroyResources = function () {
            };
            /**
            * 显示对象初始化完成
            */
            View.prototype.start = function () {
            };
            /**
             * 被移除的时候调用，可以当作析构函数使用，用来移除事件监听，清除引用等防止内存泄漏
             */
            View.prototype.onDestroy = function () {
            };
            return View;
        }(eui.Component));
        /**
         * 允许报告的子控件类型列表配置
         */
        View.AllowReportChildren = [];
        module.View = View;
        __reflect(View.prototype, "how.module.View", ["how.module.IBehaviour"]);
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=View.js.map