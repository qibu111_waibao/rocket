var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 所有场景的基类
         * @author 袁浩
         *
         */
        var Scene = (function (_super) {
            __extends(Scene, _super);
            /**
             * 场景界面
             */
            function Scene() {
                var _this = _super.call(this) || this;
                _this.left = _this.right = _this.top = _this.bottom = 0;
                return _this;
            }
            /**
             * 销毁所用资源，由resourceList指定-只有切换场景才做销毁操作
             */
            Scene.prototype.destroyResources = function () {
                // if (how.Utils.isIOSNative() || egret.Capabilities.isMobile && this.resourceList) {
                //     var resourceList: Array<string> = this.resourceList;
                //     for (var i: number = 0; i < resourceList.length; i++) {
                //         RES.destroyRes(resourceList[i], false);
                //     }
                // }
                // if (egret.Capabilities.isMobile && this.resourceList) {
                //     var resourceList: Array<string> = this.resourceList;
                //     for (var i: number = 0; i < resourceList.length; i++) {
                //         RES.destroyRes(resourceList[i], false);
                //     }
                // }
                // if (how.Utils.isIOSNative() || egret.Capabilities.isMobile) {
                //     RES.destroyRes("public", false);
                // }
            };
            return Scene;
        }(module.View));
        module.Scene = Scene;
        __reflect(Scene.prototype, "how.module.Scene");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=Scene.js.map