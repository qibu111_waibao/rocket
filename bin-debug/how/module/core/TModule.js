var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 带约束泛型的模块
         * @author 袁浩
         *
         */
        var TModule = (function (_super) {
            __extends(TModule, _super);
            /**
             * 模块
             * @param guiClass 视图类
             * @param dataClass 数据类
             */
            function TModule(guiClass, dataClass) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                return _super.call(this, guiClass, dataClass) || this;
            }
            Object.defineProperty(TModule.prototype, "tgui", {
                /**
                 * 视图对象，返回类型就是传入的类型V
                 */
                get: function () {
                    return this.gui;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TModule.prototype, "tdata", {
                /**
                 * 数据对象，返回类型就是传入的类型D
                 */
                get: function () {
                    return this.data;
                },
                enumerable: true,
                configurable: true
            });
            return TModule;
        }(module.Module));
        module.TModule = TModule;
        __reflect(TModule.prototype, "how.module.TModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=TModule.js.map