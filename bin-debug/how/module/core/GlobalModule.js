var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 全局模块，只能init一次，无法destroy
         * @author 袁浩
         *
         */
        var GlobalModule = (function (_super) {
            __extends(GlobalModule, _super);
            /**
             * 全局模块
             * 注意：不能直接new全局模块，必须通过how.HowMain中的模块管理器来初始化全局模块
             * @param guiClass 视图类
             * @param dataClass 数据类
             */
            function GlobalModule(guiClass, dataClass) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                var _this = _super.call(this, guiClass, dataClass) || this;
                var globalModule = GlobalModule.globalModules[egret.getQualifiedClassName(_this)];
                if (!globalModule) {
                    GlobalModule.globalModules[egret.getQualifiedClassName(_this)] = _this;
                }
                else {
                    error("全局模块 " + egret.getQualifiedClassName(_this) + " 只能初始化一次！");
                }
                return _this;
            }
            /**
             * 移除模块
             */
            GlobalModule.prototype.destroy = function () {
                error("全局模块 " + egret.getQualifiedClassName(this) + " 禁止销毁！");
            };
            /**
             * 根据类型获取模块
             * @param moduleClass 模块类路径
             */
            GlobalModule.getModule = function (moduleClass) {
                if (typeof (moduleClass) == "string") {
                    return GlobalModule.globalModules[moduleClass];
                }
                else {
                    return GlobalModule.globalModules[egret.getQualifiedClassName(moduleClass)];
                }
            };
            return GlobalModule;
        }(how.module.Module));
        GlobalModule.globalModules = {};
        module.GlobalModule = GlobalModule;
        __reflect(GlobalModule.prototype, "how.module.GlobalModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=GlobalModule.js.map