var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module_1) {
        /**
         * 所有模块的基类
         * @author 袁浩
         *
         */
        var Module = (function (_super) {
            __extends(Module, _super);
            /**
             * 模块
             * @param guiClass 视图类
             * @param dataClass 数据类
             */
            function Module(guiClass, dataClass) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                var _this = _super.call(this) || this;
                /**
                 * 模块标签，用来区分模块类型
                 */
                _this.tag = 0;
                _this._guiType = guiClass;
                _this._dataType = dataClass;
                _this._isGuiType = typeof (_this._guiType) != "object";
                _this._isDataType = typeof (_this._dataType) != "object";
                _this.copyRequest();
                _this.copyResponse();
                _this.eventManager.on(how.Application.APPEVENT_BACK, _this.onBack, _this);
                _this.eventManager.on(how.Application.APPEVENT_RESUME, _this.onResume, _this);
                _this.eventManager.on(how.Application.APPEVENT_EXIT, _this.onExit, _this);
                _this.awake();
                return _this;
            }
            Object.defineProperty(Module.prototype, "moduleManager", {
                /**
                 * 模块管理器
                 */
                get: function () {
                    return ModuleManager.getInstance();
                },
                enumerable: true,
                configurable: true
            });
            /**
             * 应答配置，key代表需要执行的方法，value代表类型
             * 事件的作用域是全局的，自动绑定全局事件监听（EventManager）的事件
             * 抛事件的data参数如果是数组则需要用[]包装一下，如果是普通对象可包装也可不包装
             * 子类需要创建个名为response的静态属性，在模块初始化的时候会拷贝父类的这个静态属性里面的值，就是说支持继承
             */
            Module.prototype.response = function () {
                return this._response;
            };
            /**
             * 请求配置，提供给视图层使用的，key代表需要执行的方法，value代表类型
             * 事件的作用域只在此模块，也就是说，不同的模块，可以相同的名为"test"的request而不冲突
             * 子类需要创建个名为request的静态属性，在模块初始化的时候会拷贝父类的这个静态属性里面的值，就是说支持继承
             */
            Module.prototype.request = function () {
                return this._request;
            };
            Object.defineProperty(Module.prototype, "dataType", {
                /**
                * 模块绑定的数据类型
                */
                get: function () {
                    return this._dataType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "guiType", {
                /**
                * 模块绑定的界面类型
                */
                get: function () {
                    return this._guiType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "gui", {
                /**
                 * 模块绑定的界面
                 */
                get: function () {
                    return this._gui;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "data", {
                /**
                 * 模块绑定的数据
                 */
                get: function () {
                    return this._data;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "isInit", {
                /**
                 * 是否初始化完成
                 */
                get: function () {
                    return this._isInit;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "isGuiType", {
                /**
                * 界面是通过类型创建的还是实例
                */
                get: function () {
                    return this._isGuiType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "isDataType", {
                /**
                * 数据是通过类型创建的还是实例
                */
                get: function () {
                    return this._isDataType;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "isDestroyed", {
                /**
                 * 是否已经被销毁
                 */
                get: function () {
                    return this._isDestroyed;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "eventManager", {
                /**
                 * 事件监听管理器
                 */
                get: function () {
                    return how.EventManager["getInstance"](this);
                },
                enumerable: true,
                configurable: true
            });
            /**
             * 拷贝请求
             */
            Module.prototype.copyRequest = function () {
                this._request = egret.getDefinitionByName(egret.getQualifiedClassName(this)).request;
                var parentClass = egret.getDefinitionByName(egret.getQualifiedSuperclassName(this));
                while (parentClass.request) {
                    this.copy(parentClass.request, this._request);
                    parentClass = egret.getDefinitionByName(egret.getQualifiedSuperclassName(parentClass));
                }
                egret.getDefinitionByName(egret.getQualifiedClassName(this)).request = this._request;
            };
            /**
             * 拷贝应答
             */
            Module.prototype.copyResponse = function () {
                this._response = egret.getDefinitionByName(egret.getQualifiedClassName(this)).response;
                var parentClass = egret.getDefinitionByName(egret.getQualifiedSuperclassName(this));
                while (parentClass.response) {
                    this.copy(parentClass.response, this._response);
                    parentClass = egret.getDefinitionByName(egret.getQualifiedSuperclassName(parentClass));
                }
                egret.getDefinitionByName(egret.getQualifiedClassName(this)).response = this._response;
            };
            /**
             * 拷贝数据到另一个对象中
             */
            Module.prototype.copy = function (source, target, override) {
                if (override === void 0) { override = false; }
                if (source && target) {
                    for (var key in source) {
                        if (target.hasOwnProperty(key) && override) {
                            target[key] = source[key];
                        }
                        else {
                            target[key] = source[key];
                        }
                    }
                }
            };
            Module.prototype.onExit = function (event) {
                if (!this.onApplicationQuit()) {
                    event.preventDefault();
                }
            };
            Module.prototype.onResume = function (event) {
                this.onApplicationFocus();
            };
            Module.prototype.onPause = function (event) {
                this.onApplicationPause();
            };
            /**
             * 在应用退出之前调用，如果返回false可以阻止应用退出
             */
            Module.prototype.onApplicationQuit = function () {
                return true;
            };
            /**
             * 应用得到焦点
             */
            Module.prototype.onApplicationFocus = function () {
            };
            /**
             * 应用失去焦点
             */
            Module.prototype.onApplicationPause = function () {
            };
            /**
             * 当按下返回键
             */
            Module.prototype.onBack = function () {
            };
            /**
             * 初始化界面
             */
            Module.prototype.initGUI = function () {
                this._gui = !this.isGuiType ? this._guiType : new this._guiType();
                var thisGui = this.gui;
                if (this._gui) {
                    if (this._gui.stage) {
                        this.onGUIComplete(null);
                    }
                    else {
                        this.gui.addEventListener(eui.UIEvent.CREATION_COMPLETE, this.guiAwake, this);
                        this._gui.addEventListener("childrenCreated", this.onGUIComplete, this);
                    }
                    this._gui.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onGUIRemovedFromStage, this);
                    var self = this;
                    this._gui["report"] = function (argRequest) {
                        var args = [];
                        for (var _i = 1; _i < arguments.length; _i++) {
                            args[_i - 1] = arguments[_i];
                        }
                        self._reportFun = self._reportFun || {};
                        var func = self._reportFun[argRequest];
                        if (self._request && !func) {
                            for (var key in self._request) {
                                var eventType = self._request[key];
                                if (eventType == argRequest) {
                                    func = self[key];
                                    if (!func) {
                                        error("模块 " + egret.getQualifiedClassName(self) + " 中不存在方法：" + self._request[key]);
                                    }
                                    else {
                                        self._reportFun[argRequest] = func;
                                        func.apply(self, args);
                                    }
                                    return;
                                }
                            }
                            error("模块 " + egret.getQualifiedClassName(self) + " 中不存在键值：" + argRequest);
                        }
                        else {
                            func.apply(self, args);
                        }
                    };
                }
            };
            /**
             * 发送全局事件监听和模块之间的通讯
             */
            Module.prototype.communicate = function (response) {
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
                how.EventManager["getInstance"](this).dispatchEvent(response, { ".isCommunicate": true, communicateData: args });
            };
            /**
             * 当界面初始化完成，推荐重写guiStart，不推荐重写此方法
             * @param event
             */
            Module.prototype.onGUIComplete = function (event) {
                if (!this._isGUICompleted) {
                    this.guiStart();
                    this._isGUICompleted = true;
                }
            };
            /**
             * 当视图从舞台移除，推荐重写onDestroy，不推荐重写此方法
             * @param event
             */
            Module.prototype.onGUIRemovedFromStage = function (event) {
                this.dispatchEventWith(egret.Event.CLOSE);
                event.currentTarget.removeEventListener("childrenCreated", this.onGUIComplete, this);
                event.currentTarget.removeEventListener(egret.Event.REMOVED_FROM_STAGE, this.onGUIRemovedFromStage, this);
                this.destroy();
            };
            /**
             * gui初始化完成
             */
            Module.prototype.guiAwake = function () {
            };
            /**
             * gui显示完成
             */
            Module.prototype.guiStart = function () {
            };
            /**
             * 初始化数据
             */
            Module.prototype.initData = function () {
                this._data = this._data || this._dataType ? this.newData() : null;
                if (this._data) {
                    var self = this;
                    this._data["report"] = function (argRequest) {
                        var args = [];
                        for (var _i = 1; _i < arguments.length; _i++) {
                            args[_i - 1] = arguments[_i];
                        }
                        self._reportFun = self._reportFun || {};
                        var func = self._reportFun[argRequest];
                        if (self._request && !func) {
                            for (var key in self._request) {
                                var eventType = self._request[key];
                                if (eventType == argRequest) {
                                    func = self[key];
                                    if (!func) {
                                        error("模块 " + egret.getQualifiedClassName(self) + " 中不存在方法：" + self._request[key]);
                                    }
                                    else {
                                        self._reportFun[argRequest] = func;
                                        func.apply(self, args);
                                    }
                                    return;
                                }
                            }
                            error("模块 " + egret.getQualifiedClassName(self) + " 中不存在键值：" + argRequest);
                        }
                        else {
                            func.apply(self, args);
                        }
                    };
                }
            };
            Module.prototype.newData = function () {
                if (!this.isDataType) {
                    return this._dataType;
                }
                else if (this._dataType) {
                    if (this._dataType.hasOwnProperty("getInstance")) {
                        return this._dataType.getInstance();
                    }
                    else {
                        return new this._dataType();
                    }
                }
            };
            /**
            * 初始化模块，如果不是通过模块管理器来初始化的，那么不会加入模块列表中
            */
            Module.prototype.init = function () {
                if (!this._isInit) {
                    this._isInit = true;
                    this.initGUI();
                    this.initData();
                    this.initResponse();
                    if (this._gui) {
                        this._gui.onModuleInit();
                    }
                    this.initComplete();
                    this.start();
                }
                else {
                    error("模块[" + this.hashCode + "]只能初始化一次");
                }
            };
            /**
             * 当视图和数据初始化完成，在start之前执行，在awake之后执行
             */
            Module.prototype.initComplete = function () {
            };
            /**
             * 初始化应答
             */
            Module.prototype.initResponse = function () {
                var self = this;
                if (this._response) {
                    for (var key in this._response) {
                        var eventType = this._response[key];
                        var func = this[key];
                        if (!func) {
                            error("模块" + egret.getQualifiedClassName(this) + " [" + this.hashCode + "] 中不存在方法：" + key);
                        }
                        var listener = function (event) {
                            var responseKey = this.getResponseKey(event.type);
                            if (responseKey) {
                                if (event.data && event.data[".isCommunicate"]) {
                                    var isArray = Object.prototype.toString.call(event.data.communicateData) === '[object Array]';
                                    var data = isArray ? event.data.communicateData : [event.data.communicateData];
                                    this[responseKey].apply(self, data);
                                }
                                else {
                                    this[responseKey].apply(self, [event.data]);
                                }
                            }
                        };
                        this[this.hashCode + "_" + key] = listener;
                        this.eventManager.on(eventType, listener, this);
                    }
                }
            };
            Module.prototype.getResponseKey = function (value) {
                for (var key in this._response) {
                    if (this._response[key] == value) {
                        return key;
                    }
                }
                return null;
            };
            Module.prototype.getRequestKey = function (value) {
                for (var key in this._request) {
                    if (this._request[key] == value) {
                        return key;
                    }
                }
                return null;
            };
            /**
             * 移除模块
             */
            Module.prototype.destroy = function () {
                if (this._isDestroyed) {
                    return;
                }
                this._isGUICompleted = false;
                this.onDestroy();
                this.eventManager.off(how.Application.APPEVENT_BACK, this.onBack, this);
                this.eventManager.off(how.Application.APPEVENT_RESUME, this.onResume, this);
                this.eventManager.off(how.Application.APPEVENT_EXIT, this.onExit, this);
                // this.pomelo_offAll();
                if (this._response) {
                    for (var key in this._response) {
                        var eventType = this._response[key];
                        var listener = this[this.hashCode + "_" + key];
                        if (!this[key]) {
                            error("模块" + egret.getQualifiedClassName(this) + " [" + this.hashCode + "] 中不存在方法：" + this._response[key]);
                        }
                        else {
                            this.eventManager.off(eventType, listener, this);
                        }
                    }
                }
                if (this._request) {
                    for (var key in this._request) {
                        var eventType = this._request[key];
                        if (!this[key]) {
                            error("模块" + egret.getQualifiedClassName(this) + " [" + this.hashCode + "] 中不存在方法：" + this._request[key]);
                        }
                        if (this._reportFun && this._reportFun[eventType]) {
                            this._reportFun[eventType] = null;
                            delete this._reportFun[eventType];
                        }
                    }
                }
                this._reportFun = null;
                this._gui = null;
                this._data = null;
                this._guiType = null;
                this._dataType = null;
                this._isDestroyed = true;
            };
            /**
            * 模块没有帧刷新事件
            */
            Module.prototype.update = function () {
            };
            /**
             * 模块没有渲染事件
             */
            Module.prototype.onRender = function () {
            };
            /**
             * 创建完成的时候调用，调用顺序是在构造函数之后start之前
             */
            Module.prototype.awake = function () {
            };
            /**
             * 子项全部创建完成的时候调用，可以当作是入口函数使用
             */
            Module.prototype.start = function () {
            };
            /**
             * 被移除的时候调用，可以当作析构函数使用，用来移除事件监听，清除引用等防止内存泄漏
             * 注意：在模块系统，只要不使用全局监听，基本不需要操心内存释放
             */
            Module.prototype.onDestroy = function () {
            };
            /**
             * 通知数据更新
             */
            Module.prototype.callData = function (funcName) {
                var sources = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    sources[_i - 1] = arguments[_i];
                }
                // if (sources.length == 0) {
                //     warn(egret.getQualifiedClassName(this) + ".callData(" + funcName + ")没有传递参数，可能会导致异常。");
                // }
                if (!this.data) {
                    warn(egret.getQualifiedClassName(this) + "执行了callData(\"" + funcName + "\")，但是数据已经为空！");
                    return;
                }
                var func = this.data[funcName];
                if (func) {
                    return func.apply(this.data, sources);
                }
                else {
                    error(egret.getQualifiedClassName(this) + "执行了callData(\"" + funcName + "\")，但是方法不存在！");
                }
            };
            /**
             * 通知界面更新
             * @param funcName 视图对象中的方法名，不存在也不会报错
             * @param sources 传递的参数
             */
            Module.prototype.callUI = function (funcName) {
                var sources = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    sources[_i - 1] = arguments[_i];
                }
                var func = this.gui[funcName];
                if (func) {
                    return func.apply(this.gui, sources);
                }
            };
            /**
             * 通知界面根据数据更新
             * 注：以数据对象为参数
             * @param funcName 视图对象中的方法名，不存在也不会报错
             */
            Module.prototype.callUIByData = function (funcName) {
                var func = this.gui[funcName];
                if (func) {
                    return func.call(this.gui, this.data);
                }
            };
            /**
             * 自定义界面和数据实例来初始化
             * @param gui
             * @param data
             */
            Module.prototype.initByGUIAndData = function (gui, data) {
                this._data = data;
                this._gui = gui;
            };
            Object.defineProperty(Module.prototype, "socket", {
                /**
                 * 网络套接字对象
                 */
                get: function () {
                    return how.WebSocketManager["getInstance"]();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "http", {
                /**
                 * 超文本传输协议请求对象
                 */
                get: function () {
                    return how.HttpManager["getInstance"]();
                },
                enumerable: true,
                configurable: true
            });
            return Module;
        }(egret.EventDispatcher));
        module_1.Module = Module;
        __reflect(Module.prototype, "how.module.Module", ["how.module.IBehaviour"]);
        /**
         * 模块管理器
         */
        var ModuleManager = (function () {
            /**
             * 模块管理器
             */
            function ModuleManager() {
                this.maxModuleListLength = 5;
                this.modules = {};
                this.moduleTypes = {};
            }
            /**
             * 获取模块管理器单例
             */
            ModuleManager.getInstance = function () {
                if (!this._instance) {
                    this._instance = new ModuleManager();
                }
                return this._instance;
            };
            /**
             * 通过模块管理器初始化的模块会加入到模块列表中，可以拥有先后顺序
             * @param moduleClass 模块类
             * @param guiClass 视图类
             * @param dataClass 数据类
             */
            ModuleManager.prototype.initModule = function (moduleClass, guiClass, dataClass, args, onGUIComplete, thisObject) {
                var _this = this;
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                if (args === void 0) { args = null; }
                if (onGUIComplete === void 0) { onGUIComplete = null; }
                if (thisObject === void 0) { thisObject = null; }
                var module = new moduleClass(guiClass, dataClass);
                if (egret.is(module, "how.module.GlobalModule")) {
                    error("全局模块 " + egret.getQualifiedClassName(module) + " 只能通过启动类的this.moduleManager.initGlobalModule方法来初始化！");
                    return null;
                }
                for (var key in args) {
                    module[key] = args[key];
                }
                var tag = module.tag;
                this.modules[tag] = this.modules[tag] || [];
                var moduleList = this.modules[tag];
                if (moduleList.length > this.maxModuleListLength) {
                    var deleteModuleList = moduleList.splice(0, moduleList.length - this.maxModuleListLength);
                    for (var i = 0; i < deleteModuleList.length; i++) {
                        this.moduleTypes[deleteModuleList[i]] = null;
                    }
                }
                moduleList.push(module.hashCode);
                this.moduleTypes[module.hashCode] = {
                    moduleType: egret.getQualifiedClassName(module),
                    guiType: egret.getQualifiedClassName(module.guiType),
                    dataType: egret.getQualifiedClassName(module.dataType)
                };
                module.init();
                if (onGUIComplete && module.gui) {
                    if (module.gui.stage) {
                        egret.callLater(function () {
                            onGUIComplete.call(thisObject, module.gui);
                        }, this);
                    }
                    else {
                        module.gui.addEventListener("childrenCreated", function (event) {
                            egret.callLater(function () {
                                onGUIComplete.call(thisObject, module.gui);
                            }, _this);
                        }, this);
                    }
                }
                return module;
            };
            /**
             * 初始化全局模块，只有how.HowMain的模块管理器才有此功能
             * @param moduleClass 模块类
             * @param dataClass 数据类
             */
            ModuleManager.prototype.initGlobalModule = function (moduleClass, dataClass) {
                if (dataClass === void 0) { dataClass = null; }
                var module = new moduleClass(null, dataClass);
                module.init();
                return module;
            };
            /**
             * 销毁模块
             * @param module 要销毁的模块
             */
            ModuleManager.prototype.destroyModule = function (module) {
                var index = -1;
                var moduleList;
                for (var tag in this.modules) {
                    moduleList = this.modules[tag];
                    index = moduleList.indexOf(module.hashCode);
                    if (index != -1) {
                        break;
                    }
                }
                if (index != -1) {
                    moduleList.splice(index, 1);
                }
            };
            /**
             * 初始化上一个模块
             * @param tag 模块标签
             */
            ModuleManager.prototype.initPreviousModule = function (tag) {
                var moduleList = this.modules[tag];
                if (moduleList && moduleList.length >= 2) {
                    moduleList.splice(moduleList.length - 1, 1);
                    var moduleConfig = this.moduleTypes[moduleList[moduleList.length - 1]];
                    var moduleType = egret.getDefinitionByName(moduleConfig.moduleType);
                    var module = new moduleType(egret.getDefinitionByName(moduleConfig.guiType), egret.getDefinitionByName(moduleConfig.dataType));
                    module.init();
                }
            };
            return ModuleManager;
        }());
        module_1.ModuleManager = ModuleManager;
        __reflect(ModuleManager.prototype, "how.module.ModuleManager");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=Module.js.map