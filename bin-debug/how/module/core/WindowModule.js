var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 窗口模块
         * @author 袁浩
         *
         */
        var WindowModule = (function (_super) {
            __extends(WindowModule, _super);
            /**
             * 窗口模块
             * @param guiClass 视图类
             * @param dataClass 数据类
             * @param modal 是否是模态窗口
             * @param center  是否默认居中
             * @param autoClose 点击窗口以为区域是否自动关闭
             */
            function WindowModule(guiClass, dataClass, modal, center, autoClose) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                if (modal === void 0) { modal = true; }
                if (center === void 0) { center = true; }
                if (autoClose === void 0) { autoClose = true; }
                var _this = _super.call(this, guiClass, dataClass) || this;
                /**
                 * 模块标签，用来区分模块类型
                 */
                _this.tag = 2;
                _this.modale = modal;
                _this.center = center;
                _this.autoClose = autoClose;
                return _this;
            }
            /**
            * 初始化模块
            */
            WindowModule.prototype.init = function () {
                _super.prototype.init.call(this);
                how.Application.addWindow(this.gui, this.modale, this.center, this.autoClose);
            };
            /**
            * 移除模块
            */
            WindowModule.prototype.destroy = function () {
                _super.prototype.destroy.call(this);
                //            Application.closeWindow(this.gui);
            };
            /**
             * 关闭窗口
             */
            WindowModule.prototype.close = function () {
                if (this.gui) {
                    this.gui["close"]();
                }
            };
            /**
             * 强制关闭窗口
             */
            WindowModule.prototype.forceClose = function () {
                if (this.gui) {
                    this.gui["forceClose"]();
                }
            };
            return WindowModule;
        }(module.Module));
        module.WindowModule = WindowModule;
        __reflect(WindowModule.prototype, "how.module.WindowModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=WindowModule.js.map