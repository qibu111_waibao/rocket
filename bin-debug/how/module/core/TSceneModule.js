var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 带约束泛型的场景模块
         * @author 袁浩
         *
         */
        var TSceneModule = (function (_super) {
            __extends(TSceneModule, _super);
            /**
             * 场景模块
             * @param guiClass 模块类
             * @param dataClass 模块数据
             * @param removeAllPops 切换的时候是否移除所有的弹出层，默认为false
             * @param cancelAllHttps 切换的时候是否移除所有http请求
             */
            function TSceneModule(guiClass, dataClass, removeAllPops, cancelAllHttps) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                if (removeAllPops === void 0) { removeAllPops = false; }
                if (cancelAllHttps === void 0) { cancelAllHttps = true; }
                return _super.call(this, guiClass, dataClass, removeAllPops, cancelAllHttps) || this;
            }
            Object.defineProperty(TSceneModule.prototype, "tgui", {
                /**
                 * 视图对象，返回类型就是传入的类型V
                 */
                get: function () {
                    return this.gui;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TSceneModule.prototype, "tdata", {
                /**
                 * 数据对象，返回类型就是传入的类型D
                 */
                get: function () {
                    return this.data;
                },
                enumerable: true,
                configurable: true
            });
            return TSceneModule;
        }(module.SceneModule));
        module.TSceneModule = TSceneModule;
        __reflect(TSceneModule.prototype, "how.module.TSceneModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=TSceneModule.js.map