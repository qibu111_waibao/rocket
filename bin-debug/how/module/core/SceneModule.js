var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
        * 场景模块
        * @author 袁浩
        *
        */
        var SceneModule = (function (_super) {
            __extends(SceneModule, _super);
            /**
             * 场景模块
             * @param guiClass 模块类
             * @param dataClass 模块数据
             * @param removeAllPops 切换的时候是否移除所有的弹出层，默认为false
             * @param cancelAllHttps 切换的时候是否移除所有http请求
             */
            function SceneModule(guiClass, dataClass, removeAllPops, cancelAllHttps) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                if (removeAllPops === void 0) { removeAllPops = false; }
                if (cancelAllHttps === void 0) { cancelAllHttps = true; }
                var _this = _super.call(this, guiClass, dataClass) || this;
                /**
                 * 模块标签，用来区分模块类型
                 */
                _this.tag = 1;
                _this.removeAllPops = removeAllPops;
                _this.cancelAllHttps = cancelAllHttps;
                return _this;
            }
            /**
            * 初始化模块
            */
            SceneModule.prototype.init = function () {
                _super.prototype.init.call(this);
                how.Application.changeScene(this.gui, this.removeAllPops, this.cancelAllHttps);
            };
            /**
             * 当按下返回时，如果没有重写readyBack，则默认会初始化上一个模块
             */
            SceneModule.prototype.onBack = function () {
                if (this.isBacked) {
                    return;
                }
                if (how.WindowManager.getInstance().windowCount == 0) {
                    if (this.readyBack()) {
                        this.isBacked = true;
                        this.moduleManager.initPreviousModule(this.tag);
                    }
                    else {
                        this.isBacked = false;
                    }
                }
                else {
                    how.WindowManager.getInstance().closeLast();
                }
            };
            /**
             * 当准备返回时，可以重写此函数并返回false来实现返回事件的拦截
             */
            SceneModule.prototype.readyBack = function () {
                return true;
            };
            /**
            * 移除模块
            */
            SceneModule.prototype.destroy = function () {
                _super.prototype.destroy.call(this);
                //            Application.changeScene(null,this.removeAllPops,this.cancelAllHttps);
            };
            return SceneModule;
        }(module.Module));
        module.SceneModule = SceneModule;
        __reflect(SceneModule.prototype, "how.module.SceneModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=SceneModule.js.map