var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 带约束泛型的场景模块
         * @author 袁浩
         *
         */
        var TWindowModule = (function (_super) {
            __extends(TWindowModule, _super);
            /**
             * 窗口模块
             * @param guiClass 视图类
             * @param dataClass 数据类
             * @param modal 是否是模态窗口
             * @param center  是否默认居中
             * @param autoClose 点击窗口以为区域是否自动关闭
             */
            function TWindowModule(guiClass, dataClass, modal, center, autoClose) {
                if (guiClass === void 0) { guiClass = null; }
                if (dataClass === void 0) { dataClass = null; }
                if (modal === void 0) { modal = true; }
                if (center === void 0) { center = true; }
                if (autoClose === void 0) { autoClose = true; }
                return _super.call(this, guiClass, dataClass, modal, center, autoClose) || this;
            }
            Object.defineProperty(TWindowModule.prototype, "tgui", {
                /**
                 * 视图对象，返回类型就是传入的类型V
                 */
                get: function () {
                    return this.gui;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TWindowModule.prototype, "tdata", {
                /**
                 * 数据对象，返回类型就是传入的类型D
                 */
                get: function () {
                    return this.data;
                },
                enumerable: true,
                configurable: true
            });
            return TWindowModule;
        }(module.WindowModule));
        module.TWindowModule = TWindowModule;
        __reflect(TWindowModule.prototype, "how.module.TWindowModule");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=TWindowModule.js.map