var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var module;
    (function (module) {
        /**
         * 所有数据的基类
         * @author 袁浩
         *
         */
        var Data = (function (_super) {
            __extends(Data, _super);
            /**
             * 数据类
             */
            function Data() {
                return _super.call(this) || this;
            }
            /**
            * 执行控制层的方法
            */
            Data.prototype.report = function (request) {
                var args = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    args[_i - 1] = arguments[_i];
                }
            };
            /**
             * 根据数据源设置数据
             */
            Data.prototype.setData = function (source) {
                for (var key in source) {
                    this[key] = source[key];
                }
            };
            return Data;
        }(egret.EventDispatcher));
        module.Data = Data;
        __reflect(Data.prototype, "how.module.Data");
    })(module = how.module || (how.module = {}));
})(how || (how = {}));
//# sourceMappingURL=Data.js.map