var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 和容器的通讯工具
     * @author 袁浩
     *
     */
    var ExternalInterfaceUtils = (function () {
        function ExternalInterfaceUtils() {
        }
        Object.defineProperty(ExternalInterfaceUtils, "channelID", {
            /**
             * 渠道号
             */
            get: function () {
                return this._channelID;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExternalInterfaceUtils, "IMEI", {
            /**
             * 手机设备唯一序列号
             */
            get: function () {
                return this._IMEI;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExternalInterfaceUtils, "version", {
            /**
             * 应用版本号
             */
            get: function () {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
                    var version = window.location.href;
                    version = version.substring(0, version.lastIndexOf("/index.html"));
                    version = version.substring(version.lastIndexOf("/") + 1);
                    if (/^\d\.(\d+\.)+\d+$/.test(version)) {
                        this._version = version;
                        return this._version;
                    }
                }
                return this._version;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExternalInterfaceUtils, "MACAddress", {
            /**
             * 设备MAC地址
             */
            get: function () {
                return this._MACAddress;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 获取手机卡运营商
         * 参数：回调方法，0：无卡，1：无法判断 ，2：电信，3：联通 ，4 ：移动
         */
        ExternalInterfaceUtils.getCardProvidersName = function () {
            return 0;
        };
        /**
         * 获取联通渠道号
         */
        ExternalInterfaceUtils.GetUnipayId = function () {
            return 0;
        };
        ExternalInterfaceUtils.init = function () {
            if (how.Utils.isIOSNative()) {
                if (!window["webkit"]) {
                    this.getChannelID(window["channelID"]);
                    this.getIMEI(window["imei"]);
                    this.getPackageName(window["packageName"]);
                    this.getVersion(window["version"]);
                }
                else {
                    window["webkit"].messageHandlers.getBaseInfo.postMessage("");
                }
            }
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                egret.ExternalInterface.addCallback("onResume", this.onResume);
                egret.ExternalInterface.addCallback("onPause", this.onPause);
                egret.ExternalInterface.addCallback("onBack", this.onBack);
                egret.ExternalInterface.addCallback("getIMEI", this.getIMEI);
                egret.ExternalInterface.addCallback("getMAC", this.getMAC);
                egret.ExternalInterface.addCallback("getVersion", this.getVersion);
                egret.ExternalInterface.addCallback("getPackageName", this.getPackageName);
            }
            else if (egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
                window.onkeydown = this.onKeyDown;
                //                window.onblur = this.onblur;
                //                window.onfocus = this.onfocus;
                var hidden, state, visibilityChange;
                if (typeof document.hidden !== "undefined") {
                    hidden = "hidden";
                    visibilityChange = "visibilitychange";
                    state = "visibilityState";
                }
                else if (typeof document["mozHidden"] !== "undefined") {
                    hidden = "mozHidden";
                    visibilityChange = "mozvisibilitychange";
                    state = "mozVisibilityState";
                }
                else if (typeof document["msHidden"] !== "undefined") {
                    hidden = "msHidden";
                    visibilityChange = "msvisibilitychange";
                    state = "msVisibilityState";
                }
                else if (typeof document["webkitHidden"] !== "undefined") {
                    hidden = "webkitHidden";
                    visibilityChange = "webkitvisibilitychange";
                    state = "webkitVisibilityState";
                }
                var self = this;
                // 添加监听器，在title里显示状态变化
                document.addEventListener(visibilityChange, function (event) {
                    if (document[state] == "hidden") {
                        self.onPause(document[state]);
                    }
                    else {
                        self.onResume(document[state]);
                    }
                }, false);
            }
            egret.ExternalInterface.call("getMAC", "");
            egret.ExternalInterface.call("getIMEI", "");
            egret.ExternalInterface.call("getVersion", "");
            egret.ExternalInterface.call("requestPackageName", "");
        };
        ExternalInterfaceUtils.getPackageName = function (data) {
            ExternalInterfaceUtils.packageName = data;
        };
        ExternalInterfaceUtils.onKeyDown = function (event) {
            if (event.keyCode == 27) {
                how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_BACK);
            }
        };
        ExternalInterfaceUtils.onResume = function (data) {
            how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_RESUME);
        };
        ExternalInterfaceUtils.onPause = function (data) {
            how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_PAUSE);
        };
        ExternalInterfaceUtils.onBack = function (data) {
            how.EventManager["getInstance"](this).dispatchEvent(how.Application.APPEVENT_BACK);
        };
        ExternalInterfaceUtils.getIMEI = function (data) {
            ExternalInterfaceUtils._IMEI = data;
            trace("获取到设备唯一序列号：" + data);
        };
        ExternalInterfaceUtils.getChannelID = function (data) {
            ExternalInterfaceUtils._channelID = data;
            trace("获取到设备唯一序列号：" + data);
        };
        ExternalInterfaceUtils.getMAC = function (data) {
            ExternalInterfaceUtils._MACAddress = data;
            trace("获取到设备MAC地址：" + data);
        };
        ExternalInterfaceUtils.getVersion = function (data) {
            ExternalInterfaceUtils._version = data;
            trace("获取到应用版本号：" + data);
        };
        ExternalInterfaceUtils.onWebViewJavascriptBridgeReady = function (bridge) {
            bridge.registerHandler("texttext", this.ontext.bind(this));
        };
        ExternalInterfaceUtils.ontext = function (data) {
            alert(data);
        };
        return ExternalInterfaceUtils;
    }());
    ExternalInterfaceUtils._channelID = "";
    ExternalInterfaceUtils._IMEI = "";
    ExternalInterfaceUtils._version = "0.0.1";
    ExternalInterfaceUtils._MACAddress = "0:0:0:0";
    /**
     * 获取包名
     */
    ExternalInterfaceUtils.packageName = "null";
    how.ExternalInterfaceUtils = ExternalInterfaceUtils;
    __reflect(ExternalInterfaceUtils.prototype, "how.ExternalInterfaceUtils");
})(how || (how = {}));
function setupWebViewJavascriptBridge(callback) {
    if (window["WebViewJavascriptBridge"]) {
        return callback(window["WebViewJavascriptBridge"]);
    }
    if (window["WVJBCallbacks"]) {
        return window["WVJBCallbacks"].push(callback);
    }
    window["WVJBCallbacks"] = [callback];
    var WVJBIframe = document.createElement('iframe');
    WVJBIframe.style.display = 'none';
    WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
    document.documentElement.appendChild(WVJBIframe);
    setTimeout(function () { document.documentElement.removeChild(WVJBIframe); }, 0);
}
if (window && window.navigator && navigator.userAgent == "ios_miq") {
    setupWebViewJavascriptBridge(function (bridge) {
        bridge.registerHandler('texttext', function (data, responseCallback) {
            alert("JS Echo called with:" + data);
            responseCallback(data);
        });
        //        bridge.callHandler('ObjC Echo',function responseCallback(responseData) {
        //            console.log("JS received response:",responseData)
        //        })
    });
}
//# sourceMappingURL=ExternalInterfaceUtils.js.map