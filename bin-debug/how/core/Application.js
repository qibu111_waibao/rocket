var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 应用管理器，提供初始化应用，场景切换等功能
     * @author 袁浩、王锡铜
     *
     */
    var Application = (function () {
        /**
         * 应用管理器，禁止实例化
         */
        function Application() {
            error("应用管理器禁止实例化！");
        }
        Object.defineProperty(Application, "appName", {
            /**
             * 应用名称
             */
            get: function () {
                return this._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "app", {
            /**
             * 应用
             */
            get: function () {
                return this._app;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "IMEI", {
            /**
             * 设备唯一序列号，native在安卓是取的MAC地址
             */
            get: function () {
                if (how.ExternalInterfaceUtils.IMEI) {
                    return how.ExternalInterfaceUtils.IMEI;
                }
                var result = egret.localStorage.getItem(this.appName + "imei_windows");
                if (!result) {
                    result = how.Utils.md5(how.Utils.formatDate(new Date(), "qqddMMhhmmS") + Math.random());
                    egret.localStorage.setItem(this.appName + "imei_windows", result);
                }
                return result;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 把场景缓存成位图以优化渲染
         * @param onCache 是否缓存
         */
        Application.cacheSceneAsBitmap = function (onCache) {
            if (onCache === void 0) { onCache = false; }
            this._guiLayer.cacheAsBitmap = onCache;
        };
        Object.defineProperty(Application, "version", {
            /**
             * 应用版本号-web版本根据地址栏解析
             */
            get: function () { return how.ExternalInterfaceUtils.version; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "resourceVersion", {
            /**
             * 应用资源版本对象
             */
            get: function () { return this._resourceVersion; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_RIGHTCLICK", {
            /**
             * 应用右键事件
             */
            get: function () { return "app_rightclick"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_EXIT", {
            /**
             * 应用退出事件
             */
            get: function () { return "app_exit"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_BACK", {
            /**
             * 应用返回事件
             */
            get: function () { return "app_back"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_RESUME", {
            /**
             * 应用继续事件
             */
            get: function () { return "app_resume"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_PAUSE", {
            /**
             * 应用暂停事件
             */
            get: function () { return "app_pause"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_RESIZE", {
            /**
            * 应用大小改变
            */
            get: function () { return "app_resize"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_WINDOW_OPEN", {
            /**
            * 窗口被打开
            */
            get: function () { return "app_window_open"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "APPEVENT_WINDOW_CLOSE", {
            /**
            * 窗口被关闭
            */
            get: function () { return "app_window_close"; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "currentScene", {
            /**
            * 当前场景
            * */
            get: function () {
                return this._currentScene;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Application, "loadClass", {
            /**
             * 加载资源的进度界面
             */
            get: function () {
                return this._loadClass;
            },
            enumerable: true,
            configurable: true
        });
        /**
        * 初始化应用
        * @param app {how.HowMain} 应用所在容器
        * @param indexScene {how.Scene} 第一个显示的场景
        * */
        Application.init = function (app, appName, desginWidth, desginHeight, loadClass) {
            if (desginWidth === void 0) { desginWidth = 960; }
            if (desginHeight === void 0) { desginHeight = 640; }
            if (loadClass === void 0) { loadClass = null; }
            trace(appName + "启动成功，当前版本：" + this.version);
            this._appName = appName;
            this._loadClass = loadClass;
            this._resourceVersion = new RES.VersionController();
            RES.registerVersionController(this._resourceVersion);
            this._app = app;
            this._guiLayer = this.createLayer();
            this._guiLayer.percentWidth = this._guiLayer.percentHeight = 100;
            this._windowLayer = this.createLayer();
            this._windowLayer.percentWidth = this._windowLayer.percentHeight = 100;
            this._windowLayer.touchEnabled = false;
            how.WindowManager.getInstance().init(this._windowLayer);
            how.ExternalInterfaceUtils.init();
            app.stage.addEventListener(egret.Event.RESIZE, this.onStageResize, this);
            this.desginWidth = desginWidth;
            this.desginHeight = desginHeight;
            this.setDesignSize();
            this.initGlobalMouseRightEvent();
            return this._guiLayer;
        };
        /**
        * 切换场景
        * @param newScene {how.Scene} 要切换的目标场景
        * @param removeAllPops {boolean} 切换的时候是否移除所有的弹出层，默认为false
        * @param cancelAllHttps {boolean} 切换的时候是否移除所有http请求
        */
        Application.changeScene = function (newScene, removeAllPops, cancelAllHttps) {
            if (removeAllPops === void 0) { removeAllPops = true; }
            if (cancelAllHttps === void 0) { cancelAllHttps = true; }
            if (cancelAllHttps) {
                how.HttpManager.getInstance().cancelAll();
            }
            if (newScene) {
                how.module.Window.clearTextureCache();
                if (egret.is(newScene, "how.module.Scene") && Application.loadClass) {
                    var nextScene = newScene;
                    var resourceList = nextScene.resourceList;
                    if (resourceList && !nextScene.isLoaded) {
                        if (egret.getQualifiedClassName(this._currentLoadScene) == egret.getQualifiedClassName(newScene)) {
                            return;
                        }
                        var loadWindow = new Application.loadClass();
                        this.addWindow(loadWindow, false, true, false, false);
                        nextScene.isLoaded = false;
                        var resourceLoader = new how.ResourceLoader();
                        if (this.autoAddPublicResource) {
                            if (resourceList.indexOf("public") == -1) {
                                resourceList.push("public");
                            }
                            for (var key in how.HowMain.themeConfig) {
                                if (key.indexOf("public") == 0) {
                                    resourceList.push(key);
                                }
                            }
                        }
                        this._currentLoadScene = newScene;
                        resourceLoader.loadGroups(resourceList, this.onResourceComplete, this, loadWindow, nextScene, removeAllPops);
                        return;
                    }
                }
                this._currentScene = newScene;
                this._guiLayer.removeChildren();
                this._app.removeLoadding();
                //                egret.Tween.removeAllTweens();
                if (removeAllPops) {
                    how.WindowManager.getInstance().closeAll();
                }
                this._guiLayer.addChild(newScene);
            }
        };
        Application.onResourceComplete = function (loadWindow, nextScene, removeAllPops) {
            this._currentLoadScene = null;
            this.closeWindow(loadWindow);
            this._currentScene = nextScene;
            this._guiLayer.removeChildren();
            this._app.removeLoadding();
            //            egret.Tween.removeAllTweens();
            if (removeAllPops) {
                how.WindowManager.getInstance().closeAll();
            }
            nextScene.isLoaded = true;
            this._guiLayer.addChild(nextScene);
        };
        Application.createLayer = function () {
            var layer = new eui.Group();
            layer.width = this._app.stage.stageWidth;
            layer.height = this._app.stage.stageHeight;
            layer.left = layer.right = layer.top = layer.bottom = 0;
            this._app.addChild(layer);
            return layer;
        };
        /**
         * 打开窗口
         * @param windowType 窗口类型
         * @param modal 是否是模态窗口
         * @param center  是否默认居中
         * @param autoClose 点击窗口以为区域是否自动关闭
         * @param inList 是否加入窗口渲染列表
         */
        Application.openWindow = function (windowType, modal, center, autoClose, inList) {
            if (modal === void 0) { modal = true; }
            if (center === void 0) { center = true; }
            if (autoClose === void 0) { autoClose = true; }
            if (inList === void 0) { inList = true; }
            var window = new windowType();
            how.WindowManager.getInstance().addWindow(window, modal, center, autoClose, inList);
            return window;
        };
        /**
        * 加入窗口
        * @param window {egret.gui.IVisualElement} 窗口实例
        * @param modal {boolean} 是否是模态窗口
        * @param center {boolean} 是否默认居中
        */
        Application.addWindow = function (window, modal, center, autoClose, inList) {
            if (modal === void 0) { modal = true; }
            if (center === void 0) { center = true; }
            if (autoClose === void 0) { autoClose = false; }
            if (inList === void 0) { inList = true; }
            how.WindowManager.getInstance().addWindow(window, modal, center, autoClose, inList);
        };
        /**
         * 显示载入进度界面
         */
        Application.addLoadding = function () {
            var loadWindow = new Application.loadClass();
            this.addWindow(loadWindow, false, true, false, false);
            return loadWindow;
        };
        /**
         * 移除载入进度界面
         */
        Application.removeLoadding = function (loadWindow) {
            if (!loadWindow) {
                warn("loadWindow是null，但是你还是要how.Applcation.removeLoadding来移出他。");
            }
            this.closeWindow(loadWindow);
        };
        /**
         * 关闭窗口
         * @param window {egret.gui.IVisualElement} 窗口实例
         */
        Application.closeWindow = function (window) {
            how.WindowManager.getInstance().closeWindow(window);
        };
        Application.onStageResize = function (event) {
            how.WindowManager.getInstance().updateBg();
            this.setDesignSize();
            how.EventManager["getInstance"](this).dispatchEvent(this.APPEVENT_RESIZE);
        };
        /**
        * 重新定义设计尺寸
        * @param dw {number} 设计宽度
        * @param dh {number} 设计高度
        **/
        //        public static setDesignSize(): void {
        //            var stage: egret.Stage = this._guiLayer.stage;
        //            var clientWidth: number = stage.stageWidth;
        //            var clientHeight: number = stage.stageHeight;
        //            var clientPercent: number = clientWidth / clientHeight;//获取屏幕宽高比
        //            var desginPercent: number = this.desginWidth / this.desginHeight;//获取设计宽高比
        //            if(clientPercent > desginPercent)//如果屏幕宽度过高，那么改变设计宽度
        //            {
        //                stage.scaleMode = egret.StageScaleMode.FIXED_HEIGHT;
        //            }
        //            else//否则就是高度过高
        //            {
        //                stage.scaleMode = egret.StageScaleMode.FIXED_WIDTH;
        //            }
        //        }
        /**
        * 重新定义设计尺寸
        **/
        Application.setDesignSize = function () {
            return;
            var clientWidth;
            var clientHeight;
            var stage = this._guiLayer.stage;
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                clientWidth = egret_native.EGTView.getFrameWidth();
                clientHeight = egret_native.EGTView.getFrameHeight();
            }
            else {
                clientWidth = document.documentElement.clientWidth;
                clientHeight = document.documentElement.clientHeight;
            }
            var clientPercent = clientWidth / clientHeight; //获取屏幕宽高比
            var desginPercent = this.desginWidth / this.desginHeight; //获取设计宽高比
            if (clientPercent > desginPercent) {
                this.desginWidth = clientPercent * this.desginHeight;
            }
            else {
                this.desginHeight = this.desginWidth / clientPercent;
            }
            //            stage.setContentSize(this.desginWidth,this.desginHeight);
        };
        /**
         * 初始化全局鼠标右键事件，移动平台是2个手指的触摸代表右键
         */
        Application.initGlobalMouseRightEvent = function () {
            var _this = this;
            if (!egret.Capabilities.isMobile) {
                document.oncontextmenu = function (event) {
                    how.EventManager["getInstance"](Application).dispatchEvent(Application.APPEVENT_RIGHTCLICK, event);
                    return false;
                };
            }
            egret.MainContext.instance.stage.addEventListener(egret.TouchEvent.TOUCH_BEGIN, function (event) {
                _this.isRight = event.touchPointID == 1;
            }, this);
            egret.MainContext.instance.stage.addEventListener(egret.TouchEvent.TOUCH_END, function (event) {
                if (_this.isRight) {
                    _this.isRight = false;
                    how.EventManager["getInstance"](Application).dispatchEvent(Application.APPEVENT_RIGHTCLICK, event);
                }
            }, this);
        };
        /**
        * 退出游戏
        */
        Application.exit = function () {
            var event = new egret.Event(this.APPEVENT_EXIT, false, true);
            if (how.EventManager["getInstance"](this).dispatch(event)) {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    egret.ExternalInterface.call("exit", "");
                }
                else {
                    window.close();
                }
            }
        };
        /**
         * 重启应用
         */
        Application.restart = function () {
            if (how.Utils.isIOSNative()) {
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.reStart.postMessage("");
                }
                else {
                    window["reStart"]("");
                }
            }
            else {
                window.location.reload();
            }
        };
        /**
         * 会使用一切可能通知到的手段对用户提示
         */
        Application.ring = function (title, message) {
            if (title === void 0) { title = null; }
            if (message === void 0) { message = null; }
            this.vibrate();
            this.vibrationScreen();
            this.notify(title, message);
        };
        /**
         * 振动手机
         */
        Application.vibrate = function () {
            if (this.vibrateAbled) {
                if (how.Utils.isIOSNative()) {
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "vibrate", 2: "" }));
                    }
                    else {
                        window["sound"]("vibrate");
                    }
                }
                else {
                    egret.ExternalInterface.call("vibrate", "");
                }
            }
        };
        /**
         * 桌面通知
         */
        Application.notify = function (title, message) {
            if (!this.notifyAbled) {
                return;
            }
            title = title || "";
            message = message || "";
            if (window) {
                var Notification = window["Notification"] || window["mozNotification"] || window["webkitNotification"];
                if (Notification) {
                    Notification.requestPermission(function (status) {
                        if (Notification.permission !== status) {
                            Notification.permission = status;
                        }
                        if (status === "granted") {
                            var instance = new Notification(title, {
                                body: message,
                                icon: "favicon.ico"
                            });
                            instance.onshow = function () {
                                setTimeout(function () {
                                    instance.close();
                                }, 1000);
                            };
                        }
                    });
                }
            }
        };
        /**
         * 振动屏幕
         */
        Application.vibrationScreen = function () {
            if (!this.vibrationScreenAbled) {
                return;
            }
            this._app.removeEventListener(egret.Event.ENTER_FRAME, this.onSceneEnterFrame, this);
            this.i = 0;
            this._app.addEventListener(egret.Event.ENTER_FRAME, this.onSceneEnterFrame, this);
        };
        Application.onSceneEnterFrame = function () {
            if (this.i < this.vibrationArrayY.length) {
                this._app.y = this.vibrationArrayY[this.i];
                this._app.x = this.vibrationArrayX[this.i];
            }
            else {
                this._app.removeEventListener(egret.Event.ENTER_FRAME, this.onSceneEnterFrame, this);
                this.i = 0;
            }
            this.i++;
        };
        return Application;
    }());
    /**
     * 振动开关
     */
    Application.vibrateAbled = true;
    /**
     * 振动屏幕开关
     */
    Application.vibrationScreenAbled = true;
    /**
     * 桌面提示开关
     */
    Application.notifyAbled = true;
    Application.autoAddPublicResource = false;
    Application.isRight = false;
    Application.vibrationArrayY = [-8, 8, -6, 6, -4, 4, -2, 2, 0];
    Application.vibrationArrayX = [-5, 7, -3, 9, -2, 3, -1, 1, 0];
    Application.i = 0;
    how.Application = Application;
    __reflect(Application.prototype, "how.Application");
})(how || (how = {}));
//# sourceMappingURL=Application.js.map