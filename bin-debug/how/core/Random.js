var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 线性同余随机数生成器，因为js提供的api不支持种子，所以为了匹配Unity，另写一套
     * @author 袁浩
     */
    var Random = (function () {
        /**
         * 创建一个随机数生成器
         */
        function Random(seed) {
            this.seed = seed;
            if (!this.seed && this.seed != 0) {
                this.seed = new Date().getTime();
            }
        }
        Object.defineProperty(Random.prototype, "value", {
            /**
             * 返回一个随机数，在0.0～1.0之间
             */
            get: function () {
                return this.range(0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Random.prototype, "insideUnitCircle", {
            /**
             * 返回半径为1的圆内的一个随机点
             */
            get: function () {
                var randomAngle = this.range(0, 360);
                var randomDis = this.range(0, 1);
                var randomX = randomDis * Math.cos(randomAngle * Math.PI / 180);
                var randomY = randomDis * Math.sin(randomAngle * Math.PI / 180);
                return new egret.Point(randomX, randomY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Random.prototype, "onUnitCircle", {
            /**
             * 返回半径为1的圆边的一个随机点
             */
            get: function () {
                var randomAngle = this.range(0, 360);
                var randomX = Math.cos(randomAngle * Math.PI / 180);
                var randomY = Math.sin(randomAngle * Math.PI / 180);
                return new egret.Point(randomX, randomY);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 返回一个在min和max之间的随机浮点数
         */
        Random.prototype.range = function (min, max) {
            if (!this.seed && this.seed != 0) {
                this.seed = new Date().getTime();
            }
            max = max || 1;
            min = min || 0;
            this.seed = (this.seed * 9301 + 49297) % 233280;
            var rnd = this.seed / 233280.0;
            return min + rnd * (max - min);
        };
        Object.defineProperty(Random, "value", {
            /**
             * 返回一个随机数，在0.0～1.0之间
             */
            get: function () {
                return this.range(0, 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Random, "insideUnitCircle", {
            /**
             * 返回半径为1的圆内的一个随机点
             */
            get: function () {
                var randomAngle = this.range(0, 360);
                var randomDis = this.range(0, 1);
                var randomX = randomDis * Math.cos(randomAngle * Math.PI / 180);
                var randomY = randomDis * Math.sin(randomAngle * Math.PI / 180);
                return new egret.Point(randomX, randomY);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Random, "onUnitCircle", {
            /**
             * 返回半径为1的圆边的一个随机点
             */
            get: function () {
                var randomAngle = this.range(0, 360);
                var randomX = Math.cos(randomAngle * Math.PI / 180);
                var randomY = Math.sin(randomAngle * Math.PI / 180);
                return new egret.Point(randomX, randomY);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 返回一个在min和max之间的随机浮点数
         */
        Random.range = function (min, max) {
            if (!this.seed && this.seed != 0) {
                this.seed = new Date().getTime();
            }
            max = max || 1;
            min = min || 0;
            this.seed = (this.seed * 9301 + 49297) % 233280;
            var rnd = this.seed / 233280.0;
            return min + rnd * (max - min);
        };
        return Random;
    }());
    how.Random = Random;
    __reflect(Random.prototype, "how.Random");
})(how || (how = {}));
//# sourceMappingURL=Random.js.map