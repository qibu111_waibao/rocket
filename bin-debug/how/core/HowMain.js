var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 游戏入口类的基类
     * @author 袁浩
     *
     */
    var HowMain = (function (_super) {
        __extends(HowMain, _super);
        /**
         * 游戏入口的基类
         * @param appName 游戏名称
         * @param loadingClass 初始加载界面的类
         * @param resource 资源配置路径
         * @param indexGroups 初始加载所需资源
         * @param desginWidth 设计尺寸-宽度
         * @param desginHeight 设计尺寸-高度
         * @param windowLoadingClass 运行时新界面的资源加载条的类
         * @param useLog 是否使用日志，一个自带UI的日志，按返回键（ios暂不支持、windows是esc键）开关日志显示
         */
        function HowMain(appName, loadingClass, resource, indexGroups, desginWidth, desginHeight, windowLoadingClass, useLog, themeTogether, themeConfig, themePath) {
            if (indexGroups === void 0) { indexGroups = []; }
            if (desginWidth === void 0) { desginWidth = 960; }
            if (desginHeight === void 0) { desginHeight = 640; }
            if (windowLoadingClass === void 0) { windowLoadingClass = null; }
            if (useLog === void 0) { useLog = false; }
            if (themeTogether === void 0) { themeTogether = true; }
            if (themeConfig === void 0) { themeConfig = null; }
            if (themePath === void 0) { themePath = "default.thm.json"; }
            var _this = _super.call(this) || this;
            /**
             * 是否启动Update事件，默认关闭
             */
            _this.useUpdate = false;
            _this.current = 1;
            _this.useLog = useLog;
            _this._appName = appName;
            _this.appLoadingClass = loadingClass;
            _this.indexGroups = indexGroups;
            _this.resource = resource;
            _this.desginWidth = desginWidth;
            _this.desginHeight = desginHeight;
            _this.windowLoadingClass = windowLoadingClass;
            _this.total = indexGroups.length;
            HowMain.themeConfig = themeConfig;
            HowMain.themeTogether = themeTogether;
            _this.themePath = "resource/" + themePath;
            _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this._onAddToStage, _this);
            if (_this.useUpdate) {
                _this.addEventListener(egret.Event.ENTER_FRAME, _this.onEnterFrame, _this);
                _this.addEventListener(eui.UIEvent.RENDER, _this.onRender, _this);
            }
            how.EventManager["getInstance"](_this).addEventListener(how.Application.APPEVENT_RIGHTCLICK, _this._onBack, _this);
            how.EventManager["getInstance"](_this).addEventListener(how.Application.APPEVENT_RESUME, _this.onResume, _this);
            how.EventManager["getInstance"](_this).addEventListener(how.Application.APPEVENT_EXIT, _this.onExit, _this);
            return _this;
        }
        Object.defineProperty(HowMain.prototype, "appName", {
            /**
             * 应用名称
             */
            get: function () {
                return this._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HowMain.prototype, "loadingUI", {
            /**
             * 用来显示进度的UI
             */
            get: function () {
                return this.loadingView;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HowMain.prototype, "moduleManager", {
            /**
             * 模块管理器
             */
            get: function () {
                this._moduleManager = this._moduleManager || new how.module.Module()["moduleManager"];
                return this._moduleManager;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(HowMain.prototype, "eventManager", {
            /**
             * 事件监听管理器
             */
            get: function () {
                return how.EventManager["getInstance"](this);
            },
            enumerable: true,
            configurable: true
        });
        HowMain.prototype.onRender = function () {
        };
        HowMain.prototype.onExit = function (event) {
            if (!this.onApplicationQuit()) {
                event.preventDefault();
            }
        };
        HowMain.prototype.onResume = function (event) {
            this.onApplicationFocus();
        };
        HowMain.prototype.onPause = function (event) {
            this.onApplicationPause();
        };
        HowMain.prototype.onApplicationQuit = function () {
            return true;
        };
        HowMain.prototype.onApplicationFocus = function () {
        };
        HowMain.prototype.onApplicationPause = function () {
        };
        HowMain.prototype._onBack = function () {
            if (this.useLog) {
                how.Log.useLog = !how.Log.useLog;
            }
            this.onBack();
        };
        HowMain.prototype.onBack = function () {
        };
        HowMain.prototype.awake = function () {
        };
        HowMain.prototype.update = function () {
        };
        HowMain.prototype.onDestroy = function () {
        };
        HowMain.prototype.onEnterFrame = function (event) {
            this.update();
        };
        HowMain.prototype._onAddToStage = function (event) {
            if (!useConsoleLog) {
                this.stage.maxTouches = 1;
            }
            how.Application.init(this, this._appName, this.desginWidth, this.desginHeight, this.windowLoadingClass);
            //注入自定义的素材解析器
            this.stage.registerImplementation("eui.IAssetAdapter", new AssetAdapter());
            this.stage.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            //egret.gui.Theme.load("resource/theme.thm");
            //Config loading process interface
            //设置加载进度界面
            // initialize the Resource loading library
            //初始化Resource资源加载库
            RES.addEventListener(RES.ResourceEvent.CONFIG_COMPLETE, this.onConfigComplete, this);
            RES.loadConfig("resource/" + this.resource, "resource/");
        };
        HowMain.prototype.onLoadingComplete = function (event) {
            if (this.loadingView) {
                this.loadingView.removeEventListener(egret.Event.COMPLETE, this.onLoadingComplete, this);
            }
            this.init();
            this.awake();
        };
        /**
         * 开始初始化
         */
        HowMain.prototype.init = function () {
            RES.removeEventListener(RES.ResourceEvent.CONFIG_COMPLETE, this.onConfigComplete, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
            this.loadGroup();
        };
        /**
        * 配置文件加载完成,开始预加载preload资源组。
        */
        HowMain.prototype.onConfigComplete = function (event) {
            how.Application.resourceConfig = event.target.resConfig;
            if (this.appLoadingClass) {
                this.loadingView = new this.appLoadingClass();
                this.loadingView.addEventListener(egret.Event.COMPLETE, this.onLoadingComplete, this);
                this.addChildAt(this.loadingView, 0);
            }
            else {
                this.onLoadingComplete(null);
            }
        };
        /**
        * 资源组加载出错
        * Resource group loading failed
        */
        HowMain.prototype.onResourceLoadError = function (event) {
            this.onLoaddingError(event);
            //忽略加载失败的项目
            this.onResourceLoadComplete(event);
        };
        HowMain.prototype.loadGroup = function () {
            if (this.indexGroups.length > 0) {
                var loadGroup = this.indexGroups.shift();
                this.onLoadGroupReady(loadGroup);
                RES.loadGroup(loadGroup);
            }
            else {
                this.onAllGroupComplete();
                RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
                RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
                RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
                if (HowMain.themeTogether) {
                    var theme = new eui.Theme(this.themePath, this.stage);
                    theme.addEventListener(eui.UIEvent.COMPLETE, this.onThemeLoadComplete, this);
                }
                else {
                    this.setTheme();
                    this.start();
                }
            }
        };
        HowMain.prototype.setTheme = function () {
            var parseSkinName = eui.Component.prototype.$parseSkinName;
            eui.Component.prototype.$parseSkinName = function () {
                if (typeof this.skinName == "string" && this.skinName.lastIndexOf(".exml") == -1) {
                    if (!HowMain.themeConfig[this.skinName]) {
                        error(this.skinName + "不存在");
                    }
                    else {
                        this.skinName = how.Application.resourceVersion.getVirtualUrl(HowMain.themeConfig[this.skinName]);
                    }
                }
                if (this.skinName) {
                    parseSkinName.call(this);
                }
            };
        };
        /**
         * 所有组资源加载完成
         */
        HowMain.prototype.onAllGroupComplete = function () {
        };
        /**
         * 主题文件加载完成
         */
        HowMain.prototype.onThemeLoadComplete = function () {
            this.start();
        };
        /**
         * 移除进度条
         */
        HowMain.prototype.removeLoadding = function () {
            if (this.loadingView) {
                this.removeChild(this.loadingView);
                this.loadingView = null;
            }
        };
        /**
         * 子类继承获取准备加载下一个组
         */
        HowMain.prototype.onLoadGroupReady = function (groupName) {
            trace("资源[" + groupName + "]:准备加载");
        };
        /**
        * 子类继承获取组加载完成
        */
        HowMain.prototype.onLoadGroupComplete = function (event) {
            trace("资源[" + event.groupName + "]:加载结束");
        };
        /**
        * preload资源组加载完成
        * preload resource group is loaded
        */
        HowMain.prototype.onResourceLoadComplete = function (event) {
            this.onLoadGroupComplete(event);
            this.loadGroup();
            this.current++;
        };
        /**
        * preload资源组加载进度
        * loading process of preload resource
        */
        HowMain.prototype.onResourceProgress = function (event) {
            var percent = event.itemsLoaded / event.itemsTotal * 100;
            percent = Math.ceil(percent);
            this.onLoaddingProgress(percent, this.current, this.total);
        };
        /**
         * 子类继承获取加载失败
         */
        HowMain.prototype.onLoaddingError = function (event) {
            warn("资源[" + event.groupName + "]:加载失败");
        };
        /**
         * 子类继承获取加载进度
         */
        HowMain.prototype.onLoaddingProgress = function (percent, current, total) {
        };
        HowMain.prototype.start = function () {
        };
        return HowMain;
    }(eui.UILayer));
    HowMain.themeTogether = true;
    how.HowMain = HowMain;
    __reflect(HowMain.prototype, "how.HowMain", ["how.module.IBehaviour"]);
    /**
     * 模块管理器
     */
    var ModuleManager = (function () {
        function ModuleManager() {
        }
        /**
         * 获取模块管理器单例
         */
        ModuleManager.getInstance = function () {
            return null;
        };
        /**
         * 通过模块管理器初始化的模块会加入到模块列表中，可以拥有先后顺序
         * @param moduleClass 模块类
         * @param guiClass 视图类
         * @param dataClass 数据类
         */
        ModuleManager.prototype.initModule = function (moduleClass, guiClass, dataClass) {
            if (guiClass === void 0) { guiClass = null; }
            if (dataClass === void 0) { dataClass = null; }
            return null;
        };
        /**
         * 初始化全局模块，只有how.HowMain的模块管理器才有此功能
         * @param moduleClass 模块类
         * @param dataClass 数据类
         */
        ModuleManager.prototype.initGlobalModule = function (moduleClass, dataClass) {
            if (dataClass === void 0) { dataClass = null; }
            return null;
        };
        /**
         * 销毁模块
         * @param module 要销毁的模块
         */
        ModuleManager.prototype.destroyModule = function (module) {
        };
        /**
         * 初始化上一个模块
         * @param tag 模块标签
         */
        ModuleManager.prototype.initPreviousModule = function (tag) {
        };
        return ModuleManager;
    }());
    __reflect(ModuleManager.prototype, "ModuleManager");
    /**
     * 主题解析器
     */
    var ThemeAdapter = (function () {
        function ThemeAdapter() {
        }
        /**
         * 解析主题
         * @param url 待解析的主题url
         * @param compFunc 解析完成回调函数，示例：compFunc(e:egret.Event):void;
         * @param errorFunc 解析失败回调函数，示例：errorFunc():void;
         * @param thisObject 回调的this引用
         */
        ThemeAdapter.prototype.getTheme = function (url, compFunc, errorFunc, thisObject) {
            function onGetRes(e) {
                compFunc.call(thisObject, e);
            }
            function onError(e) {
                if (e.resItem.url == url) {
                    RES.removeEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onError, null);
                    errorFunc.call(thisObject);
                }
            }
            RES.addEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onError, null);
            RES.getResByUrl(url, onGetRes, this, RES.ResourceItem.TYPE_TEXT);
        };
        return ThemeAdapter;
    }());
    __reflect(ThemeAdapter.prototype, "ThemeAdapter", ["eui.IThemeAdapter"]);
    /**
     * 素材解析器
     */
    var AssetAdapter = (function () {
        function AssetAdapter() {
        }
        /**
         * @language zh_CN
         * 解析素材
         * @param source 待解析的新素材标识符
         * @param compFunc 解析完成回调函数，示例：callBack(content:any,source:string):void;
         * @param thisObject callBack的 this 引用
         */
        AssetAdapter.prototype.getAsset = function (source, compFunc, thisObject) {
            function onGetRes(data) {
                compFunc.call(thisObject, data, source);
            }
            if (RES.hasRes(source)) {
                var data = RES.getRes(source);
                if (data) {
                    onGetRes(data);
                }
                else {
                    RES.getResAsync(source, onGetRes, this);
                }
            }
            else {
                RES.getResByUrl(source, onGetRes, this, RES.ResourceItem.TYPE_IMAGE);
            }
        };
        return AssetAdapter;
    }());
    __reflect(AssetAdapter.prototype, "AssetAdapter", ["eui.IAssetAdapter"]);
})(how || (how = {}));
//# sourceMappingURL=HowMain.js.map