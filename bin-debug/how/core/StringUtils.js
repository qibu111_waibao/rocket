var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 字符串工具类
     * @author 袁浩
     *
     */
    var StringUtils = (function () {
        function StringUtils() {
        }
        /**
         * 类似C#中的格式化字符串函数
         * 例：format("Hello {0}",world)
         * @param str {string} 要格式化的字符串
         * @param args {Array<any>} 参数列表
         * @returns {string} 格式化之后的字符串
         */
        StringUtils.format = function (str) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            var result = str;
            for (var i = 0; i < args.length; i++) {
                result = result.replace("{" + i + "}", args[i].toString());
            }
            return result;
        };
        /**
         * 判断是否是空字符串，null、undefined和""都会返回true
         * @returns {boolean} 是否是空字符串
         */
        StringUtils.isEmpty = function (value) {
            return value == null || value == undefined || value.length == 0;
        };
        /**
         * 去左右两端空格
         */
        StringUtils.trim = function (str) {
            return str.replace(/(^\s*)|(\s*$)/g, "");
        };
        /**
          * 白鹭专用字符串转富文本
          * 要求格式：
          * @parma str {string} 传入的字符串
          * @parma type {number} 消息类型 4：系统消息 8：玩家喊话
          * [#去掉“0x”的六位颜色值（16进制）]文本  例如：[#F7E1C2]欢迎来到《米乐汇德州扑克》的游戏世界！
          */
        StringUtils.textToRichText = function (str, type) {
            var reStr = [];
            if (type == 4) {
                if (str == null) {
                    return;
                }
                if (str.indexOf("[") != -1) {
                    var strArr = str.split("["); //划分字符串转出字串组
                    for (var i = 0; i < strArr.length; i++) {
                        if (i == 0) {
                            reStr.push({ text: strArr[i], style: {} }); //直接加人返回富文本数组
                        }
                        else {
                            var color;
                            var nowtext;
                            if (strArr[i].charAt(0) == "#") {
                                color = "0x" + strArr[i].substr(1, 6);
                                nowtext = strArr[i].substr(9); //从第九位开始为正式文本内容
                            }
                            reStr.push({ text: nowtext, style: { "textColor": Number(color) } }); //将整理好的富文本加人返回富文本数组
                        }
                    }
                }
                else {
                    reStr.push({ text: str, style: {} }); //直接加人返回富文本数组
                }
            }
            else {
                reStr.push({ text: str, style: {} });
            }
            return reStr;
        };
        return StringUtils;
    }());
    how.StringUtils = StringUtils;
    __reflect(StringUtils.prototype, "how.StringUtils");
})(how || (how = {}));
//# sourceMappingURL=StringUtils.js.map