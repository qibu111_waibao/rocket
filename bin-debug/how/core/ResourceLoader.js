var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 资源加载类，加载一批资源组
     * @author 袁浩
     *
     */
    var ResourceLoader = (function (_super) {
        __extends(ResourceLoader, _super);
        function ResourceLoader() {
            return _super.call(this) || this;
        }
        /**
         * 开始加载资源组
         * @param groups 资源组名称列表
         * @param onComplete 所有资源加载完成的回调
         * @param thisObject 回调方法的执行上下文
         * @param args 回调方法参数
         */
        ResourceLoader.prototype.loadGroups = function (groups, onComplete, thisObject) {
            if (onComplete === void 0) { onComplete = null; }
            if (thisObject === void 0) { thisObject = null; }
            var args = [];
            for (var _i = 3; _i < arguments.length; _i++) {
                args[_i - 3] = arguments[_i];
            }
            this.groups = groups;
            this.onComplete = onComplete;
            this.thisObject = thisObject;
            this.args = args;
            RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
            this.loadGroup();
        };
        ResourceLoader.prototype.loadGroup = function () {
            var _this = this;
            if (this.groups && this.groups.length > 0) {
                var loadGroup = this.groups.shift();
                var themePath = how.HowMain.themeConfig ? how.HowMain.themeConfig[loadGroup] : null;
                if (themePath) {
                    EXML.load(how.Application.resourceVersion.getVirtualUrl(themePath), function (clazz, url) {
                        _this.loadGroup();
                    }, this, true);
                }
                else {
                    // if (!RES.isGroupLoaded(loadGroup)) {
                    RES.loadGroup(loadGroup);
                }
            }
            else {
                RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
                RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
                RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
                this.onAllGroupComplete();
            }
        };
        /**
        * 资源组加载出错
        */
        ResourceLoader.prototype.onResourceLoadError = function (event) {
            //忽略加载失败的项目
            this.onResourceLoadComplete(event);
            this.dispatchEvent(event);
            trace("资源[" + event.groupName + "]:加载失败");
        };
        /**
        * 资源组加载进度
        */
        ResourceLoader.prototype.onResourceProgress = function (event) {
            this.dispatchEvent(event);
        };
        /**
        * 资源组加载完成
        */
        ResourceLoader.prototype.onResourceLoadComplete = function (event) {
            trace("资源[" + event.groupName + "]:加载完成");
            this.dispatchEvent(event);
            this.loadGroup();
        };
        /**
         * 所有组资源加载完成
         */
        ResourceLoader.prototype.onAllGroupComplete = function () {
            if (this.onComplete) {
                this.onComplete.apply(this.thisObject, this.args);
            }
            this.dispatchEventWith(egret.Event.COMPLETE);
            this.onComplete = null;
            this.thisObject = null;
            this.args = null;
            this.groups = null;
        };
        /**
         * 清除引用
         */
        ResourceLoader.prototype.destroy = function () {
            RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.removeEventListener(RES.ResourceEvent.GROUP_LOAD_ERROR, this.onResourceLoadError, this);
            RES.removeEventListener(RES.ResourceEvent.GROUP_PROGRESS, this.onResourceProgress, this);
        };
        return ResourceLoader;
    }(egret.EventDispatcher));
    how.ResourceLoader = ResourceLoader;
    __reflect(ResourceLoader.prototype, "how.ResourceLoader");
})(how || (how = {}));
//# sourceMappingURL=ResourceLoader.js.map