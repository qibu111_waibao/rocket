var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 声音管理器
     * 必须要预加载所有声音文件
     */
    var SoundManager = (function () {
        function SoundManager() {
        }
        Object.defineProperty(SoundManager, "musicVolume", {
            get: function () {
                return this._musicVolume;
            },
            set: function (value) {
                this._musicVolume = value;
                if (how.Utils.isIOSNative()) {
                    // if (this.musicVolume > 0 && this.currentMusicSource) {
                    //     this.playMusic(this.currentMusicSource);
                    //     // if (window["webkit"]) {
                    //     //     window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playMusic", 2: "bg_mp3" }));
                    //     // }
                    //     // else {
                    //     //     window["sound"]("playMusic", "bg_mp3");
                    //     // }
                    // }
                    // else {
                    //     this.stopMusic();
                    //     // if (window["webkit"]) {
                    //     //     window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "stopMusic", 2: "" }));
                    //     // }
                    //     // else {
                    //     //     window["sound"]("stopMusic");
                    //     // }
                    // }
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "setVolume", 2: value }));
                    }
                    else {
                        window["sound"]("setVolume", value);
                    }
                }
                else if (this.music) {
                    this.music.volume = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SoundManager, "effectVolume", {
            get: function () {
                return this._effectVolume;
            },
            set: function (value) {
                this._effectVolume = value;
                for (var i = 0; i < this.effectList.length; i++) {
                    this.effectList[i].volume = value;
                }
            },
            enumerable: true,
            configurable: true
        });
        SoundManager.init = function () {
            if (!this.isInited) {
                this.isInited = true;
                how.EventManager["getInstance"](this).addEventListener(how.Application.APPEVENT_PAUSE, this.onAppPause, this);
                how.EventManager["getInstance"](this).addEventListener(how.Application.APPEVENT_RESUME, this.onAppResume, this);
            }
        };
        SoundManager.onAppPause = function () {
            if (egret.Capabilities.isMobile && !how.Utils.isIOSNative()) {
                if (this.music) {
                    this.musicPostion = this.music.position;
                    this.music.stop();
                    this.music = null;
                }
                this.stopAllEffects();
            }
        };
        SoundManager.onAppResume = function () {
            if (egret.Capabilities.isMobile && !how.Utils.isIOSNative()) {
                if (this.musicSource) {
                    this.playMusic(this.musicSource, this.musicLoop, this.musicPostion);
                }
            }
        };
        /**
         * 播放音乐，建议用mp3格式
         * @param source 相对于resource的音乐资源路径
         * @param loop 是否循环播放
         */
        SoundManager.playMusic = function (source, loop, startTime) {
            var _this = this;
            if (loop === void 0) { loop = true; }
            if (startTime === void 0) { startTime = 0; }
            this.init();
            this.currentMusicSource = source;
            if (how.Utils.isIOSNative()) {
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playMusic", 2: source }));
                }
                else {
                    window["sound"]("playMusic", source);
                }
            }
            else {
                if (this.music) {
                    this.music.stop();
                }
                this.musicSource = source;
                this.musicLoop = loop;
                var sound = RES.getRes(source);
                if (sound) {
                    sound.type = egret.Sound.MUSIC;
                    this.music = sound.play(startTime, loop ? 0 : 1);
                    this.music.volume = this._musicVolume;
                    this.music.once(egret.Event.SOUND_COMPLETE, function (event) {
                        _this.music = null;
                    }, this);
                }
                else {
                    RES.getResAsync(source, function (data, key) {
                        data.type = egret.Sound.MUSIC;
                        if (_this.music) {
                            _this.music.stop();
                        }
                        _this.music = data.play();
                        _this.music.volume = _this.musicVolume;
                    }, this);
                }
            }
        };
        /**
         * 停止播放音乐
         */
        SoundManager.stopMusic = function () {
            if (how.Utils.isIOSNative()) {
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "stopMusic", 2: "" }));
                }
                else {
                    window["sound"]("stopMusic");
                }
            }
            else {
                this.musicSource = null;
                this.music.stop();
                this.music = null;
            }
        };
        /**
         * 播放音效，建议用mp3格式
         * @param source 相对于resource的音效资源路径
         * @param loop 是否循环播放
         */
        SoundManager.playEffect = function (source, loop) {
            var _this = this;
            if (loop === void 0) { loop = false; }
            if (how.Utils.isIOSNative()) {
                if (this.effectVolume) {
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playSound", 2: source }));
                    }
                    else {
                        window["sound"]("playSound", source);
                    }
                    return;
                }
            }
            else {
                this.init();
                var sound = RES.getRes(source);
                if (sound) {
                    sound.type = egret.Sound.EFFECT;
                    var effect = sound.play(0, loop ? 0 : 1);
                    effect.volume = this._effectVolume;
                    this.effectList.push(effect);
                    effect.once(egret.Event.SOUND_COMPLETE, function (event) {
                        var index = _this.effectList.indexOf(effect);
                        if (index != -1) {
                            _this.effectList.splice(index, 1);
                        }
                    }, this);
                }
                else {
                    RES.getResAsync(source, function (data, key) {
                        if (data) {
                            data.type = egret.Sound.EFFECT;
                            var effect = data.play(0, loop ? 0 : 1);
                            effect.volume = _this._effectVolume;
                            _this.effectList.push(effect);
                            effect.once(egret.Event.SOUND_COMPLETE, function (event) {
                                var index = _this.effectList.indexOf(effect);
                                if (index != -1) {
                                    _this.effectList.splice(index, 1);
                                }
                            }, _this);
                        }
                    }, this);
                }
            }
        };
        /**
         * 停止播放所有音效
         */
        SoundManager.stopAllEffects = function () {
            while (this.effectList.length) {
                this.effectList.shift().stop();
            }
        };
        return SoundManager;
    }());
    SoundManager.effectList = [];
    SoundManager._musicVolume = 1;
    SoundManager._effectVolume = 1;
    how.SoundManager = SoundManager;
    __reflect(SoundManager.prototype, "how.SoundManager");
})(how || (how = {}));
//# sourceMappingURL=SoundManager.js.map