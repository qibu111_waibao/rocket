var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 声音工具类
     * @author 袁浩
     *
     */
    var SoundUtils = (function () {
        function SoundUtils() {
        }
        Object.defineProperty(SoundUtils, "musicVolume", {
            /**
            * 音乐音量
            */
            get: function () {
                return this._musicVolume;
            },
            set: function (value) {
                this._musicVolume = value;
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    egret.ExternalInterface.call("playMusic", "||" + this.musicVolume);
                    return;
                }
                if (this.music) {
                    this.music.volume = this.musicVolume;
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 播放音乐，更换音乐需要先暂停
         */
        SoundUtils.playMusic = function (path) {
            var _this = this;
            // this.continueMusic();
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                var rsi = how.Application.resourceConfig.getResourceItem(path);
                if (!rsi) {
                    return;
                }
                var url = rsi.url;
                var argpath = how.Application.resourceVersion.getVirtualUrl(url);
                egret.ExternalInterface.call("playMusic", argpath + "||" + this.musicVolume);
                return;
            }
            if (RES.hasRes(path)) {
                var sound = RES.getRes(path);
                if (sound) {
                    sound.type = egret.Sound.MUSIC;
                    this.music = sound.play();
                    this.music.volume = this.musicVolume;
                }
                else {
                    RES.getResAsync(path, function (data, key) {
                        data.type = egret.Sound.MUSIC;
                        if (_this.music) {
                            _this.music.stop();
                        }
                        _this.music = data.play();
                        _this.music.volume = _this.musicVolume;
                    }, this);
                }
            }
            else {
                var sound = new egret.Sound();
                sound.type = egret.Sound.MUSIC;
                sound.once(egret.Event.COMPLETE, function (event) {
                    sound.type = egret.Sound.MUSIC;
                    _this.music = sound.play();
                    _this.music.volume = _this.musicVolume;
                }, this);
                sound.load(path);
            }
        };
        SoundUtils.stopMusic = function () {
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                egret.ExternalInterface.call("playMusic", "||" + this.musicVolume);
                return;
            }
            if (!this.music) {
                return;
            }
            this.music.stop();
            this.music = null;
        };
        SoundUtils.initSound = function (path, type) {
            var sound = RES.getRes(path);
            if (!sound) {
                return;
            }
            sound.type = type;
            if (type == egret.Sound.MUSIC) {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    var rsi = how.Application.resourceConfig.getResourceItem(path);
                    if (!rsi) {
                        return;
                    }
                    var url = rsi.url;
                    var argpath = how.Application.resourceVersion.getVirtualUrl(url);
                    egret.ExternalInterface.call("playMusic", argpath + "||" + this.musicVolume);
                }
                else {
                    this.music = sound.play();
                    this.music.volume = this.musicVolume;
                }
            }
            else {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    var rsi = how.Application.resourceConfig.getResourceItem(path);
                    if (!rsi) {
                        return;
                    }
                    var url = rsi.url;
                    var argpath = how.Application.resourceVersion.getVirtualUrl(url);
                    egret.ExternalInterface.call("playSound", argpath + "||" + this.effectVolume);
                }
                else {
                    sound.play(0, 1).volume = this.effectVolume;
                }
            }
        };
        SoundUtils.playEffect = function (path) {
            var _this = this;
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                var rsi = how.Application.resourceConfig.getResourceItem(path);
                if (!rsi) {
                    return;
                }
                var url = rsi.url;
                var argpath = how.Application.resourceVersion.getVirtualUrl(url);
                egret.ExternalInterface.call("playSound", argpath + "||" + this.effectVolume);
                return;
            }
            if (RES.hasRes(path)) {
                if (RES.getRes(path)) {
                    this.initSound(path, egret.Sound.EFFECT);
                }
                else {
                    RES.getResAsync(path, function (data, key) {
                        _this.initSound(path, egret.Sound.EFFECT);
                    }, this);
                }
            }
            else {
                var sound = new egret.Sound();
                sound.type = egret.Sound.EFFECT;
                sound.once(egret.Event.COMPLETE, function (event) {
                    sound.type = egret.Sound.EFFECT;
                    sound.play().volume = _this.effectVolume;
                }, this);
                sound.load(path);
            }
        };
        //进入游戏场暂停音乐
        SoundUtils.pauseMusic = function () {
            this._pauseVolume = this.musicVolume;
            this.musicVolume = 0;
        };
        //进入主场景继续音乐
        SoundUtils.continueMusic = function () {
            if (this._pauseVolume != -1) {
                this.musicVolume = this._pauseVolume;
                this._pauseVolume = -1;
            }
        };
        return SoundUtils;
    }());
    /**
     * 音效音量
     */
    SoundUtils.effectVolume = 1;
    SoundUtils._musicVolume = 1;
    SoundUtils._pauseVolume = -1;
    how.SoundUtils = SoundUtils;
    __reflect(SoundUtils.prototype, "how.SoundUtils");
})(how || (how = {}));
//# sourceMappingURL=SoundUtils.js.map