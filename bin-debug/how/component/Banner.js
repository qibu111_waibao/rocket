var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 标语提示组件，用一个自上而下的横幅来显示信息。
     * 和弹窗、对话框不同，他不会影响用户交互，也没有交互产生，用于一些弱提示。
     * 一定时间会自动消失，如果弹出多个则上一个消失及时会暂停，直到新的消失才会继续启用。
     * @author 袁浩
     *
     */
    var Banner = (function (_super) {
        __extends(Banner, _super);
        function Banner(message, okHandler, thisObject) {
            var _this = _super.call(this) || this;
            _this.runTime = 0; //已经运行了多久
            _this.skinName = how.Banner.skinName;
            _this.message = message;
            _this.horizontalCenter = 0;
            _this.okHandler = okHandler;
            _this.thisObject = thisObject;
            return _this;
        }
        Banner.prototype.childrenCreated = function () {
            this.messageLabel.text = this.message;
            //            this.cacheAsBitmap = true;
            this.y = 260 + this.height;
            this.alpha = 0;
            egret.Tween.get(this).to({ y: 260, alpha: 1 }, 200);
            this.startTime = egret.getTimer();
            this.playTimer();
        };
        /**
         * 停止标语消失倒计时
         */
        Banner.prototype.pauseTimer = function () {
            egret.clearTimeout(this.timeID);
            var nowTime = egret.getTimer();
            this.runTime = nowTime - this.startTime;
        };
        /**
         * 继续标语消失倒计时
         */
        Banner.prototype.playTimer = function () {
            this.timeID = egret.setTimeout(this.onTimeOut, this, Banner.delay - this.runTime);
        };
        /**
         * 重置标语倒计时
         */
        Banner.prototype.replayTimer = function () {
            this.pauseTimer();
            this.startTime = egret.getTimer();
            this.playTimer();
        };
        /**
         * 关闭本标语
         */
        Banner.prototype.close = function () {
            how.Application.closeWindow(this);
            egret.clearTimeout(this.timeID);
            Banner.bannerList.pop();
            if (Banner.bannerList.length) {
                var banner = Banner.bannerList[Banner.bannerList.length - 1];
                banner.playTimer(); //继续之前的标语的倒计时
            }
            if (this.okHandler) {
                this.okHandler.apply(this.thisObject);
            }
        };
        Banner.prototype.onTimeOut = function () {
            egret.Tween.get(this).to({ y: 260 - this.height, alpha: 0 }, 200).call(this.close, this);
        };
        /**
        * 初始化对话框，在游戏启动的时候调用
        * @param skinName {string} 弹窗皮肤
        */
        Banner.init = function (skinName, delay) {
            if (delay === void 0) { delay = 2000; }
            this.skinName = skinName;
            this.delay = delay;
            this.bannerList = new Array();
        };
        /**
        * 显示一个弹窗
        * @param message {string} 提示文字
        * @returns {how.Banner} 弹窗的实例
        */
        Banner.show = function (message, okHandler, thisObject) {
            if (!this.skinName) {
                warn("Banner标语未初始化，将不会被显示，请先调用how.Banner.init()。");
            }
            var banner = new how.Banner(message, okHandler, thisObject);
            if (Banner.bannerList.length) {
                var currentBanner = Banner.bannerList[Banner.bannerList.length - 1];
                currentBanner.pauseTimer(); //停止当前标语的消失倒计时
                if (currentBanner.message == message && currentBanner.okHandler == okHandler && currentBanner.thisObject == thisObject) {
                    currentBanner.replayTimer();
                }
            }
            this.bannerList.push(banner);
            how.Application.addWindow(banner, false, false, false, false);
            return banner;
        };
        return Banner;
    }(eui.Component));
    how.Banner = Banner;
    __reflect(Banner.prototype, "how.Banner");
})(how || (how = {}));
//# sourceMappingURL=Banner.js.map