var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 文本
     * @author 王源智
     */
    var Label = (function (_super) {
        __extends(Label, _super);
        function Label() {
            return _super.call(this) || this;
        }
        Label.prototype.childrenCreated = function () {
            this.fontFamily = "微软雅黑";
        };
        return Label;
    }(eui.Label));
    how.Label = Label;
    __reflect(Label.prototype, "how.Label");
})(how || (how = {}));
//# sourceMappingURL=Label.js.map