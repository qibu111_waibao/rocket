var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 文本输入框
     * @author 王源智
     */
    var TextInput = (function (_super) {
        __extends(TextInput, _super);
        function TextInput() {
            return _super.call(this) || this;
        }
        TextInput.prototype.childrenCreated = function () {
            //            this.textDisplay.textColor = 0xffffff;
            //            this.promptDisplay.textColor = 0xa9a9a9;
            //            this.textDisplay.displayAsPassword = this._displayAsPassword;
        };
        return TextInput;
    }(eui.TextInput));
    how.TextInput = TextInput;
    __reflect(TextInput.prototype, "how.TextInput");
})(how || (how = {}));
//# sourceMappingURL=TextInput.js.map