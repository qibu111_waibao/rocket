var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 序列帧动画组件，可加入到布局
     * 官方提供的工具是egret.MovieClip类型的，当时感觉无法配合GUI来布局，所以这里提供了自定义的
     * 目前看来，应该官方提供的也可以参与布局，尽管如此，动画组件还是用这个类，后续把官方支持集成进来就行
     * 一轮播放完成会抛出egret.Event.COMPLETE事件
     * @author 袁浩
     *
     */
    var Animation = (function (_super) {
        __extends(Animation, _super);
        function Animation(sourceJSON, nameReg) {
            var _this = _super.call(this) || this;
            _this.sourceGroup = "";
            /**
             * 名称过滤，即图集中只有相关的名称资源才加入序列帧列表中
             */
            _this.nameReg = "demo\\d+_png";
            /**
             * 是否自动播放
             */
            _this.autoPlay = true;
            /**
             * 是否循环播放
             */
            _this.loop = true;
            /**
             * 动画帧频
             */
            _this.frameRate = 24;
            _this.frames = new Array();
            _this._currentFrame = -1;
            _this._isPlaying = false;
            _this.addEventListener(egret.Event.REMOVED_FROM_STAGE, _this.onRemoved, _this);
            _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAdded, _this);
            _this._sourceJSON = sourceJSON;
            _this.nameReg = nameReg; //"demo\\d+_png"
            return _this;
        }
        Object.defineProperty(Animation.prototype, "sourceJSON", {
            get: function () {
                return this._sourceJSON;
            },
            set: function (value) {
                this._sourceJSON = value;
                this.getResources();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Animation.prototype, "isPlaying", {
            /**
             * 是否正在播放
             */
            get: function () {
                return this._isPlaying;
            },
            enumerable: true,
            configurable: true
        });
        Animation.prototype.onRemoved = function (event) {
            this.stop();
        };
        Animation.prototype.onAdded = function (event) {
            this.refresh();
        };
        Animation.prototype.childrenCreated = function () {
            this.getResources();
        };
        Animation.prototype.getResources = function () {
            if (this.sourceJSON) {
                var url = Animation.resourcePath + "/" + this.sourceJSON;
                RES.getResByUrl(url, this.onComplete, this);
            }
            else {
                var resourceList = RES.getGroupByName(this.sourceGroup);
                for (var i = 0; i < resourceList.length; i++) {
                    if (!this.nameReg || this.nameReg.length == 0 || new RegExp(this.nameReg).test(resourceList[i].name)) {
                        this.frames.push(resourceList[i].name);
                    }
                }
                this.frames.sort(function (a, b) {
                    var anum = parseInt(a.replace(/[^0-9]/ig, ""));
                    var bnum = parseInt(b.replace(/[^0-9]/ig, ""));
                    if (anum > bnum) {
                        return 1;
                    }
                    else if (anum < bnum) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                });
            }
        };
        /**
         * 当图集资源加载完成时
         * @param data {string} 获取到的数据
         * @param url {string} 加载的地址
         *
         */
        Animation.prototype.onComplete = function (data, url) {
            if (!data) {
                return;
            }
            this.frames = new Array();
            var frames = data.frames;
            for (var key in frames) {
                if (!this.nameReg || this.nameReg.length == 0 || new RegExp(this.nameReg).test(key)) {
                    this.frames.push(key);
                }
            }
            this.frames.sort(function (a, b) {
                var anum = parseInt(a.replace(/[^0-9]/ig, ""));
                var bnum = parseInt(b.replace(/[^0-9]/ig, ""));
                if (anum > bnum) {
                    return 1;
                }
                else if (anum < bnum) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            this.refresh();
        };
        /**
         * 播放动画
         */
        Animation.prototype.play = function () {
            if (this._isPlaying) {
                this.stop();
            }
            this.timerID = egret.setInterval(this.onInterval, this, 1000 / this.frameRate);
            this._isPlaying = true;
        };
        /**
         * 停止动画
         */
        Animation.prototype.stop = function () {
            this._currentFrame = 0;
            egret.clearInterval(this.timerID);
            this._isPlaying = true;
        };
        Object.defineProperty(Animation.prototype, "currentFrame", {
            /**
             * 帧动画当前所在帧帧
             */
            get: function () {
                return this._currentFrame;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 刷新动画
         */
        Animation.prototype.refresh = function () {
            this.stop();
            if (this.autoPlay) {
                this.onInterval();
                this.play();
            }
            else {
                this.source = this.frames[this.currentFrame];
            }
        };
        /**
         * 播放下一帧
         */
        Animation.prototype.playNext = function () {
            this.onInterval();
        };
        Animation.prototype.onInterval = function () {
            if (how.Utils.checkRealVisible(this)) {
                this.source = this.frames[this.currentFrame];
                this._currentFrame++;
                if (this._currentFrame == this.frames.length) {
                    if (this.loop) {
                        this._currentFrame = 0;
                    }
                    else {
                        this.stop();
                    }
                    if (!this.completeEvent) {
                        this.completeEvent = new egret.Event(egret.Event.ENDED);
                    }
                    this.dispatchEvent(this.completeEvent); //抛出一轮播放完成事件
                }
            }
        };
        return Animation;
    }(eui.Image));
    /**
     * 资源根目录
     */
    Animation.resourcePath = "resource";
    how.Animation = Animation;
    __reflect(Animation.prototype, "how.Animation");
})(how || (how = {}));
//# sourceMappingURL=Animation.js.map