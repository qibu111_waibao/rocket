var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 帧动画按钮
     * @author 王源智
     */
    var AnimationButton = (function (_super) {
        __extends(AnimationButton, _super);
        function AnimationButton() {
            return _super.call(this) || this;
        }
        Object.defineProperty(AnimationButton.prototype, "getanimDisplay", {
            get: function () {
                return this.animDisplay;
            },
            enumerable: true,
            configurable: true
        });
        return AnimationButton;
    }(how.Button));
    how.AnimationButton = AnimationButton;
    __reflect(AnimationButton.prototype, "how.AnimationButton");
})(how || (how = {}));
//# sourceMappingURL=AnimationButton.js.map