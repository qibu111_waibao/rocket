var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 可编辑文本
     * @author 王源智
     */
    var EditableText = (function (_super) {
        __extends(EditableText, _super);
        function EditableText() {
            return _super.call(this) || this;
        }
        EditableText.prototype.childrenCreated = function () {
            this.fontFamily = "微软雅黑";
        };
        return EditableText;
    }(eui.EditableText));
    how.EditableText = EditableText;
    __reflect(EditableText.prototype, "how.EditableText");
})(how || (how = {}));
//# sourceMappingURL=EditableText.js.map