var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 日志界面
     * @author 袁浩
     *
     */
    var Log = (function (_super) {
        __extends(Log, _super);
        function Log() {
            var _this = _super.call(this) || this;
            _this._htmlText = "";
            /**
             * HtmlTextParser解析最大行数
             */
            _this.maxLength = 40;
            _this.touchChildren = _this.touchEnabled = false;
            _this.initChild();
            return _this;
        }
        Object.defineProperty(Log.prototype, "label", {
            /**
             * 日志普通文本
             */
            get: function () {
                return this._label;
            },
            set: function (value) {
                this._label = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Log.prototype, "htmlText", {
            /**
             * 日志当前的html文本
             */
            get: function () {
                return this._htmlText;
            },
            set: function (value) {
                this._htmlText = value;
                var textElementList = (new egret.HtmlTextParser).parser(value);
                if (textElementList.length >= this.maxLength) {
                    textElementList = textElementList.slice(textElementList.length - this.maxLength);
                }
                this.label.textFlow = textElementList;
            },
            enumerable: true,
            configurable: true
        });
        Log.prototype.initChild = function () {
            var rect = new eui.Rect();
            rect.fillColor = 0xFFFFFF;
            rect.fillAlpha = 0.2;
            rect.strokeWeight = 0;
            rect.percentWidth = rect.percentHeight = 100;
            this.addChild(rect);
            this._label = new eui.Label();
            this._label.wordWrap = true;
            this._label.bottom = 0;
            this._label.verticalAlign = egret.VerticalAlign.BOTTOM;
            this.addChild(this._label);
        };
        Object.defineProperty(Log, "useLog", {
            get: function () {
                return this._useLog;
            },
            /**
             * 隐藏/显示日志
             */
            set: function (value) {
                if (!useConsoleLog) {
                    return;
                }
                this._useLog = value;
                this.init();
                if (!value && this.log.stage) {
                    this.log.parent.removeChild(this.log);
                }
                else if (value) {
                    egret.MainContext.instance.stage.addChild(this.log);
                }
            },
            enumerable: true,
            configurable: true
        });
        Log.init = function () {
            this.log = this.log || new Log();
            if (this.log.parent) {
                this.log.parent.setChildIndex(this.log, this.log.parent.numChildren);
            }
        };
        Log.trace = function (message) {
            this.init();
            this.log.htmlText += '<font color="#FFFFFF" size="18">' + message + '\n</font>';
        };
        Log.warn = function (message) {
            this.init();
            this.log.htmlText += '<font color="#ffff00" size="18">' + message + '\n</font>';
        };
        Log.error = function (message) {
            this.init();
            this.log.htmlText += '<font color="#ff0000" size="18">' + message + '\n</font>';
        };
        return Log;
    }(eui.UILayer));
    how.Log = Log;
    __reflect(Log.prototype, "how.Log");
})(how || (how = {}));
//# sourceMappingURL=Log.js.map