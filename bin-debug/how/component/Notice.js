var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 公告组件，用一个存在一定时间的横幅来显示重要的消息。
     * 如果文字内容过长，会自左向右滚动，直到显示完毕。
     * 一定时间会自动消失，如果期间有多条消息需要展示，那么会排队等待上一个消失，也没有交互产生。
     * @author 王锡铜
     *
     */
    var Notice = (function (_super) {
        __extends(Notice, _super);
        function Notice(message, okHandler, thisObject, type) {
            if (type === void 0) { type = 8; }
            var _this = _super.call(this) || this;
            _this.left = _this.right = _this.top = _this.bottom = 0;
            _this.skinName = how.Notice.skinName;
            _this.message = message;
            _this.okHandler = okHandler;
            _this.thisObject = thisObject;
            _this.masstype = type;
            _this.touchEnabled = false;
            _this.touchEnabled = _this.touchChildren = false;
            return _this;
        }
        Notice.prototype.childrenCreated = function () {
            // this.chatLabel.textFlow = how.StringUtils.textToRichText(this.message, this.masstype);
            if ((typeof this.message) == "string") {
                this.chatLabel.text = this.message;
            }
            else {
                this.chatLabel.textFlow = this.message;
            }
            this.timeStart();
        };
        /**
         * 计时开始
         */
        Notice.prototype.timeStart = function () {
            this.isShow = true;
            // if (this.chatLabel.width > this.width - this.labelScroller.right - this.labelScroller.left) {//如果label的长度大于控件长度 则 使用tween
            var runTime = this.chatLabel.text.length * how.Notice.everyWordTime; //以字符数量计算滚动时间
            this.chatLabel.x = this.width - this.labelScroller.right - this.labelScroller.left;
            var tw = egret.Tween.get(this.chatLabel);
            tw.to({ x: -this.chatLabel.width }, runTime);
            this.timeOut = egret.setTimeout(function () {
                this.close();
            }, this, runTime);
            // } else {
            //     this.timeOut = egret.setTimeout(function () {
            //         this.close();
            //     }, this, how.Notice.delay);
            // }
        };
        /**
         * 计时结束
         */
        Notice.prototype.close = function () {
            this.isShow = false;
            how.Application.closeWindow(this);
            egret.clearTimeout(this.timeOut);
            how.Notice.noticeList.shift();
            if (this.okHandler) {
                this.okHandler.apply(this.thisObject);
            }
        };
        /**
        * 初始化公告，在游戏启动的时候调用
        * @param skinName {string} 公告皮肤
        * @param delay {number} 无需滚动的情况下 Notice的持续时间
        * @param everyWordTime {number} 需要滚动的情况下 每个字符的时间
        */
        Notice.init = function (skinName, delay, everyWordTime) {
            if (delay === void 0) { delay = 5000; }
            if (everyWordTime === void 0) { everyWordTime = 500; }
            this.skinName = skinName;
            this.delay = delay;
            this.everyWordTime = everyWordTime;
            this.noticeList = new Array();
        };
        /**
         * 显示一个Notice
         * @param massage {string} 公告内容
         * @param massType {number} 消息类型 4：系统消息（优先级高）  8：喊话消息（优先级低）
         * @param okHandler {Function} 显示完成后的回调函数
         * @param thisObject {any} 上下文
         */
        Notice.show = function (massage, massType, okHandler, thisObject) {
            if (massType === void 0) { massType = 8; }
            if (!this.skinName) {
                warn("Notic公告未初始化，将不会被显示，请先调用how.Notice.init()。");
            }
            if (massage == null) {
                return;
            }
            var notice = new how.Notice(massage, function () {
                if (okHandler) {
                    okHandler.apply(thisObject);
                }
                if (this.noticeList[0]) {
                    var timeOut = egret.setTimeout(function () {
                        how.Application.addWindow(this.noticeList[0], false, false, false, false);
                    }, this, 500);
                }
            }, this, massType);
            //notice优先级判断
            if (massType == 8) {
                this.noticeList.push(notice); //notice插入队列末端
            }
            else {
                this.noticeList.splice(1, 0, notice); //notice插入队列前端
            }
            if (!this.noticeList[0].isShow) {
                how.Application.addWindow(this.noticeList[0], false, false, false, false);
            }
            return notice;
        };
        return Notice;
    }(eui.Component));
    how.Notice = Notice;
    __reflect(Notice.prototype, "how.Notice");
})(how || (how = {}));
//# sourceMappingURL=Notice.js.map