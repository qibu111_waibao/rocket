var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    var DateChooser = (function (_super) {
        __extends(DateChooser, _super);
        function DateChooser() {
            var _this = _super.call(this) || this;
            _this._selectedDate = _this._selectedDate || new Date();
            _this.addEventListener(eui.UIEvent.CREATION_COMPLETE, _this.onCreationComplete, _this);
            return _this;
        }
        Object.defineProperty(DateChooser.prototype, "selectedDate", {
            get: function () {
                return this._selectedDate;
            },
            set: function (value) {
                this._selectedDate = value;
                if (this.dayList) {
                    this.onCreationComplete();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DateChooser.prototype, "days", {
            /**
             * 一共有多少天
             */
            get: function () {
                return this.getDaysInMonth();
            },
            enumerable: true,
            configurable: true
        });
        DateChooser.prototype.onCreationComplete = function () {
            this.weekList.dataProvider = this.weekList.dataProvider || new eui.ArrayCollection(new Array(7));
            this.weekList.selectedIndex = this._selectedDate.getDay() == 0 ? 7 : this._selectedDate.getDay() - 1;
            this.weekList.touchEnabled = this.weekList.touchChildren = false;
            this.dayList.dataProvider = this.getDays();
        };
        /**
         * 刷新数据
         */
        DateChooser.prototype.refresh = function () {
            this.onCreationComplete();
        };
        /**
         * 获取日期列表数据
         */
        DateChooser.prototype.getDays = function () {
            var today = this._selectedDate.getDate();
            var fristWeek = this.getFirstWeek();
            fristWeek = fristWeek == 0 ? 6 : fristWeek - 1;
            var days = new Array(42);
            var lastDayIndex = this.getDaysInMonth() + fristWeek - 1;
            for (var i = 0; i < 42; i++) {
                if (i < fristWeek || i > lastDayIndex) {
                    days[i] = null;
                }
                else {
                    days[i] = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth(), i - fristWeek + 1);
                }
            }
            return new eui.ArrayCollection(days);
        };
        /**
         * 获取当月第一天是周几
         */
        DateChooser.prototype.getFirstWeek = function () {
            var date = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth(), this._selectedDate.getDate());
            date.setDate(1);
            return date.getDay();
        };
        /**
         * 获取当月一共多少天
         */
        DateChooser.prototype.getDaysInMonth = function () {
            var date = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth() + 1, this._selectedDate.getDate());
            date.setDate(0);
            return date.getDate();
        };
        return DateChooser;
    }(eui.Component));
    how.DateChooser = DateChooser;
    __reflect(DateChooser.prototype, "how.DateChooser");
})(how || (how = {}));
//# sourceMappingURL=DateChooser.js.map