var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * how组件工具类
     * @author 袁浩
     *
     */
    var ComponentUtils = (function () {
        /**
         * how组件工具类，提供批量初始化组件功能
         */
        function ComponentUtils() {
        }
        /**
         * 初始化how组件
         */
        ComponentUtils.init = function (alertSkin, dialogSkin, bannerSkin, noticeSkin, loaddingSkin, callBack, thisObject) {
            if (thisObject === void 0) { thisObject = null; }
            if (!how.HowMain.themeTogether) {
                var resourceLoader = new how.ResourceLoader();
                resourceLoader.loadGroups([alertSkin, dialogSkin, bannerSkin, noticeSkin, loaddingSkin], function () {
                    how.Alert.init(alertSkin);
                    how.Dialog.init(dialogSkin);
                    how.Banner.init(bannerSkin);
                    how.Notice.init(noticeSkin);
                    how.Loadding.init(loaddingSkin);
                    callBack.apply(thisObject);
                }, thisObject);
            }
            else {
                how.Alert.init(alertSkin);
                how.Dialog.init(dialogSkin);
                how.Banner.init(bannerSkin);
                how.Notice.init(noticeSkin);
                how.Loadding.init(loaddingSkin);
                callBack.apply(thisObject);
            }
        };
        return ComponentUtils;
    }());
    how.ComponentUtils = ComponentUtils;
    __reflect(ComponentUtils.prototype, "how.ComponentUtils");
})(how || (how = {}));
//# sourceMappingURL=Component.js.map