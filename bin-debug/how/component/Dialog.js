var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 带信息提示、确认按钮和取消按钮的对话框组件。
     * 可同时存在多个。
     * @author 袁浩
     *
     */
    var Dialog = (function (_super) {
        __extends(Dialog, _super);
        function Dialog(message, title, buttonLable, okHandler, cancelHandler, thisObject) {
            var _this = _super.call(this) || this;
            _this.skinName = how.Dialog.skinName;
            _this.message = message;
            _this.title = title;
            _this.buttonLable = buttonLable;
            _this.okHandler = okHandler;
            _this.cancelHandler = cancelHandler;
            _this.thisObject = thisObject;
            return _this;
        }
        Dialog.prototype.childrenCreated = function () {
            this.txtTitle.text = this.title;
            var arrBtnLabel = this.buttonLable ? this.buttonLable.split("|") : [];
            this.okButton.label = this.buttonLable || this.okButton.label;
            this.cancelButton.label = this.buttonLable || this.cancelButton.label;
            this.messageLabel.text = this.message;
            this.okButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onOKButton, this);
            this.cancelButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCancelButton, this);
        };
        Dialog.prototype.onOKButton = function (event) {
            this.okButton.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onOKButton, this);
            how.Application.closeWindow(this);
            if (this.okHandler) {
                this.okHandler.apply(this.thisObject);
            }
        };
        Dialog.prototype.onCancelButton = function (event) {
            this.cancelButton.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onCancelButton, this);
            how.Application.closeWindow(this);
            if (this.cancelHandler) {
                this.cancelHandler.apply(this.thisObject);
            }
        };
        /**
         * 初始化对话框，在游戏启动的时候调用
         * @param skinName {string} 弹窗皮肤
         */
        Dialog.init = function (skinName) {
            this.skinName = skinName;
        };
        /**
         * 显示一个对话框
         * @param message {string} 提示文字
         * @param okHandler {Function} 按下确定按钮执行的方法
         * @param cancelHandler {Function} 按下取消按钮执行的方法
         * @param thisObject {any} 按下按钮执行的方法的上下文
         * @returns {how.Dialog} 对话框的实例
         */
        Dialog.show = function (message, okHandler, cancelHandler, thisObject, title, buttonLable) {
            title = title || how.Dialog.titleString;
            buttonLable = buttonLable || how.Dialog.buttonLableString;
            if (!this.skinName) {
                warn("Dialog弹窗未初始化，将不会被显示，请先调用how.Dialog.init()。");
            }
            var dialog = new how.Dialog(message, title, buttonLable, okHandler, cancelHandler, thisObject);
            how.Application.addWindow(dialog);
            return dialog;
        };
        return Dialog;
    }(eui.Component));
    how.Dialog = Dialog;
    __reflect(Dialog.prototype, "how.Dialog");
})(how || (how = {}));
//# sourceMappingURL=Dialog.js.map