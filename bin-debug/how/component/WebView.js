var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * html控件，可以显示html内容
     * @author 袁浩
     *
     */
    var WebView = (function (_super) {
        __extends(WebView, _super);
        function WebView() {
            var _this = _super.call(this) || this;
            /**
             * 是否每帧都渲染还是只渲染一次
             */
            _this.alwaysRender = true;
            /**
             * 是否一直在顶层渲染
             */
            _this.alwaysInFront = true;
            return _this;
        }
        Object.defineProperty(WebView.prototype, "source", {
            get: function () {
                return this._source;
            },
            set: function (value) {
                this._source = value;
                if (this.webRenderer) {
                    if (egret.Capabilities.os == "iOS") {
                        this.webRenderer.getElementsByTagName("iframe")[0].src = value;
                    }
                    else {
                        this.webRenderer["src"] = value;
                    }
                }
                else if (this.nativeRenderer) {
                    this.nativeRenderer.src = value;
                    if (how.Utils.isIOSNative()) {
                        if (window["webkit"]) {
                            window["webkit"].messageHandlers.web.postMessage(JSON.stringify({ 1: "updateWebView", 2: JSON.stringify(this.nativeRenderer) }));
                        }
                        else {
                            window["web"]("updateWebView", JSON.stringify(this.nativeRenderer));
                        }
                    }
                    else {
                        egret.ExternalInterface.call("updateWebView", JSON.stringify(this.nativeRenderer));
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebView.prototype, "html", {
            get: function () {
                return this._html;
            },
            set: function (value) {
                if (this.webRenderer) {
                    if (egret.Capabilities.os == "iOS") {
                        this.webRenderer.getElementsByTagName("iframe")[0].contentDocument.body.innerHTML = value;
                    }
                    else {
                        this.webRenderer["contentDocument"].body.innerHTML = value;
                    }
                }
                else if (this.nativeRenderer) {
                    egret.ExternalInterface.call("setHTML", value);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebView.prototype, "appScaleX", {
            get: function () {
                return this.appWidth / this.stage.stageWidth;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebView.prototype, "appScaleY", {
            get: function () {
                return this.appHeight / this.stage.stageHeight;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebView.prototype, "appWidth", {
            get: function () {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
                    return document.getElementsByClassName("egret-player")[0]["scrollWidth"];
                }
                else {
                    return egret_native.EGTView.getFrameWidth();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebView.prototype, "appHeight", {
            get: function () {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
                    return document.getElementsByClassName("egret-player")[0]["scrollHeight"];
                }
                else {
                    return egret_native.EGTView.getFrameHeight();
                }
            },
            enumerable: true,
            configurable: true
        });
        WebView.prototype.render = function () {
            if (how.Utils.isIOSNative()) {
                this.nativeRenderer = new NativeRenderer();
                this.nativeRenderer.src = this.source;
                this.renderTransform();
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.web.postMessage(JSON.stringify({ 1: "addWebView", 2: JSON.stringify(this.nativeRenderer) }));
                }
                else {
                    window["web"]("addWebView", JSON.stringify(this.nativeRenderer));
                }
            }
            else if (egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
                if (egret.Capabilities.os == "iOS") {
                    this.webRenderer = document.createElement("div");
                    var iframe = document.createElement("iframe");
                    ;
                    iframe.frameBorder = "0";
                    this.webRenderer.style.position = "absolute";
                    this.webRenderer.appendChild(iframe);
                    this.renderTransform();
                    document.body.appendChild(this.webRenderer);
                    iframe.src = this.source;
                }
                else {
                    this.webRenderer = document.createElement("iframe");
                    this.webRenderer["frameBorder"] = "0";
                    this.webRenderer.style.position = "absolute";
                    this.renderTransform();
                    document.body.appendChild(this.webRenderer);
                    this.webRenderer["src"] = this.source;
                }
            }
            else {
                this.nativeRenderer = new NativeRenderer();
                this.nativeRenderer.src = this.source;
                this.renderTransform();
                egret.ExternalInterface.call("addWebView", JSON.stringify(this.nativeRenderer));
            }
        };
        WebView.prototype.$invalidateTransform = function () {
            _super.prototype.$invalidateTransform.call(this);
            this.renderTransform();
        };
        WebView.prototype.renderTransform = function () {
            if (!this.stage) {
                return;
            }
            var localPoint = this.localToGlobal();
            var realX = localPoint.x * this.appScaleX;
            var realWidth = this.width * this.appScaleX * this.scaleX;
            var realY = localPoint.y * this.appScaleY;
            var realHeight = this.height * this.appScaleY * this.scaleY;
            if (this.webRenderer) {
                document.body.scrollTop = 0;
                this.webRenderer.style.left = realX + "px";
                this.webRenderer.style.top = realY + "px";
                this.webRenderer.style.width = realWidth + "px";
                this.webRenderer.style.height = realHeight + "px";
                this.webRenderer.style.zIndex = this.alwaysInFront ? "1" : "-1"; //-webkit-overflow-scrolling:touch; overflow:auto
                this.webRenderer.style["webkitOverflowScrolling"] = "touch";
                this.webRenderer.style.overflow = "auto";
            }
            else if (this.nativeRenderer) {
                this.nativeRenderer.x = realX;
                this.nativeRenderer.y = realY;
                this.nativeRenderer.width = realWidth;
                this.nativeRenderer.height = realHeight;
                if (how.Utils.isIOSNative()) {
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.web.postMessage(JSON.stringify({ 1: "updateWebView", 2: JSON.stringify(this.nativeRenderer) }));
                    }
                    else {
                        window["web"]("updateWebView", JSON.stringify(this.nativeRenderer));
                    }
                }
                else {
                    egret.ExternalInterface.call("updateWebView", JSON.stringify(this.nativeRenderer));
                }
            }
        };
        WebView.prototype.$onAddToStage = function (stage, nestLevel) {
            _super.prototype.$onAddToStage.call(this, stage, nestLevel);
            egret.setTimeout(this.render, this, 50);
            // this.render();
            // if(this.alwaysRender && !how.Utils.isIOSNative()) {
            //     this.addEventListener(egret.Event.ENTER_FRAME,this.onEnterFrame,this);
            // }
        };
        WebView.prototype.onEnterFrame = function (event) {
            this.renderTransform();
        };
        WebView.prototype.$onRemoveFromStage = function () {
            _super.prototype.$onRemoveFromStage.call(this);
            if (this.webRenderer) {
                document.body.removeChild(this.webRenderer);
                this.webRenderer = null;
            }
            else if (this.nativeRenderer) {
                if (how.Utils.isIOSNative()) {
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.web.postMessage(JSON.stringify({ 1: "removeWebView", 2: "" }));
                    }
                    else {
                        window["web"]("removeWebView");
                    }
                }
                else {
                    egret.ExternalInterface.call("removeWebView", "");
                }
            }
        };
        return WebView;
    }(egret.DisplayObject));
    how.WebView = WebView;
    __reflect(WebView.prototype, "how.WebView");
    var NativeRenderer = (function () {
        function NativeRenderer() {
            this.x = 0;
            this.y = 0;
            this.width = 0;
            this.height = 0;
        }
        return NativeRenderer;
    }());
    how.NativeRenderer = NativeRenderer;
    __reflect(NativeRenderer.prototype, "how.NativeRenderer");
})(how || (how = {}));
//# sourceMappingURL=WebView.js.map