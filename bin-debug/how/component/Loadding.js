var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var how;
(function (how) {
    /**
     * 载入中
     * @author 袁浩
     *
     */
    var Loadding = (function (_super) {
        __extends(Loadding, _super);
        function Loadding() {
            var _this = _super.call(this) || this;
            _this.skinName = Loadding.skinName;
            _this.left = _this.right = _this.top = _this.bottom = 0;
            return _this;
        }
        /**
         * 初始化弹窗，在游戏启动的时候调用
         * @param skinName {string} 弹窗皮肤
         */
        Loadding.init = function (skinName) {
            this.skinName = skinName;
        };
        return Loadding;
    }(eui.Component));
    how.Loadding = Loadding;
    __reflect(Loadding.prototype, "how.Loadding");
})(how || (how = {}));
//# sourceMappingURL=Loadding.js.map