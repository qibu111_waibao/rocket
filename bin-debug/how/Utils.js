var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * 游戏工具箱
     * @author 袁浩
     *
     */
    var Utils = (function () {
        function Utils() {
        }
        /**
        * md5方式加密字符串
        */
        Utils.md5 = function (str) {
            this.md5Object = this.md5Object || new md5();
            return this.md5Object.hex_md5(str);
        };
        /**
        * 将数值保留2位小数后格式化成金额形式
        * @param num 数值(Number或者String)
        * @type {String}
        */
        Utils.formatCurrency = function (num) {
            num = parseFloat(num);
            var result;
            if (num < 10000) {
                result = this.toFixed(num, 0);
            }
            else if (num < 100000000) {
                var n = num / 10000;
                result = this.isInteger(n) ? n.toString() + "万" : this.toFixed(n, 0) + "万";
            }
            else if (num < 1000000000000) {
                var n = num / 100000000;
                result = this.isInteger(n) ? n.toString() + "亿" : this.toFixed(n, 0) + "亿";
            }
            else {
                var n = num / 1000000000000;
                result = this.isInteger(n) ? n.toString() + "兆" : this.toFixed(n, 0) + "兆";
            }
            return result;
        };
        /**
        * 没有四舍五入的fix方法
        */
        Utils.toFixed = function (value, length) {
            value = value.toString();
            var pointIndex = value.lastIndexOf(".");
            if (pointIndex == -1) {
                return value;
            }
            if (length > 0) {
                return value.substring(0, pointIndex + length + 1);
            }
            else {
                return value.substring(0, pointIndex + length);
            }
        };
        /**
        * 判断是否是整型
        */
        Utils.isInteger = function (x) {
            return x % 1 === 0;
        };
        /**
        * 判断是否是整型
        */
        Utils.isFunction = function (func) {
            return typeof func == 'function';
        };
        /**
        * 判断是否是数组
        * */
        Utils.isArray = function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        };
        /**
        * 根据元素的属性和值获取数组中的元素
        */
        Utils.getItem = function (array, property, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][property] == value) {
                    return array[i];
                }
            }
            return null;
        };
        /**
        * 删除数组中的某项元素
        * */
        Utils.remove = function (array, item) {
            var index = array.indexOf(item);
            if (index > -1) {
                array.splice(index, 1);
            }
        };
        /**
        * 日期格式化
        */
        Utils.formatDate = function (date, fmt) {
            var o = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds() //毫秒   
            };
            if (/(y+)/.test(fmt))
                fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        };
        /*倒计时格式化*/
        Utils.formatCD = function (cd, fmt) {
            var d = Math.floor(cd / (24 * 3600));
            var h = Math.floor((cd - d * 24 * 3600) / 3600);
            var m = Math.floor((cd - d * 24 * 3600) % 3600 / 60);
            var s = Math.floor((cd - d * 24 * 3600) % 3600 % 60);
            var o = {
                "d+": d,
                "h+": h,
                "m+": m,
                "s+": s,
            };
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        };
        /**
        * 查找数组元素的索引
        */
        Utils.indexOf = function (array, item) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == item) {
                    return i;
                }
            }
            return -1;
        };
        /**
         * 拷贝数据到另一个对象中
         */
        Utils.copy = function (source, target, override) {
            if (override === void 0) { override = false; }
            if (source && target) {
                for (var key in source) {
                    if (target.hasOwnProperty(key) && override) {
                        target[key] = source[key];
                    }
                }
            }
        };
        Utils.checkRealVisible = function (display) {
            var parent = display;
            while (parent && !egret.is(parent, "egret.Stage")) {
                if (!parent.visible) {
                    return false;
                }
                parent = parent.parent;
            }
            return true;
        };
        /**
         * 移除指定类型的子控件
         * @param parent 父容器
         * @param type 类型
         * @param reverse 反转，只有指定类型的不会被移除
         */
        Utils.removeChildrenNotByType = function (parent, types, reverse) {
            var otherChildren = [];
            for (var i = 0; i < parent.numChildren; i++) {
                var children = parent.getChildAt(i);
                var canRemove = true;
                for (var i = 0; i < types.length; i++) {
                    canRemove = canRemove && egret.is(children, types[i]) != reverse;
                }
                if (canRemove) {
                    otherChildren.push(children);
                }
            }
            while (otherChildren.length) {
                parent.removeChild(otherChildren.shift());
            }
        };
        /**
         * 是否是IOS的native
         */
        Utils.isIOSNative = function () {
            return window.navigator && navigator.userAgent == "ios_miq";
        };
        Utils.threePoint = function (value, maxWidth) {
            var txt = new egret.TextField();
            txt.text = value;
            var result = value;
            var removeCount = 1;
            var pointCount = 0;
            while (txt.width > maxWidth) {
                result = value.substr(0, value.length - removeCount);
                pointCount++;
                pointCount = pointCount > 3 ? 3 : pointCount;
                for (var i = 0; i < pointCount; i++) {
                    result = result + ".";
                }
                removeCount++;
                txt.text = result;
            }
            return result;
        };
        return Utils;
    }());
    /**
    * 获取utf8字符串长度
    */
    Utils.getUtf8Length = function (str) {
        var cnt = 0;
        for (var i = 0; i < str.length; i++) {
            var value = str.charCodeAt(i);
            if (value < 0x080) {
                cnt += 1;
            }
            else if (value < 0x0800) {
                cnt += 2;
            }
            else {
                cnt += 3;
            }
        }
        return cnt;
    };
    /**
    * 获取指定范围的随机整数
    * 包含n到m
     */
    Utils.getRandom = function (n, m) {
        var c = m - n + 1;
        return Math.floor(Math.random() * c + n);
    };
    how.Utils = Utils;
    __reflect(Utils.prototype, "how.Utils");
})(how || (how = {}));
//# sourceMappingURL=Utils.js.map