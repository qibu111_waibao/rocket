var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var how;
(function (how) {
    /**
     * WebSocket管理器，收发websocket消息全部用这个类
     * 接受消息用how.EventManager，因为用WebSocketManager是无效的
     * 因为how.EventManager包含dispatcher而WebSocketManager没有，WebSocketManager是无法抛出事件的
     * @author 袁浩
     *
     */
    var WebSocketManager = (function () {
        /**
         * WebSocket管理器
         */
        function WebSocketManager() {
            this._isConnecting = false;
            this._connected = false;
            /**
            * 是否打印消息日志
            **/
            this.useLog = true;
            /**
             * 重连间隔
             */
            this._connectSign = 0;
            this._intervalHost = "";
            this._intervalPort = 0;
            WebSocketManager._beginTimer = egret.getTimer();
            this.resetWebSocket();
        }
        /**
         * 重置websocket，之前请先断开连接
         */
        WebSocketManager.prototype.resetWebSocket = function () {
            if (this.ws) {
                this.ws.removeEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this);
                this.ws.removeEventListener(egret.Event.CONNECT, this.onSocketOpen, this);
                this.ws.removeEventListener(egret.Event.CLOSE, this.onSocketClose, this);
                this.ws.removeEventListener(egret.IOErrorEvent.IO_ERROR, this.onSocketClose, this);
            }
            this.ws = new egret.WebSocket();
            this.ws.addEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this);
            this.ws.addEventListener(egret.Event.CONNECT, this.onSocketOpen, this);
            this.ws.addEventListener(egret.Event.CLOSE, this.onSocketClose, this);
            this.ws.addEventListener(egret.IOErrorEvent.IO_ERROR, this.onSocketClose, this);
        };
        /**
         * WebSocket管理器单例
         */
        WebSocketManager.getInstance = function () {
            if (!this._instance) {
                this._instance = new WebSocketManager();
            }
            return this._instance;
        };
        Object.defineProperty(WebSocketManager.prototype, "isConnecting", {
            /**
            * 是否正在连接
            **/
            get: function () {
                return this._isConnecting;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebSocketManager.prototype, "connected", {
            /**
            * 是否已经连接
            **/
            get: function () {
                return this._connected;
            },
            enumerable: true,
            configurable: true
        });
        /**
        * 连接到指定的主机和端口
        * @param host {string} 主机ip或者域名
        * @param port {number} 主机端口号
        **/
        WebSocketManager.prototype.connect = function (host, port) {
            if (this._connectSign != 0) {
                this._intervalHost = host;
                this._intervalPort = port;
                return;
            }
            this._connectSign = egret.setTimeout(this.onReconnect, this, 2000);
            if (!this._connected && !this._isConnecting) {
                this.resetWebSocket();
                this._isConnecting = true;
                this._connected = false;
                this.ws.connect(host, port);
                trace("准备连接网络...");
            }
        };
        //网络多次重连间隔
        WebSocketManager.prototype.onReconnect = function () {
            this._connectSign = 0;
            if (this._intervalHost != "") {
                this.connect(this._intervalHost, this._intervalPort);
                this._intervalHost = "";
            }
        };
        /**
        * 关闭连接
        **/
        WebSocketManager.prototype.close = function () {
            if (this.ws.connected) {
                this.ws.close();
                this._isConnecting = false;
                this._connected = false;
            }
        };
        /**
        * 发送数据给服务器
        * @param cmd {string} 命令
        * @param host {any} 数据
        **/
        WebSocketManager.prototype.send = function (cmd, data) {
            if (this.ws.connected) {
                data = data || {};
                var m = parseInt(cmd.split(",")[0]);
                var s = parseInt(cmd.split(",")[1]);
                var d = { m: m, s: s, d: data };
                if (this.useLog) {
                    trace("发送：" + d.m + "," + d.s + " 数据：" + JSON.stringify(data) + "  time：" + (egret.getTimer() - WebSocketManager._beginTimer));
                }
                this.ws.writeUTF(JSON.stringify(d));
                this.ws.flush();
            }
            else {
                trace("网络已断开，无法再发送消息");
                how.EventManager["getInstance"](this).dispatchEvent(egret.Event.CLOSE);
            }
        };
        WebSocketManager.prototype.onReceiveMessage = function (event) {
            var result = this.ws.readUTF();
            //特判
            if (result.indexOf("43") != -1 && result.indexOf("106") != -1 && result.indexOf("ChipReqInfo") != -1) {
                result = result.substring(0, result.length - 4) + "]}}";
            }
            var resultData = JSON.parse(result);
            how.EventManager["getInstance"](this).dispatchEvent(event.type, result);
            if (this.useLog) {
                trace("收到：" + resultData.m + "," + resultData.s + " 数据：" + JSON.stringify(resultData.d) + "  time：" + (egret.getTimer() - WebSocketManager._beginTimer));
            }
            if (how.Utils.isArray(resultData.d) && (how.Application.version == "1.0.0" || how.Application.version == "1.0.0.0")) {
                warn("消息：resultData.m" + "," + resultData.s + "的数据接口为数组，在1.0.0.0版本中可能有BUG，请检查调试。");
            }
            how.EventManager["getInstance"](this).dispatchEvent(resultData.m + "," + resultData.s, resultData.d);
        };
        WebSocketManager.prototype.onSocketOpen = function (event) {
            this._isConnecting = false;
            this._connected = true;
            trace("网络已连接");
            how.EventManager["getInstance"](this).dispatchEvent(event.type);
        };
        WebSocketManager.prototype.onSocketClose = function (event) {
            if (this._connected || this._isConnecting) {
                this._isConnecting = false;
                this._connected = false;
                trace("网络已断开");
                how.EventManager["getInstance"](this).dispatchEvent(egret.Event.CLOSE);
            }
        };
        return WebSocketManager;
    }());
    how.WebSocketManager = WebSocketManager;
    __reflect(WebSocketManager.prototype, "how.WebSocketManager");
})(how || (how = {}));
//# sourceMappingURL=WebSocketManager.js.map