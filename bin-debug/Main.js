var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
* 主类，游戏入口
* @author 袁浩
*/
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = this;
        var groups = ["preload"];
        _this = _super.call(this, "zhajinhua", LoadingScene, "default.res.json", groups, 960, 640, LoaddingWindow) || this;
        how.Animation.resourcePath = "resource/packs";
        return _this;
        //以下代码注释掉就可以在PC浏览器自适应无黑边了
        // if (!egret.Capabilities.isMobile) {
        //     egret.MainContext.instance.stage.scaleMode = egret.StageScaleMode.SHOW_ALL;
        // }
    }
    Main.prototype.initSound = function () {
        this.once(egret.TouchEvent.TOUCH_TAP, function () {
            how.SoundManager.playEffect("windowopen_mp3");
            // how.SoundManager.stopAllEffects();
        }, this);
    };
    Main.prototype.start = function () {
        how.ComponentUtils.init("public.AlertSkin", "public.DialogSkin", "public.BannerSkin", "public.NoticeSkin", "public.LoaddingSkin", this.afterStart, this); //初始化通用组
    };
    Main.prototype.afterStart = function () {
        mouse.enable(this.stage); //初始化鼠标系统
        mouse.setMouseMoveEnabled(true);
        this.initSound(); //初始化音乐
        this.initLocalStorage(); //初始化本地数据
        this.stage.maxTouches = 1; //多点触摸禁止
        this.moduleManager.initModule(WelcomeModule, WelcomeView);
    };
    /*初始化本地数据，音乐，音效，震动，自动登录等*/
    Main.prototype.initLocalStorage = function () {
    };
    /**
    * 子类继承获取加载进度
    */
    Main.prototype.onLoaddingProgress = function (percent, current, total) {
        var loadingUI = this.loadingUI;
        loadingUI.setProgress(percent, current, total);
    };
    /**
     * 当所有资源组加载完成
     */
    Main.prototype.onAllGroupComplete = function () {
        var loadingUI = this.loadingUI;
        // loadingUI.setText(LanguageConfig.logining);//初始化样式
    };
    return Main;
}(how.HowMain));
__reflect(Main.prototype, "Main");
//# sourceMappingURL=Main.js.map