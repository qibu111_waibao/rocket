var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
/**
 * 语言配置
 * @author 袁浩
 *
 */
var LanguageConfig = (function () {
    function LanguageConfig() {
    }
    return LanguageConfig;
}());
LanguageConfig.loadProgress = "{0}%";
__reflect(LanguageConfig.prototype, "LanguageConfig");
//# sourceMappingURL=LanguageConfig.js.map