var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 教程界面
 * @author 袁浩
 */
var TeachView = (function (_super) {
    __extends(TeachView, _super);
    function TeachView() {
        var _this = _super.call(this) || this;
        _this.skinName = "TeachSkin";
        return _this;
    }
    TeachView.prototype.awake = function () {
        this.playCunzhangTween();
        this.teachButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTeachButton, this);
        this.gotoButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGotoButton, this);
    };
    TeachView.prototype.playButtonsTween = function (button, index) {
        egret.Tween.get(button).wait(index * GameConfig.cunzhangButtonsTime).to({ alpha: 1 }, GameConfig.cunzhangButtonsTime);
    };
    TeachView.prototype.playCunzhangTween = function () {
        var _this = this;
        this.helloButton.alpha = 0;
        this.teachButton.alpha = 0;
        this.gotoButton.alpha = 0;
        this.cunzhang.verticalCenter = this.stage.stageHeight / 2 + this.cunzhang.height / 2;
        egret.Tween.get(this.cunzhang).to({ verticalCenter: 0 }, 600, egret.Ease.circOut).call(function () {
            _this.playButtonsTween(_this.helloButton, 0);
            _this.playButtonsTween(_this.teachButton, 1);
            _this.playButtonsTween(_this.gotoButton, 2);
        }, this);
    };
    TeachView.prototype.onTeachButton = function () {
        this.report("onTeachButton");
    };
    TeachView.prototype.onGotoButton = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        this.helloButton.alpha = 0;
        this.teachButton.alpha = 0;
        this.gotoButton.alpha = 0;
        egret.Tween.get(this.cunzhang).to({ verticalCenter: -this.stage.stageHeight / 2 - this.cunzhang.height / 2 }, 600, egret.Ease.circIn).call(function () {
            _this.report("onGotoButton");
        }, this);
    };
    TeachView.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
    };
    return TeachView;
}(base.Scene));
__reflect(TeachView.prototype, "TeachView");
//# sourceMappingURL=TeachView.js.map