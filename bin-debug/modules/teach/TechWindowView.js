var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 教程窗口界面
 * @author 袁浩
 */
var TeachWindowView = (function (_super) {
    __extends(TeachWindowView, _super);
    function TeachWindowView() {
        var _this = _super.call(this) || this;
        _this.skinName = "TeachWindowSkin";
        return _this;
    }
    TeachWindowView.prototype.start = function () {
        this.teachLabel.text = RES.getRes("Text_json").teach;
    };
    return TeachWindowView;
}(base.Window));
__reflect(TeachWindowView.prototype, "TeachWindowView");
//# sourceMappingURL=TechWindowView.js.map