var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 教程模块
 */
var TeachModule = (function (_super) {
    __extends(TeachModule, _super);
    function TeachModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TeachModule.prototype.onTeachButton = function () {
        this.moduleManager.initModule(TeachWindowModule, TeachWindowView);
    };
    TeachModule.prototype.onGotoButton = function () {
        this.moduleManager.initModule(MainModule, MainView);
    };
    return TeachModule;
}(how.module.TSceneModule));
TeachModule.request = {
    onTeachButton: "onTeachButton",
    onGotoButton: "onGotoButton"
};
__reflect(TeachModule.prototype, "TeachModule");
//# sourceMappingURL=TeachModule.js.map