var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 实验室第二个游戏模块
 */
var LabGame1Module = (function (_super) {
    __extends(LabGame1Module, _super);
    function LabGame1Module() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return LabGame1Module;
}(base.GameModule));
__reflect(LabGame1Module.prototype, "LabGame1Module");
//# sourceMappingURL=LabGame1Module.js.map