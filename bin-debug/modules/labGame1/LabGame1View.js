var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 实验室第二个游戏界面
 */
var LabGame1View = (function (_super) {
    __extends(LabGame1View, _super);
    function LabGame1View() {
        var _this = _super.call(this) || this;
        _this.skinName = "LabGame1Skin";
        return _this;
    }
    Object.defineProperty(LabGame1View.prototype, "resourceList", {
        get: function () {
            return ["labGame1"];
        },
        enumerable: true,
        configurable: true
    });
    LabGame1View.prototype.awake = function () {
        this.initChazuo();
        this.initDengpao();
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
        this.checkIsWin();
    };
    LabGame1View.prototype.initChazuo = function () {
        var data = [1, 2, 3, 4, 0, 0];
        while (data.length) {
            var image = this.chazuoGroup.getChildAt(6 - data.length);
            var index = data.splice(Math.floor(how.Random.range(0, data.length)), 1)[0];
            image.source = "插座" + index + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onImageBegin, this);
            mouse.setButtonMode(image, true);
            if (index == 0) {
                image.y = 175;
            }
            else {
                image.y = 0;
            }
        }
    };
    LabGame1View.prototype.initDengpao = function () {
        var data = [1, 2, 3, 4, 0, 0];
        while (data.length) {
            var group = this.dengpaoGroup.getChildAt(6 - data.length);
            var image = group.getChildAt(0);
            var index = data.splice(Math.floor(how.Random.range(0, data.length)), 1)[0];
            if (index != 0) {
                image.source = "灯泡_没亮" + index + "_png";
            }
            else {
                image.source = "";
            }
        }
    };
    LabGame1View.prototype.onImageBegin = function (event) {
        var image = event.currentTarget;
        var imagePoint = image.localToGlobal();
        this.addChild(image);
        image.x = imagePoint.x;
        image.y = imagePoint.y;
        this.dragImgData = { img: image, imagePoint: imagePoint, stagePoint: new egret.Point(event.stageX, event.stageY) };
        egret.Tween.removeTweens(image);
    };
    LabGame1View.prototype.onTouchMove = function (event) {
        if (this.dragImgData) {
            var offsetX = event.stageX - this.dragImgData.stagePoint.x;
            // var offsetY = event.stageY - this.dragImgData.stagePoint.y;
            this.dragImgData.img.x = this.dragImgData.imagePoint.x + offsetX;
        }
    };
    LabGame1View.prototype.onTouchEnd = function (event) {
        if (this.dragImgData) {
            this.checkPoint();
        }
        this.dragImgData = null;
    };
    LabGame1View.prototype.checkPoint = function () {
        var dragImg = this.dragImgData.img;
        var minDistance = Number.MAX_VALUE;
        var minImage;
        var oldPoint = this.chazuoGroup.globalToLocal(this.dragImgData.imagePoint.x, this.dragImgData.imagePoint.y);
        var nowPoint = new egret.Point(dragImg.x, dragImg.y + dragImg.height);
        this.chazuoGroup.addChild(dragImg);
        dragImg.x = oldPoint.x;
        dragImg.y = oldPoint.y;
        for (var i = 0; i < this.chazuoGroup.numChildren; i++) {
            var image = this.chazuoGroup.getChildAt(i);
            var globalPoint = image.localToGlobal();
            globalPoint.y += image.height;
            var distance = egret.Point.distance(globalPoint, nowPoint);
            if (distance < minDistance) {
                minImage = image;
            }
            minDistance = distance < minDistance ? distance : minDistance;
        }
        dragImg.x = minImage.x;
        minImage.x = oldPoint.x;
        dragImg.y = dragImg.source == "插座0_png" ? 175 : 0;
        minImage.y = minImage.source == "插座0_png" ? 175 : 0;
        this.checkIsWin();
    };
    LabGame1View.prototype.checkIsWin = function () {
        var chazuos = [];
        for (var i = 0; i < this.chazuoGroup.numChildren; i++) {
            chazuos.push(this.chazuoGroup.getChildAt(i));
        }
        chazuos.sort(function (a, b) {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x > b.x) {
                return 1;
            }
            else {
                return 0;
            }
        });
        var successCount = 0;
        for (var i = 0; i < this.dengpaoGroup.numChildren; i++) {
            var group = this.dengpaoGroup.getChildAt(i);
            var image = group.getChildAt(0);
            var source = image.source.toString();
            if (image.source) {
                var index = source.substring(source.indexOf("亮") + 1, source.lastIndexOf("_"));
                if (chazuos[i].source == "插座" + index + "_png") {
                    image.source = "灯泡_亮" + index + "_png";
                    successCount++;
                }
                else {
                    image.source = "灯泡_没亮" + index + "_png";
                }
            }
        }
        if (successCount == 4) {
            this.win();
        }
    };
    LabGame1View.prototype.loss = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame1Loss", "游戏2失败", 0x3f0c3f);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 1, 1, false);
        }, this);
        this.stopTime();
    };
    LabGame1View.prototype.win = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame1Win", "游戏2成功", 0x3f0c3f);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 1, 1, true);
        }, this);
        this.stopTime();
    };
    LabGame1View.prototype.onTimeOver = function () {
        this.loss();
    };
    return LabGame1View;
}(base.Scene));
__reflect(LabGame1View.prototype, "LabGame1View");
//# sourceMappingURL=LabGame1View.js.map