var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 家第一个游戏界面
 */
var HomeGame0View = (function (_super) {
    __extends(HomeGame0View, _super);
    function HomeGame0View() {
        var _this = _super.call(this) || this;
        _this.sucessImages = [];
        _this.skinName = "HomeGame0Skin";
        return _this;
    }
    Object.defineProperty(HomeGame0View.prototype, "resourceList", {
        get: function () {
            return ["homeGame0"];
        },
        enumerable: true,
        configurable: true
    });
    HomeGame0View.prototype.start = function () {
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
        this.initImages();
    };
    HomeGame0View.prototype.initImages = function () {
        var data = [1, 2, 3, 4, 5, 6];
        while (data.length) {
            var image = this.imageList.getChildAt(6 - data.length);
            image.source = "拼图" + data.splice(Math.floor(how.Random.range(0, data.length)), 1) + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onImageBegin, this);
        }
        for (var i = 0; i < 6; i++) {
            var image = this.imageList.getChildAt(i);
            image.x = Math.round(how.Random.range(0, this.imageList.getChildAt(1).x));
            image.y = Math.round(how.Random.range(0, this.imageList.getChildAt(4).y));
        }
    };
    HomeGame0View.prototype.onImageBegin = function (event) {
        var image = event.currentTarget;
        var imagePoint = image.localToGlobal();
        this.addChild(image);
        image.x = imagePoint.x;
        image.y = imagePoint.y;
        this.dragImgData = { img: image, imagePoint: imagePoint, stagePoint: new egret.Point(event.stageX, event.stageY) };
        egret.Tween.removeTweens(image);
    };
    HomeGame0View.prototype.onTouchMove = function (event) {
        if (this.dragImgData) {
            var offsetX = event.stageX - this.dragImgData.stagePoint.x;
            var offsetY = event.stageY - this.dragImgData.stagePoint.y;
            this.dragImgData.img.x = this.dragImgData.imagePoint.x + offsetX;
            this.dragImgData.img.y = this.dragImgData.imagePoint.y + offsetY;
        }
    };
    HomeGame0View.prototype.onTouchEnd = function (event) {
        if (this.dragImgData) {
            this.checkPoint();
        }
        this.dragImgData = null;
    };
    HomeGame0View.prototype.checkPoint = function () {
        var dragImg = this.dragImgData.img;
        for (var i = 0; i < this.realList.numElements; i++) {
            var image = this.realList.getChildAt(i);
            var globalPoint = image.localToGlobal();
            if (Math.abs(globalPoint.x - dragImg.x) <= GameConfig.pingtuOffset && Math.abs(globalPoint.y - dragImg.y) <= GameConfig.pingtuOffset) {
                egret.Tween.get(dragImg).to({ x: globalPoint.x, y: globalPoint.y }, 500);
                if (image.source == dragImg.source) {
                    if (this.sucessImages.indexOf(dragImg) == -1) {
                        this.sucessImages.push(dragImg);
                    }
                    if (this.sucessImages.length == 6) {
                        this.win();
                    }
                }
                else {
                    if (this.sucessImages.indexOf(dragImg) != -1) {
                        this.sucessImages.splice(this.sucessImages.indexOf(dragImg), 1);
                    }
                }
                return;
            }
        }
        if (this.sucessImages.indexOf(dragImg) != -1) {
            this.sucessImages.splice(this.sucessImages.indexOf(dragImg), 1);
        }
    };
    HomeGame0View.prototype.win = function () {
        var _this = this;
        var mc = this.createFullMc("homeGame0Win", "游戏9成功", 0xf48b88, 2);
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 0, 0, true);
        }, this);
        this.stopTime();
    };
    HomeGame0View.prototype.loss = function () {
        var _this = this;
        var mc = this.createFullMc("homeGame0Loss", "游戏9失败", 0xf48b88);
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 0, 0, false);
        }, this);
        this.stopTime();
    };
    HomeGame0View.prototype.onTimeOver = function () {
        this.loss();
    };
    HomeGame0View.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
        this.stage.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.removeEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
    };
    return HomeGame0View;
}(base.Scene));
__reflect(HomeGame0View.prototype, "HomeGame0View");
//# sourceMappingURL=HomeGame0View.js.map