var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 工程第一个游戏界面
 */
var FactoryGame0View = (function (_super) {
    __extends(FactoryGame0View, _super);
    function FactoryGame0View() {
        var _this = _super.call(this) || this;
        /**
         * 不合格鞋子数量
         */
        _this.buhegeCount = 0;
        _this.skinName = "FactoryGame0Skin";
        return _this;
    }
    Object.defineProperty(FactoryGame0View.prototype, "resourceList", {
        get: function () {
            return ["factoryGame0"];
        },
        enumerable: true,
        configurable: true
    });
    FactoryGame0View.prototype.awake = function () {
        this.initImages();
    };
    FactoryGame0View.prototype.initImages = function () {
        for (var i = 0; i < this.shoesGroup.numChildren; i++) {
            var isJige = !!Math.round(how.Random.range(0, 1));
            var shoeIndex = Math.round(how.Random.range(1, 2));
            var jigeString = isJige ? "及格鞋" : "不及格鞋";
            var group = this.shoesGroup.getChildAt(i);
            var image = group.getChildAt(0);
            group.getChildAt(1).visible = false;
            image.source = jigeString + shoeIndex + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onImageTouch, this);
            if (!isJige) {
                this.buhegeCount++;
            }
        }
        var randomImage = this.shoesGroup.getChildAt(Math.round(how.Random.range(0, 5))).getChildAt(0);
        if (randomImage.source.toString().indexOf("不及格") != 0) {
            randomImage.source = "不及格鞋" + Math.round(how.Random.range(1, 2)) + "_png"; //保证至少有1个不及格鞋
            this.buhegeCount++;
        }
    };
    FactoryGame0View.prototype.onImageTouch = function (event) {
        var image = event.currentTarget;
        if (image.source.toString().indexOf("不及格") == 0 && !image.parent.getChildAt(1).visible) {
            image.parent.getChildAt(1).visible = true;
            this.buhegeCount--;
            if (this.buhegeCount == 0) {
                this.win();
            }
        }
    };
    FactoryGame0View.prototype.win = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame0Win", "游戏5成功", 0x0b9444);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 2, 0, true);
        }, this);
        this.stopTime();
    };
    FactoryGame0View.prototype.loss = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame0Loss", "游戏5失败", 0x0b9444);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 2, 0, false);
        }, this);
        this.stopTime();
    };
    FactoryGame0View.prototype.onTimeOver = function () {
        this.loss();
    };
    return FactoryGame0View;
}(base.Scene));
__reflect(FactoryGame0View.prototype, "FactoryGame0View");
//# sourceMappingURL=FactoryGame0View.js.map