var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 实验室第一个游戏界面
 */
var LabGame0View = (function (_super) {
    __extends(LabGame0View, _super);
    function LabGame0View() {
        var _this = _super.call(this) || this;
        _this.successCount = 0;
        _this.isShuidiRun = false;
        _this.skinName = "LabGame0Skin";
        return _this;
    }
    Object.defineProperty(LabGame0View.prototype, "resourceList", {
        get: function () {
            return ["labGame0"];
        },
        enumerable: true,
        configurable: true
    });
    LabGame0View.prototype.start = function () {
        this.startSanjiao();
        this.createAni();
        this.diguanButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDiguanButton, this);
    };
    LabGame0View.prototype.startSanjiao = function () {
        egret.Tween.get(this.sanjiao, { loop: true }).to({ y: this.sanjiao.parent.height - this.sanjiao.height }, GameConfig.sanjiaoTime / 2).to({ y: 0 }, GameConfig.sanjiaoTime / 2);
    };
    LabGame0View.prototype.checkLoss = function () {
        var pos = this.sanjiao.y + this.sanjiao.height / 2;
        return pos >= this.successArea.y && pos <= this.successArea.y + this.successArea.height;
    };
    LabGame0View.prototype.onDiguanButton = function () {
        var _this = this;
        if (this.checkLoss()) {
            if (!this.isShuidiRun) {
                var shuidi = new eui.Image();
                shuidi.source = this.shuidiImg.source;
                var point = this.shuidiImg.localToGlobal();
                shuidi.x = point.x;
                shuidi.y = point.y;
                this.addChild(shuidi);
                this.isShuidiRun = true;
                egret.Tween.get(shuidi).to({ y: this["rongye" + this.successCount].localToGlobal().y + this["rongye" + this.successCount].height - this.shuidiImg.height }, 300).call(function () {
                    _this["rongye" + _this.successCount].visible = true;
                    _this.isShuidiRun = false;
                    _this.removeChild(shuidi);
                    _this.successCount++;
                    if (_this.successCount == 3) {
                        _this.win();
                    }
                    else {
                        _this.ani.gotoAndPlay(0);
                    }
                }, this);
            }
        }
        else {
            this.loss();
        }
    };
    LabGame0View.prototype.createAni = function () {
        var group = new eui.Group();
        var mc = this.createMC("labGame0Drink", "游戏1喝东西", true, 1);
        group.addChild(mc);
        group.bottom = 410;
        group.horizontalCenter = 80;
        this.addChild(group);
        mc.gotoAndPlay(45);
        this.ani = mc;
    };
    LabGame0View.prototype.loss = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame0Loss", "游戏1失败", 0x1584be);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 1, 0, false);
        }, this);
        this.stopTime();
    };
    LabGame0View.prototype.win = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame0Win", "游戏1成功", 0x1584be);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 1, 0, true);
        }, this);
        this.stopTime();
    };
    LabGame0View.prototype.onTimeOver = function () {
        this.loss();
    };
    LabGame0View.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
        egret.Tween.removeAllTweens();
    };
    return LabGame0View;
}(base.Scene));
__reflect(LabGame0View.prototype, "LabGame0View");
//# sourceMappingURL=LabGame0View.js.map