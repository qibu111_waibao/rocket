var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 证书模块
 */
var ResumeModule = (function (_super) {
    __extends(ResumeModule, _super);
    function ResumeModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ResumeModule.prototype.guiAwake = function () {
        this.callUI("initUI", this.type, this.addScore);
        ResumeModule.currentScores[this.type] += this.addScore;
        if (this.addScore > 0) {
            if (ResumeModule.currentGameCount < 2) {
                ResumeModule.currentGameCount++;
            }
            else {
                trace("提升难度");
                ResumeModule.currentNandu++;
            }
        }
    };
    return ResumeModule;
}(how.module.TWindowModule));
ResumeModule.currentScores = [0, 0, 0];
ResumeModule.currentNandu = 1;
ResumeModule.teachComplete = [false, false, false];
ResumeModule.currentGameCount = 0;
__reflect(ResumeModule.prototype, "ResumeModule");
//# sourceMappingURL=ResumeModule.js.map