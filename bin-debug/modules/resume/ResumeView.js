var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 证书界面
 * @author 袁浩
 */
var ResumeView = (function (_super) {
    __extends(ResumeView, _super);
    function ResumeView() {
        var _this = _super.call(this) || this;
        _this.skinName = "ResumeSkin";
        return _this;
    }
    ResumeView.prototype.initUI = function (type, addScore) {
        var currentScores = ResumeModule.currentScores;
        for (var i = 0; i < currentScores.length; i++) {
            var rect = this["type" + i + "Rect"];
            rect.percentWidth = currentScores[i] / GameConfig.maxScore * 100;
            if (i == type) {
                var toPercent = (currentScores[i] + addScore) / GameConfig.maxScore * 100;
                toPercent = toPercent > 100 ? 100 : toPercent;
                egret.Tween.get(rect).to({ percentWidth: toPercent }, 300);
            }
            this["gameResume" + i].source = ResumeModule.teachComplete[i] ? "gameResume" + i + "_png" : "gameResume" + i + "_grey_png";
        }
    };
    return ResumeView;
}(base.Window));
__reflect(ResumeView.prototype, "ResumeView");
//# sourceMappingURL=ResumeView.js.map