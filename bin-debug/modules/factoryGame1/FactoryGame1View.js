var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 工程第二个游戏界面
 */
var FactoryGame1View = (function (_super) {
    __extends(FactoryGame1View, _super);
    function FactoryGame1View() {
        var _this = _super.call(this) || this;
        _this.currentStep = 0;
        _this.currentPercent = 0;
        _this.data = ["第一步_png", "第二步_png", "第三步_png"];
        _this.skinName = "FactoryGame1Skin";
        return _this;
    }
    Object.defineProperty(FactoryGame1View.prototype, "resourceList", {
        get: function () {
            return ["factoryGame1"];
        },
        enumerable: true,
        configurable: true
    });
    FactoryGame1View.prototype.start = function () {
        this.createImage();
        this.createAni();
    };
    FactoryGame1View.prototype.createAni = function () {
        var _this = this;
        var group = new eui.Group();
        var mc = this.createMC("qingxiAni", "清洗", true);
        mc.y = -mc.height;
        group.addChild(mc);
        group.width = mc.width;
        group.height = group.height;
        group.right = 0;
        group.verticalCenter = 0;
        this.addChild(group);
        mc.gotoAndPlay(45);
        this.ani = mc;
        mc.visible = false;
        this.ani.addEventListener(egret.Event.LOOP_COMPLETE, function () {
            _this.ani.visible = false;
        }, this);
    };
    FactoryGame1View.prototype.playAni = function () {
        this.ani.visible = true;
    };
    FactoryGame1View.prototype.createImage = function () {
        var group = this.buttonsGroup.getChildAt(this.currentStep);
        var image = group.getChildAt(0);
        image.source = this.data.length == 1 ? this.data[0] : this.data.splice(Math.round(how.Random.range(0, this.data.length - 1)), 1)[0];
        group.visible = true;
        this.setMask();
        group.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGroupTouch, this);
    };
    FactoryGame1View.prototype.setMask = function () {
        var group = this.buttonsGroup.getChildAt(this.currentStep);
        var fillIcon = group.getChildAt(1);
        if (group.numChildren == 3) {
            group.removeChildAt(2);
        }
        fillIcon.mask = null;
        var sector = how.DisplayUtils.getSector(fillIcon.width / 2, 0, this.currentPercent / 100 * 360);
        sector.x += fillIcon.width / 2;
        sector.y += fillIcon.height / 2;
        sector.touchEnabled = false;
        group.addChild(sector);
        fillIcon.mask = sector;
    };
    FactoryGame1View.prototype.setUpdate = function () {
        this.useUpdate = true;
        _super.prototype.setUpdate.call(this);
    };
    FactoryGame1View.prototype.update = function () {
        if (this.currentStep < 3) {
            this.currentPercent -= GameConfig.fasheJinduDijian;
            this.currentPercent = this.currentPercent < 0 ? 0 : this.currentPercent;
            this.setMask();
        }
    };
    FactoryGame1View.prototype.onGroupTouch = function (event) {
        this.playAni();
        this.currentPercent += GameConfig.fasheJinduStep;
        this.currentPercent = this.currentPercent > 100 ? 100 : this.currentPercent;
        this.setMask();
        if (this.currentPercent >= 100) {
            var group = event.currentTarget;
            group.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onGroupTouch, this);
            this.currentPercent = 0;
            this.currentStep++;
            if (this.currentStep < 3) {
                this.createImage();
            }
            else {
                this.win();
            }
        }
    };
    FactoryGame1View.prototype.win = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame1Win", "游戏8成功", 0xe9c7b5, 2);
        mc.x -= 50;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 2, 1, true);
        }, this);
        this.stopTime();
    };
    FactoryGame1View.prototype.loss = function () {
        var _this = this;
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame1Loss", "游戏8失败", 0xe9c7b5, 2);
        mc.x -= 50;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 2, 1, false);
        }, this);
        this.stopTime();
    };
    FactoryGame1View.prototype.onTimeOver = function () {
        this.loss();
    };
    return FactoryGame1View;
}(base.Scene));
__reflect(FactoryGame1View.prototype, "FactoryGame1View");
//# sourceMappingURL=FactoryGame1View.js.map