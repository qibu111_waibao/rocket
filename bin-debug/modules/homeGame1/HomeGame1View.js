var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 家第二个游戏界面
 */
var HomeGame1View = (function (_super) {
    __extends(HomeGame1View, _super);
    function HomeGame1View() {
        var _this = _super.call(this) || this;
        _this.paopaoCount = 0;
        _this.skinName = "HomeGame1Skin";
        return _this;
    }
    Object.defineProperty(HomeGame1View.prototype, "resourceList", {
        get: function () {
            return ["homeGame1"];
        },
        enumerable: true,
        configurable: true
    });
    /**
     * 重写开始倒计时方法，这关不需要时间倒计时
     */
    HomeGame1View.prototype.startCountdown = function () {
        this.timeID = egret.setInterval(this.createImg, this, GameConfig.paopaoNandu[this.nandu].paopaoInterval);
    };
    HomeGame1View.prototype.createImg = function () {
        var paopao = new eui.Image();
        paopao.x = how.Random.range(0, this.stage.stageWidth - 76);
        paopao.y = this.stage.stageHeight;
        paopao.source = "泡泡" + Math.round(how.Random.range(1, 3)) + "_png";
        this.addChild(paopao);
        egret.Tween.get(paopao, { onChange: this.onChange, onChangeObj: this }).to({ y: -76 }, GameConfig.paopaoNandu[this.nandu].paopaoMoveTime);
        paopao.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onPaopao, this);
    };
    HomeGame1View.prototype.onPaopao = function (event) {
        var _this = this;
        var paopao = event.currentTarget;
        egret.Tween.removeTweens(paopao);
        var paopaoName = paopao.source.toString().substring(0, 3);
        var mc = this.createMC(paopaoName + "Ani", paopaoName, true, 1);
        mc.addEventListener(egret.Event.COMPLETE, function () {
            if (mc.parent) {
                _this.removeChild(mc);
            }
            if (_this.paopaoCount == 10) {
                _this.win();
            }
        }, this);
        mc.x = paopao.x - 156;
        mc.y = paopao.y - 140; //134
        this.addChild(mc);
        this.removeChild(paopao);
        this.paopaoCount++;
        if (this.paopaoCount == 10) {
            this.stop();
        }
    };
    HomeGame1View.prototype.onChange = function (event, aa) {
        var tween = event.currentTarget;
        var paopao = tween["_target"];
        for (var i = 0; i < this.zhangaiGroup.numChildren; i++) {
            var zhangai = this.zhangaiGroup.getChildAt(i);
            var zhangaiGlobalPoint = zhangai.localToGlobal();
            var zhangaiRectangle = new egret.Rectangle(zhangaiGlobalPoint.x, zhangaiGlobalPoint.y, zhangai.width, zhangai.height);
            var paopaoRectangle = new egret.Rectangle(paopao.x, paopao.y, paopao.width, paopao.height);
            if (zhangaiRectangle.intersects(paopaoRectangle)) {
                this.loss();
                break;
            }
        }
    };
    HomeGame1View.prototype.loss = function () {
        var _this = this;
        this.stop();
        var mc = this.createFullMc("homeGame1Loss", "游戏3失败", 0x7fb0bd);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 0, 1, false);
        }, this);
        this.stopTime();
    };
    HomeGame1View.prototype.win = function () {
        var _this = this;
        this.stop();
        var mc = this.createFullMc("homeGame1Win", "游戏3成功", 0x7fb0bd);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, function () {
            _this.report("onGameOver", 0, 1, true);
        }, this);
        this.stopTime();
    };
    HomeGame1View.prototype.onTimeOver = function () {
        this.loss();
    };
    HomeGame1View.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
        this.stop();
    };
    HomeGame1View.prototype.stop = function () {
        egret.Tween.removeAllTweens();
        egret.clearInterval(this.timeID);
    };
    return HomeGame1View;
}(base.Scene));
__reflect(HomeGame1View.prototype, "HomeGame1View");
//# sourceMappingURL=HomeGame1View.js.map