var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 主界面模块
 */
var MainModule = (function (_super) {
    __extends(MainModule, _super);
    function MainModule() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.showTeach = true;
        return _this;
    }
    MainModule.prototype.onResumeButton = function (isWin, nandu, type) {
        if (nandu === void 0) { nandu = -1; }
        if (type === void 0) { type = 0; }
        this.moduleManager.initModule(ResumeModule, ResumeView, null, { addScore: isWin ? GameConfig.baseScore * (nandu + 1) : 0, type: type });
    };
    MainModule.prototype.addSayModule = function (type, gameData) {
        if (gameData === void 0) { gameData = null; }
        if (!gameData || gameData.nandu == 0) {
            this.moduleManager.initModule(SayModule, SayView, null, { type: type, gameData: gameData });
        }
        else {
            this.onResumeButton(gameData.isWin, gameData.nandu, type);
        }
    };
    MainModule.prototype.guiAwake = function () {
        this.callUI("setTeachVisible", this.showTeach);
        if (this.showSay) {
            this.addSayModule(this.showSay.sayType, this.showSay.gameData);
            this.showSay = null;
        }
    };
    MainModule.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
    };
    return MainModule;
}(how.module.TSceneModule));
MainModule.request = {
    onResumeButton: "onResumeButton",
    addSayModule: "addSayModule"
};
__reflect(MainModule.prototype, "MainModule");
//# sourceMappingURL=MainModule.js.map