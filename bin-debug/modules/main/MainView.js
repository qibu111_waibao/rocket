var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 主界面
 */
var MainView = (function (_super) {
    __extends(MainView, _super);
    function MainView() {
        var _this = _super.call(this) || this;
        _this.skinName = "MainSkin";
        return _this;
    }
    Object.defineProperty(MainView.prototype, "resourceList", {
        get: function () {
            return ["main"];
        },
        enumerable: true,
        configurable: true
    });
    MainView.prototype.awake = function () {
        var gongchang = this.createMC("gongchang", "工厂");
        gongchang.x = -140;
        gongchang.y = -200;
        this.gongchangGroup.addChild(gongchang);
        var shiyanshi = this.createMC("shiyanshi", "实验室");
        shiyanshi.x = -50;
        shiyanshi.y = -100;
        this.shiyanshiGroup.addChild(shiyanshi);
    };
    MainView.prototype.start = function () {
        this.playTween(this.homeIcon);
        this.playTween(this.factoryIcon);
        this.playTween(this.launchIcon);
        this.playTween(this.labIcon);
        this.iknowButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onIKnowButton, this);
        this.resumeButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResumeButton, this);
        this.homeGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onHomeGroup, this);
        this.labGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onLabGroup, this);
        this.factoryGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onFactoryGroup, this);
        this.rocketGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRocketGroup, this);
        mouse.setButtonMode(this.homeImg, true);
        mouse.setButtonMode(this.factoryImg, true);
        mouse.setButtonMode(this.launchImg, true);
        mouse.setButtonMode(this.labImg, true);
    };
    MainView.prototype.setTeachVisible = function (showTeach) {
        this.blackGroup.visible = this.tipGroup.visible = showTeach;
        if (showTeach) {
            for (var i = 0; i < 5; i++) {
                var lineImg = this["line" + i];
                var tipText = this["tipText" + i];
                lineImg.visible = false;
                tipText.alpha = 0;
            }
            this.iknowButton.alpha = 0;
            this.playTeachTweens(0);
        }
    };
    MainView.prototype.playTeachTweens = function (index) {
        var _this = this;
        if (index < 5) {
            var lineImg = this["line" + index];
            var tipText = this["tipText" + index];
            var oldWidth = lineImg.width;
            lineImg.width = 0;
            lineImg.visible = true;
            egret.Tween.get(lineImg).to({ width: oldWidth }, GameConfig.lineTime).call(function () {
                egret.Tween.get(tipText).to({ alpha: 1 }, GameConfig.tipTextTime).call(_this.playTeachTweens, _this, [index + 1]);
            }, this);
        }
        else {
            egret.Tween.get(this.iknowButton).to({ alpha: 1 }, GameConfig.iknowTime);
        }
    };
    MainView.prototype.onIKnowButton = function () {
        var _this = this;
        egret.Tween.get(this.tipGroup).to({ alpha: 0 }, 300).call(function () {
            _this.tipGroup.visible = false;
        }, this);
        egret.Tween.get(this.blackGroup).to({ alpha: 0 }, 300).call(function () {
            _this.blackGroup.visible = false;
        }, this);
    };
    MainView.prototype.playTween = function (icon) {
        icon.rotation = -30;
        egret.Tween.get(icon, { loop: true }).to({ rotation: 30 }, 1500).to({ rotation: -30 }, 1500);
    };
    MainView.prototype.setUpdate = function () {
        this.useUpdate = true;
        _super.prototype.setUpdate.call(this);
    };
    MainView.prototype.update = function () {
        this.hroadImg.left = this.troadImg.x + this.troadImg.height / 2 - 1;
    };
    MainView.prototype.onResumeButton = function () {
        this.report("onResumeButton");
    };
    MainView.prototype.onHomeGroup = function () {
        this.report("addSayModule", 0);
    };
    MainView.prototype.onLabGroup = function () {
        this.report("addSayModule", 1);
    };
    MainView.prototype.onFactoryGroup = function () {
        this.report("addSayModule", 2);
    };
    MainView.prototype.onRocketGroup = function () {
        this.report("addSayModule", 3);
    };
    MainView.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
        egret.Tween.removeAllTweens();
    };
    return MainView;
}(base.Scene));
__reflect(MainView.prototype, "MainView");
//# sourceMappingURL=MainView.js.map