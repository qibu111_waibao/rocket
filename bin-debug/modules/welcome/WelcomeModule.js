var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 主界面模块
 */
var WelcomeModule = (function (_super) {
    __extends(WelcomeModule, _super);
    function WelcomeModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WelcomeModule.prototype.onStartButton = function () {
        this.moduleManager.initModule(TeachModule, TeachView);
    };
    return WelcomeModule;
}(how.module.TSceneModule));
WelcomeModule.request = {
    onStartButton: "onStartButton"
};
__reflect(WelcomeModule.prototype, "WelcomeModule");
//# sourceMappingURL=WelcomeModule.js.map