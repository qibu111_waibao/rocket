var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 欢迎界面
 * @author 袁浩
 */
var WelcomeView = (function (_super) {
    __extends(WelcomeView, _super);
    function WelcomeView() {
        var _this = _super.call(this) || this;
        _this.skinName = "WelcomeSkin";
        return _this;
    }
    WelcomeView.prototype.start = function () {
        this.startButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStartButton, this);
    };
    WelcomeView.prototype.onStartButton = function () {
        this.report("onStartButton");
    };
    return WelcomeView;
}(base.Scene));
__reflect(WelcomeView.prototype, "WelcomeView");
//# sourceMappingURL=WelcomeView.js.map