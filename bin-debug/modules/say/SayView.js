var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 关卡介绍界面
 * @author 袁浩
 */
var SayView = (function (_super) {
    __extends(SayView, _super);
    function SayView() {
        var _this = _super.call(this) || this;
        _this.skinName = "SaySkin";
        return _this;
    }
    Object.defineProperty(SayView.prototype, "resourceList", {
        get: function () {
            return ["say"];
        },
        enumerable: true,
        configurable: true
    });
    SayView.prototype.awake = function () {
        this.textData = RES.getRes("Text_json");
    };
    SayView.prototype.start = function () {
        var _this = this;
        if (this.type != 3) {
            this.gameText0.text = this.textData["gameSay"][this.type][0];
            this.gameText1.text = this.textData["gameSay"][this.type][1];
        }
        this.closeButton0.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton4.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.gameIcon0.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.viewStack.selectedIndex = 2; _this.gameIndex = 0; _this.setText(0); }, this);
        this.gameIcon1.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.gameIndex = 1; _this.setText(0); _this.viewStack.selectedIndex = 2; }, this);
        this.nextButton0.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.viewStack.selectedIndex = 1; }, this);
        this.preButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.viewStack.selectedIndex = 1; }, this);
        this.nextButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { if (_this.type != 3) {
            _this.viewStack.selectedIndex = 3;
            _this.setText(1);
        }
        else {
            _this.onStartGame(_this.type, _this.gameIndex);
        } }, this);
        this.preButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.viewStack.selectedIndex = 2; }, this);
        this.preButton4.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.viewStack.selectedIndex = 3; }, this);
        this.nextButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.onStartGame(_this.type, _this.gameIndex); }, this);
        this.nextButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { _this.onStartGame(_this.type, _this.gameIndex); }, this);
        this.preButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onNextGame, this);
    };
    SayView.prototype.initUI = function (type, gameData) {
        this.type = type;
        if (gameData) {
            this.gameIndex = gameData.gameIndex;
        }
        if (this.type == 2 && this.gameIndex == 1 || (gameData && !gameData.isWin)) {
            this.preButton3.visible = false;
            this.viewStack.selectedIndex = 4;
            this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.complete : this.textData.gameOver.loss;
        }
        else if (gameData) {
            this.viewStack.selectedIndex = 4;
            if (this.gameIndex == 1) {
                this.preButton3.visible = false;
                this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.getResume : this.textData.gameOver.loss;
                ResumeModule.teachComplete[this.type] = gameData.isWin; //每关第二个游戏完成都会获得证书
            }
            else {
                this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.win : this.textData.gameOver.loss;
            }
        }
        if (this.type == 3) {
            this.preButton1.visible = false;
            this.nextButton1.getChildAt(1).source = "开始游戏_png";
        }
        for (var i = 0; i < 4; i++) {
            var closeButton = this["closeButton" + i];
            if (closeButton) {
                closeButton.getChildAt(0).source = "按钮" + type + "_png";
            }
            var preButton = this["preButton" + i];
            if (preButton) {
                preButton.getChildAt(0).source = "按钮" + type + "_png";
            }
            var nextButton = this["nextButton" + i];
            if (nextButton) {
                nextButton.getChildAt(0).source = "按钮" + type + "_png";
            }
        }
        if (type != 3) {
            this.gameIcon0.source = "图标_game" + type + "_0_png";
            this.gameIcon1.source = "图标_game" + type + "_1_png";
        }
        else {
            this.viewStack.selectedIndex = 2;
            this.gameIndex = 0;
            this.setText(0);
        }
        if (type == 0) {
            this.roleImg.source = "对话框_npcmainrole_png";
            this.rectBg.fillColor = 0xdff3fd;
        }
        else if (type == 1) {
            this.roleImg.source = "对话框_npclab_png";
            this.rectBg.fillColor = 0xCFEBEB;
        }
        else if (type == 2) {
            this.roleImg.source = "对话框_npcfactory_png";
            this.rectBg.fillColor = 0x97ABC7;
        }
        else if (type == 3) {
            this.roleImg.source = "对话框_npclauch_png";
            this.rectBg.fillColor = 0xA8AED8;
        }
    };
    SayView.prototype.onStartGame = function (type, gameIndex, nandu) {
        if (nandu === void 0) { nandu = 0; }
        if (type == 0) {
            this.report("onStartHomeGame" + gameIndex, nandu);
        }
        else if (type == 1) {
            this.report("onStartLabGame" + gameIndex, nandu);
        }
        else if (type == 2) {
            this.report("onStartFactoryGame" + gameIndex, nandu);
        }
        else if (type == 3) {
            // this.viewStack.selectedIndex = 5;
            this.createRandomGame();
        }
        if (type != 3) {
            how.WindowManager.getInstance().closeAll();
        }
    };
    SayView.prototype.createRandomGame = function () {
        var types = [0, 1, 2];
        for (var i = 0; i < 3; i++) {
            var type = types.splice(Math.floor(how.Random.range(0, types.length)), 1)[0];
            var gameIndex = Math.floor(how.Random.range(0, 2));
            // var image = this["randomGameIcon" + i] as eui.Image;
            // image.source = "图标_game" + type + "_" + gameIndex + "_png";
            ResumeModule.currentNandu = ResumeModule.currentNandu > GameConfig.nanduTime.length - 1 ? GameConfig.nanduTime.length - 1 : ResumeModule.currentNandu;
            this.onStartGame(type, gameIndex, ResumeModule.currentNandu);
            break;
        }
    };
    SayView.prototype.onNextGame = function () {
        if (this.gameIndex == 0) {
            this.gameIndex++;
        }
        else {
            this.gameIndex = 0;
            this.type++;
        }
        // this.onStartGame(this.type, this.gameIndex);
        this.setText(0);
        this.viewStack.selectedIndex = 2;
    };
    SayView.prototype.setText = function (index) {
        this["text" + index].text = this.textData["say"][this.type][this.gameIndex][index];
    };
    return SayView;
}(base.Window));
__reflect(SayView.prototype, "SayView");
//# sourceMappingURL=SayView.js.map