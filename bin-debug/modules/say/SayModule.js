var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 关卡介绍模块
 */
var SayModule = (function (_super) {
    __extends(SayModule, _super);
    function SayModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SayModule.prototype.onStartHomeGame0 = function (nandu) {
        this.moduleManager.initModule(HomeGame0Module, HomeGame0View, null, { nandu: nandu });
    };
    SayModule.prototype.onStartHomeGame1 = function (nandu) {
        this.moduleManager.initModule(HomeGame1Module, HomeGame1View, null, { nandu: nandu });
    };
    SayModule.prototype.onStartLabGame1 = function (nandu) {
        this.moduleManager.initModule(LabGame1Module, LabGame1View, null, { nandu: nandu });
    };
    SayModule.prototype.onStartLabGame0 = function (nandu) {
        this.moduleManager.initModule(LabGame0Module, LabGame0View, null, { nandu: nandu });
    };
    SayModule.prototype.onStartFactoryGame0 = function (nandu) {
        this.moduleManager.initModule(FactoryGame0Module, FactoryGame0View, null, { nandu: nandu });
    };
    SayModule.prototype.onStartFactoryGame1 = function (nandu) {
        this.moduleManager.initModule(FactoryGame1Module, FactoryGame1View, null, { nandu: nandu });
    };
    SayModule.prototype.guiAwake = function () {
        this.callUI("initUI", this.type, this.gameData);
    };
    return SayModule;
}(how.module.TWindowModule));
SayModule.request = {
    onStartHomeGame0: "onStartHomeGame0",
    onStartHomeGame1: "onStartHomeGame1",
    onStartLabGame0: "onStartLabGame0",
    onStartLabGame1: "onStartLabGame1",
    onStartFactoryGame0: "onStartFactoryGame0",
    onStartFactoryGame1: "onStartFactoryGame1"
};
__reflect(SayModule.prototype, "SayModule");
//# sourceMappingURL=SayModule.js.map