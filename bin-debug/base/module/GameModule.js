var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var base;
(function (base) {
    /**
     * 工厂第一个游戏模块
     */
    var GameModule = (function (_super) {
        __extends(GameModule, _super);
        function GameModule() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        GameModule.prototype.guiAwake = function () {
            this.callUI("initUI", this.nandu);
        };
        GameModule.prototype.onGameOver = function (type, gameIndex, isWin) {
            this.moduleManager.initModule(MainModule, MainView, null, { showTeach: false, showSay: { sayType: type, gameData: { isWin: isWin, gameIndex: gameIndex, nandu: this.nandu } } });
        };
        return GameModule;
    }(how.module.TSceneModule));
    GameModule.request = {
        onGameOver: "onGameOver"
    };
    base.GameModule = GameModule;
    __reflect(GameModule.prototype, "base.GameModule");
})(base || (base = {}));
//# sourceMappingURL=GameModule.js.map