var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var base;
(function (base) {
    /**
     * 载入中
     * @author 袁浩
     *
     */
    var Loadding = (function (_super) {
        __extends(Loadding, _super);
        function Loadding() {
            return _super.call(this) || this;
        }
        Loadding.prototype.childrenCreated = function () {
            this.addEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
            this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onStopRotation, this);
            // this.grpLoading.visible = false;
            // this._timeOut = egret.setTimeout(this.onDelayShow, this, 500);
        };
        Loadding.prototype.onDelayShow = function () {
            this.grpLoading.visible = true;
        };
        Loadding.prototype.onFrameRotation = function (event) {
            if (this.grpLoading) {
                this.grpLoading.rotation += 10;
            }
        };
        Loadding.prototype.onStopRotation = function (event) {
            this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        };
        Loadding.prototype.$onRemoveFromStage = function () {
            _super.prototype.$onRemoveFromStage.call(this);
            this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
            egret.clearTimeout(this._timeOut);
        };
        return Loadding;
    }(how.Loadding));
    base.Loadding = Loadding;
    __reflect(Loadding.prototype, "base.Loadding");
})(base || (base = {}));
//# sourceMappingURL=Loadding.js.map