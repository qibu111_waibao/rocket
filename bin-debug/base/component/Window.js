var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var base;
(function (base) {
    /**
     * 场景类中间层
     */
    var Window = (function (_super) {
        __extends(Window, _super);
        function Window() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Window.prototype.childrenCreated = function () {
            _super.prototype.childrenCreated.call(this);
            this.initCloseButton();
            // how.SoundManager.playEffect(SoundConfig.WINDOW_OPEN);
        };
        Window.prototype.initCloseButton = function () {
            if (this["closeBtn"]) {
                this["closeBtn"].addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
            }
            if (this["closeButton"]) {
                this["closeButton"].addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
            }
        };
        Window.prototype.onCloseButton = function () {
            this.close();
        };
        /**
        * 关闭窗口
        * */
        Window.prototype.close = function () {
            _super.prototype.close.call(this);
            // how.SoundManager.playEffect(SoundConfig.WINDOW_CLOSE);
        };
        return Window;
    }(how.module.Window));
    base.Window = Window;
    __reflect(Window.prototype, "base.Window");
})(base || (base = {}));
//# sourceMappingURL=Window.js.map