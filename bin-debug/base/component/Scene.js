var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var base;
(function (base) {
    /**
     * 场景类中间层
     */
    var Scene = (function (_super) {
        __extends(Scene, _super);
        function Scene() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Scene.prototype.createMC = function (pathName, movieClipName, autoPlay, playTimes) {
            if (autoPlay === void 0) { autoPlay = true; }
            if (playTimes === void 0) { playTimes = -1; }
            var json = RES.getRes(pathName + "_json");
            var png = RES.getRes(pathName + "_png");
            var gfactory = new egret.MovieClipDataFactory(json, png);
            var mc = new egret.MovieClip(gfactory.generateMovieClipData(movieClipName));
            if (autoPlay) {
                mc.gotoAndPlay("normal", playTimes);
            }
            return mc;
        };
        Scene.prototype.createFullMc = function (pathName, movieClipName, color, scale) {
            if (color === void 0) { color = 0; }
            if (scale === void 0) { scale = 1; }
            this.removeChildren();
            var rect = new eui.Rect();
            rect.fillColor = color;
            rect.left = rect.right = rect.top = rect.bottom = 0;
            this.addChild(rect);
            this.useUpdate = true;
            var mc = this.createMC(pathName, movieClipName, true, 1);
            mc.scaleX = mc.scaleY = scale;
            this.addChild(mc);
            mc.x = this.stage.stageWidth / 2 - mc.width * scale / 2;
            mc.y = this.stage.stageHeight / 2 - mc.height * scale / 2;
            return mc;
        };
        Scene.prototype.initUI = function (nandu) {
            this.nandu = nandu;
            this.startCountdown();
        };
        /**
         * 开始倒计时
         */
        Scene.prototype.startCountdown = function () {
            var rect = this["timeRect"];
            egret.Tween.get(rect).to({ percentWidth: 0 }, GameConfig.nanduTime[this.nandu] * 1000).call(this.onTimeOver, this);
        };
        Scene.prototype.onTimeOver = function () {
            trace("时间到！！！！");
        };
        Scene.prototype.stopTime = function () {
            if (this["timeRect"]) {
                egret.Tween.removeTweens(this["timeRect"]);
            }
        };
        Scene.prototype.onDestroy = function () {
            _super.prototype.onDestroy.call(this);
            this.stopTime();
        };
        return Scene;
    }(how.module.Scene));
    /**
     * 当前难度
     */
    Scene.currentNandu = 0;
    base.Scene = Scene;
    __reflect(Scene.prototype, "base.Scene");
})(base || (base = {}));
//# sourceMappingURL=Scene.js.map