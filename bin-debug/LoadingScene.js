var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * 游戏初始的载入界面
 * @author 袁浩
 */
var LoadingScene = (function (_super) {
    __extends(LoadingScene, _super);
    function LoadingScene() {
        var _this = _super.call(this) || this;
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddToStage, _this);
        return _this;
    }
    LoadingScene.prototype.onAddToStage = function (event) {
        this.onStageResize(null);
        if (RES.getGroupByName("loadding").length) {
            this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage, this);
            this.stage.addEventListener(egret.Event.RESIZE, this.onStageResize, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.loadGroup("loadding");
        }
        else {
            this.dispatchEventWith(egret.Event.COMPLETE); //告诉HowMain，加载界面初始化完成
        }
    };
    LoadingScene.prototype.onStageResize = function (event) {
        this.width = this.stage.stageWidth;
        this.height = this.stage.stageHeight;
    };
    LoadingScene.prototype.onRemovedFromStage = function (event) {
        this.stage.removeEventListener(egret.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage, this);
        this.stage.removeEventListener(egret.Event.RESIZE, this.onStageResize, this);
    };
    LoadingScene.prototype.onResourceLoadComplete = function (event) {
        RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
        if (RES.getGroupByName("loadding").length) {
            this.createView();
        }
    };
    LoadingScene.prototype.createView = function () {
        this.width = this.stage.stageWidth;
        this.height = this.stage.stageHeight;
        var border = new eui.Image();
        border.source = "loading_框_png";
        border.horizontalCenter = border.verticalCenter = 0;
        this.addChild(border);
        var bgGroup = new eui.Group();
        var horizontal = new eui.HorizontalLayout();
        horizontal.verticalAlign = "middle";
        horizontal.gap = 20;
        bgGroup.layout = horizontal;
        bgGroup.horizontalCenter = 7;
        bgGroup.verticalCenter = 0;
        for (var i = 0; i < 5; i++) {
            var icon = new eui.Image();
            icon.source = "loading_5_png";
            bgGroup.addChild(icon);
        }
        this.addChild(bgGroup);
        var group = new eui.Group();
        group.horizontalCenter = 7;
        group.verticalCenter = 0;
        var contentGroup = new eui.Group();
        var horizontal = new eui.HorizontalLayout();
        horizontal.verticalAlign = "middle";
        horizontal.gap = 20;
        contentGroup.layout = horizontal;
        for (var i = 0; i < 5; i++) {
            var icon = new eui.Image();
            icon.source = "loading_" + i + "_png";
            contentGroup.addChild(icon);
        }
        var scroller = new eui.Scroller();
        scroller.percentWidth = 100;
        scroller.viewport = contentGroup;
        this.scroller = scroller;
        group.addChild(scroller);
        this.addChild(group);
        this.dispatchEventWith(egret.Event.COMPLETE); //告诉HowMain，加载界面初始化完成
    };
    LoadingScene.prototype.onProgressComplete = function (event) {
        this.dispatchEventWith(egret.Event.COMPLETE); //告诉HowMain，加载界面初始化完成
    };
    LoadingScene.prototype.setProgress = function (percent, current, total) {
        if (current === void 0) { current = 0; }
        if (total === void 0) { total = 0; }
        this.scroller.percentWidth = percent;
    };
    LoadingScene.prototype.setText = function (text) {
    };
    return LoadingScene;
}(eui.Group));
__reflect(LoadingScene.prototype, "LoadingScene");
//# sourceMappingURL=LoadingScene.js.map