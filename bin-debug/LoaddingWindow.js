var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var LoaddingWindow = (function (_super) {
    __extends(LoaddingWindow, _super);
    function LoaddingWindow() {
        return _super.call(this) || this;
    }
    LoaddingWindow.prototype.childrenCreated = function () {
        this.addEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onStopRotation, this);
        // this.grpLoading.visible = false;
        // this._timeOut = egret.setTimeout(this.onDelayShow, this, 500);
    };
    LoaddingWindow.prototype.onDelayShow = function () {
        this.loadding.visible = true;
    };
    LoaddingWindow.prototype.onFrameRotation = function (event) {
        if (this.loadding) {
            this.loadding.rotation += 10;
        }
    };
    LoaddingWindow.prototype.onStopRotation = function (event) {
        this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
    };
    LoaddingWindow.prototype.$onRemoveFromStage = function () {
        _super.prototype.$onRemoveFromStage.call(this);
        this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        egret.clearTimeout(this._timeOut);
    };
    return LoaddingWindow;
}(how.Loadding));
__reflect(LoaddingWindow.prototype, "LoaddingWindow");
//# sourceMappingURL=LoaddingWindow.js.map