
var game_file_list = [
    //以下为自动修改，请勿修改
    //----auto game_file_list start----
	"libs/modules/egret/egret.js",
	"libs/modules/egret/egret.native.js",
	"libs/modules/game/game.js",
	"libs/modules/res/res.js",
	"libs/modules/eui/eui.js",
	"libs/modules/tween/tween.js",
	"libs/modules/socket/socket.js",
	"bin-debug/how/module/core/Module.js",
	"bin-debug/how/module/core/SceneModule.js",
	"bin-debug/how/module/view/View.js",
	"bin-debug/how/module/view/Scene.js",
	"bin-debug/how/module/core/WindowModule.js",
	"bin-debug/how/module/core/TSceneModule.js",
	"bin-debug/how/module/view/Window.js",
	"bin-debug/base/component/Window.js",
	"bin-debug/base/component/Scene.js",
	"bin-debug/how/core/HowMain.js",
	"bin-debug/how/module/core/TWindowModule.js",
	"bin-debug/how/component/Loadding.js",
	"bin-debug/how/component/Button.js",
	"bin-debug/base/module/GameModule.js",
	"bin-debug/how/module/core/TModule.js",
	"bin-debug/how/component/EditableText.js",
	"bin-debug/how/component/Label.js",
	"bin-debug/config/GameConfig.js",
	"bin-debug/how/component/Log.js",
	"bin-debug/how/component/Notice.js",
	"bin-debug/how/component/PageView.js",
	"bin-debug/how/component/TabBarButtonItemRenderer.js",
	"bin-debug/how/component/TextInput.js",
	"bin-debug/how/component/WebView.js",
	"bin-debug/how/core/Application.js",
	"bin-debug/how/core/console.js",
	"bin-debug/how/core/DisplayUtils.js",
	"bin-debug/how/core/EventManager.js",
	"bin-debug/how/core/ExternalInterfaceUtils.js",
	"bin-debug/config/LanguageConfig.js",
	"bin-debug/how/core/md5.js",
	"bin-debug/how/core/Random.js",
	"bin-debug/how/core/ResourceLoader.js",
	"bin-debug/how/core/SoundManager.js",
	"bin-debug/how/core/SoundUtils.js",
	"bin-debug/how/core/StringUtils.js",
	"bin-debug/how/core/WindowManager.js",
	"bin-debug/how/module/core/GlobalModule.js",
	"bin-debug/how/module/core/IBehaviour.js",
	"bin-debug/how/component/Alert.js",
	"bin-debug/how/component/Animation.js",
	"bin-debug/how/component/AnimationButton.js",
	"bin-debug/how/component/Banner.js",
	"bin-debug/base/component/Loadding.js",
	"bin-debug/how/component/Checkbox.js",
	"bin-debug/how/module/data/Data.js",
	"bin-debug/how/module/view/ItemView.js",
	"bin-debug/how/component/Component.js",
	"bin-debug/how/component/DateChooser.js",
	"bin-debug/how/component/Dialog.js",
	"bin-debug/how/mouse/MouseDelegate.js",
	"bin-debug/how/mouse/MouseEvent.js",
	"bin-debug/how/net/HttpManager.js",
	"bin-debug/how/net/WebSocketManager.js",
	"bin-debug/how/Utils.js",
	"bin-debug/LoaddingWindow.js",
	"bin-debug/LoadingScene.js",
	"bin-debug/Main.js",
	"bin-debug/modules/factoryGame0/FactoryGame0Module.js",
	"bin-debug/modules/factoryGame0/FactoryGame0View.js",
	"bin-debug/modules/factoryGame1/FactoryGame1Module.js",
	"bin-debug/modules/factoryGame1/FactoryGame1View.js",
	"bin-debug/modules/homeGame0/HomeGame0Module.js",
	"bin-debug/modules/homeGame0/HomeGame0View.js",
	"bin-debug/modules/homeGame1/HomeGame1Module.js",
	"bin-debug/modules/homeGame1/HomeGame1View.js",
	"bin-debug/modules/labGame0/LabGame0Module.js",
	"bin-debug/modules/labGame0/LabGame0View.js",
	"bin-debug/modules/labGame1/LabGame1Module.js",
	"bin-debug/modules/labGame1/LabGame1View.js",
	"bin-debug/modules/main/MainModule.js",
	"bin-debug/modules/main/MainView.js",
	"bin-debug/modules/resume/ResumeModule.js",
	"bin-debug/modules/resume/ResumeView.js",
	"bin-debug/modules/say/SayModule.js",
	"bin-debug/modules/say/SayView.js",
	"bin-debug/modules/teach/TeachModule.js",
	"bin-debug/modules/teach/TeachView.js",
	"bin-debug/modules/teach/TechWindowModule.js",
	"bin-debug/modules/teach/TechWindowView.js",
	"bin-debug/modules/welcome/WelcomeModule.js",
	"bin-debug/modules/welcome/WelcomeView.js",
	//----auto game_file_list end----
];

var window = this;

egret_native.setSearchPaths([""]);

egret_native.requireFiles = function () {
    for (var key in game_file_list) {
        var src = game_file_list[key];
        require(src);
    }
};

egret_native.egretInit = function () {
    if(egret_native.featureEnable) {
        //控制一些优化方案是否开启
        var result = egret_native.featureEnable({
            
        });
    }
    egret_native.requireFiles();
    //egret.dom为空实现
    egret.dom = {};
    egret.dom.drawAsCanvas = function () {
    };
};

egret_native.egretStart = function () {
    var option = {
        //以下为自动修改，请勿修改
        //----auto option start----
		entryClassName: "Main",
		frameRate: 60,
		scaleMode: "fixedNarrow",
		contentWidth: 960,
		contentHeight: 640,
		showPaintRect: false,
		showFPS: false,
		fpsStyles: "x:0,y:0,size:12,textColor:0xffffff,bgAlpha:0.9",
		showLog: false,
		logFilter: "",
		maxTouches: 2,
		textureScaleFactor: 1
		//----auto option end----
    };

    egret.native.NativePlayer.option = option;
    egret.runEgret();
    egret_native.Label.createLabel("/system/fonts/DroidSansFallback.ttf", 20, "", 0);
    egret_native.EGTView.preSetOffScreenBufferEnable(true);
};