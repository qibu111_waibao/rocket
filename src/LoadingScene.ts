/**
 * 游戏初始的载入界面
 * @author 袁浩
 */
class LoadingScene extends eui.Group {
    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    private onAddToStage(event: egret.Event) {
        this.onStageResize(null);
        if (RES.getGroupByName("loadding").length) {
            this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage, this);
            this.stage.addEventListener(egret.Event.RESIZE, this.onStageResize, this);
            RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
            RES.loadGroup("loadding");
        }
        else {
            this.dispatchEventWith(egret.Event.COMPLETE);//告诉HowMain，加载界面初始化完成
        }
    }
    private onStageResize(event: egret.Event): void {
        this.width = this.stage.stageWidth;
        this.height = this.stage.stageHeight;
    }
    private onRemovedFromStage(event: egret.Event): void {
        this.stage.removeEventListener(egret.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage, this);
        this.stage.removeEventListener(egret.Event.RESIZE, this.onStageResize, this);
    }
    protected onResourceLoadComplete(event: RES.ResourceEvent): void {
        RES.removeEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
        if (RES.getGroupByName("loadding").length) {
            this.createView();
        }
    }
    private scroller: eui.Scroller;
    private createView(): void {
        this.width = this.stage.stageWidth;
        this.height = this.stage.stageHeight;

        var border: eui.Image = new eui.Image();
        border.source = "loading_框_png";
        border.horizontalCenter = border.verticalCenter = 0;
        this.addChild(border);

        var bgGroup = new eui.Group();
        var horizontal = new eui.HorizontalLayout();
        horizontal.verticalAlign = "middle";
        horizontal.gap = 20;
        bgGroup.layout = horizontal;
        bgGroup.horizontalCenter = 7;
        bgGroup.verticalCenter = 0;
        for (var i = 0; i < 5; i++) {
            var icon = new eui.Image();
            icon.source = "loading_5_png";
            bgGroup.addChild(icon);
        }
        this.addChild(bgGroup);

        var group = new eui.Group();
        group.horizontalCenter = 7;
        group.verticalCenter = 0;

        var contentGroup = new eui.Group();
        var horizontal = new eui.HorizontalLayout();
        horizontal.verticalAlign = "middle";
        horizontal.gap = 20;
        contentGroup.layout = horizontal;
        for (var i = 0; i < 5; i++) {
            var icon = new eui.Image();
            icon.source = "loading_" + i + "_png";
            contentGroup.addChild(icon);
        }

        var scroller = new eui.Scroller();
        scroller.percentWidth = 100;
        scroller.viewport = contentGroup;
        this.scroller = scroller;
        group.addChild(scroller);
        this.addChild(group);
        this.dispatchEventWith(egret.Event.COMPLETE);//告诉HowMain，加载界面初始化完成
    }
    private onProgressComplete(event: eui.UIEvent): void {
        this.dispatchEventWith(egret.Event.COMPLETE);//告诉HowMain，加载界面初始化完成
    }
    public setProgress(percent: number, current: number = 0, total: number = 0): void {
        this.scroller.percentWidth = percent;
    }
    public setText(text: string): void {
    }
}