/**
 * 特效配置
 */
class GameConfig {
    /**
     * 村长界面，按钮缓慢显示出来的时间
     */
    public static cunzhangButtonsTime: number = 500;
    /**
     * 教程界面，点线完全展开所耗时间
     */
    public static lineTime: number = 1500;
    /**
     * 教程界面，提示文字显示所耗时间
     */
    public static tipTextTime: number = 500;
    /**
     * 教程界面，我懂啦按钮显示所耗时间
     */
    public static iknowTime: number = 500;
    /**
     * 拼图游戏，图和格子相差多少像素算成功
     */
    public static pingtuOffset: number = 15;
    /**
     * 实验室滴管三角移动一圈（上->下->上）耗时
     */
    public static sanjiaoTime: number = 2000;
    /**
     * 发射台游戏一次点击递加的进度值（最好是可以被100整除）
     */
    public static fasheJinduStep: number = 10;
    /**
     * 发射台游戏进度条递减速度
     */
    public static fasheJinduDijian: number = 0.2;
    /**
     * 难度配置（秒），第0个是试玩阶段的时间
     */
    public static nanduTime: number[] = [30, 12, 11, 10, 9, 8, 7];
    /**
     * 每过多少游戏提示难度
     */
    public static levelNanduGameCount: number = 3;
    /**
     * 得分基数，每过一关得分为基数*难度
     */
    public static baseScore: number = 2;
    /**
     * 得分上限（得了多少分进度条变满）
     */
    public static maxScore: number = 100;
    /**
     * 太空垃圾游戏泡泡产生间隔和移动到顶部时间的难度配置（毫秒），第0个是试玩阶段的时间
     */
    public static paopaoNandu: { paopaoInterval: number, paopaoMoveTime: number }[] = [
        { paopaoInterval: 800, paopaoMoveTime: 4000 },
        { paopaoInterval: 500, paopaoMoveTime: 3000 },
        { paopaoInterval: 480, paopaoMoveTime: 2800 },
        { paopaoInterval: 450, paopaoMoveTime: 2500 },
        { paopaoInterval: 420, paopaoMoveTime: 2200 },
        { paopaoInterval: 400, paopaoMoveTime: 2000 },
        { paopaoInterval: 380, paopaoMoveTime: 1800 },
    ];
}