class LoaddingWindow extends how.Loadding {
    public loadding: eui.Image;

    private _timeOut: number;
    public constructor() {
        super();
    }
    public childrenCreated(): void {
        this.addEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onStopRotation, this);
        // this.grpLoading.visible = false;
        // this._timeOut = egret.setTimeout(this.onDelayShow, this, 500);
    }
    private onDelayShow(): void {
        this.loadding.visible = true;
    }

    private onFrameRotation(event: egret.Event): void {
        if (this.loadding) {
            this.loadding.rotation += 10;
        }
    }

    private onStopRotation(event: egret.Event): void {
        this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
    }
    $onRemoveFromStage(): void {
        super.$onRemoveFromStage();
        this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        egret.clearTimeout(this._timeOut);
    }
}