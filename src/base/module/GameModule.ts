module base {
    /**
     * 工厂第一个游戏模块
     */
    export class GameModule<V extends Scene, D extends how.module.Data> extends how.module.TSceneModule<Scene, any> {
        public nandu: number;
        public static request = {
            onGameOver: "onGameOver"
        }
        public guiAwake(): void {
            this.callUI("initUI", this.nandu);
        }
        public onGameOver(type: number, gameIndex: number, isWin: boolean): void {
            this.moduleManager.initModule(MainModule, MainView, null, { showTeach: false, showSay: { sayType: type, gameData: { isWin: isWin, gameIndex: gameIndex, nandu: this.nandu } } });
        }
    }
}