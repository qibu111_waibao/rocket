module base {
	/**
	 * 载入中
	 * @author 袁浩
	 *
	 */
    export class Loadding extends how.Loadding {
        public grpLoading: eui.Group;

        private _timeOut: number;
        public constructor() {
            super();
        }
        public childrenCreated(): void {
            this.addEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
            this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onStopRotation, this);
            // this.grpLoading.visible = false;
            // this._timeOut = egret.setTimeout(this.onDelayShow, this, 500);
        }
        private onDelayShow(): void {
            this.grpLoading.visible = true;
        }

        private onFrameRotation(event: egret.Event): void {
            if (this.grpLoading) {
                this.grpLoading.rotation += 10;
            }
        }

        private onStopRotation(event: egret.Event): void {
            this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
        }
        $onRemoveFromStage(): void {
            super.$onRemoveFromStage();
            this.removeEventListener(egret.Event.ENTER_FRAME, this.onFrameRotation, this);
            egret.clearTimeout(this._timeOut);
        }
    }
}
