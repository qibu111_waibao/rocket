module base {
    /**
     * 场景类中间层
     */
    export class Scene extends how.module.Scene {
        protected createMC(pathName: string, movieClipName: string, autoPlay: boolean = true, playTimes: number = -1): egret.MovieClip {
            var json = RES.getRes(pathName + "_json");
            var png = RES.getRes(pathName + "_png");
            var gfactory: egret.MovieClipDataFactory = new egret.MovieClipDataFactory(json, png);
            var mc: egret.MovieClip = new egret.MovieClip(gfactory.generateMovieClipData(movieClipName));
            if (autoPlay) {
                mc.gotoAndPlay("normal", playTimes);
            }
            return mc;
        }
        protected createFullMc(pathName: string, movieClipName: string, color: number = 0, scale: number = 1): egret.MovieClip {
            this.removeChildren();
            var rect = new eui.Rect();
            rect.fillColor = color;
            rect.left = rect.right = rect.top = rect.bottom = 0;
            this.addChild(rect);
            this.useUpdate = true;
            var mc = this.createMC(pathName, movieClipName, true, 1);
            mc.scaleX = mc.scaleY = scale;
            this.addChild(mc);
            mc.x = this.stage.stageWidth / 2 - mc.width * scale / 2;
            mc.y = this.stage.stageHeight / 2 - mc.height * scale / 2;
            return mc;
        }
        protected nandu: number;
        public initUI(nandu: number): void {
            this.nandu = nandu;
            this.startCountdown();
        }
        /**
         * 当前难度
         */
        public static currentNandu: number = 0;
        /**
         * 开始倒计时
         */
        public startCountdown(): void {
            var rect = this["timeRect"] as eui.Rect;
            egret.Tween.get(rect).to({ percentWidth: 0 }, GameConfig.nanduTime[this.nandu] * 1000).call(this.onTimeOver, this);
        }
        protected onTimeOver(): void {
            trace("时间到！！！！");
        }
        protected stopTime() {
            if (this["timeRect"]) {
                egret.Tween.removeTweens(this["timeRect"]);
            }
        }
        public onDestroy(): void {
            super.onDestroy();
            this.stopTime();
        }
    }
}