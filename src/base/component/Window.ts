module base {
    /**
     * 场景类中间层
     */
    export class Window extends how.module.Window {
        public childrenCreated(): void {
            super.childrenCreated();
            this.initCloseButton();
            // how.SoundManager.playEffect(SoundConfig.WINDOW_OPEN);
        }
        protected initCloseButton(): void {
            if (this["closeBtn"]) {
                this["closeBtn"].addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
            }
            if (this["closeButton"]) {
                this["closeButton"].addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
            }
        }
        protected onCloseButton(): void {
            this.close();
        }
        /**
        * 关闭窗口
        * */
        public close(): void {
            super.close();
            // how.SoundManager.playEffect(SoundConfig.WINDOW_CLOSE);
        }
    }
}