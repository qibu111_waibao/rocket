/**
 * 证书模块
 */
class ResumeModule extends how.module.TWindowModule<ResumeView, any> {
    public addScore: number;
    public type: number;
    public static currentScores: number[] = [0, 0, 0];
    public static currentNandu: number = 1;
    public static teachComplete: boolean[] = [false, false, false];
    private static currentGameCount: number = 0;
    public guiAwake(): void {
        this.callUI("initUI", this.type, this.addScore);
        ResumeModule.currentScores[this.type] += this.addScore;
        if (this.addScore > 0) {
            if (ResumeModule.currentGameCount < 2) {
                ResumeModule.currentGameCount++;
            }
            else {
                trace("提升难度");
                ResumeModule.currentNandu++;
            }
        }
    }
}