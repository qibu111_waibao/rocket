/**
 * 证书界面
 * @author 袁浩
 */
class ResumeView extends base.Window {
    public constructor() {
        super();
        this.skinName = "ResumeSkin";
    }
    public initUI(type: number, addScore: number) {
        var currentScores = ResumeModule.currentScores;
        for (var i = 0; i < currentScores.length; i++) {
            var rect = this["type" + i + "Rect"] as eui.Rect;
            rect.percentWidth = currentScores[i] / GameConfig.maxScore * 100;
            if (i == type) {
                var toPercent = (currentScores[i] + addScore) / GameConfig.maxScore * 100;
                toPercent = toPercent > 100 ? 100 : toPercent;
                egret.Tween.get(rect).to({ percentWidth: toPercent }, 300);
            }
            this["gameResume" + i].source = ResumeModule.teachComplete[i] ? "gameResume" + i + "_png" : "gameResume" + i + "_grey_png";
        }
    }
}