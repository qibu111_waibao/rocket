/**
 * 家第二个游戏界面
 */
class HomeGame1View extends base.Scene {
    public zhangaiGroup: eui.Group;
    private timeID: number;
    private paopaoCount: number = 0;
    public get resourceList(): Array<string> {
        return ["homeGame1"];
    }
    public constructor() {
        super();
        this.skinName = "HomeGame1Skin";
    }
    /**
     * 重写开始倒计时方法，这关不需要时间倒计时
     */
    public startCountdown(): void {
        this.timeID = egret.setInterval(this.createImg, this, GameConfig.paopaoNandu[this.nandu].paopaoInterval);
    }
    private createImg(): void {
        var paopao = new eui.Image();
        paopao.x = how.Random.range(0, this.stage.stageWidth - 76);
        paopao.y = this.stage.stageHeight;
        paopao.source = "泡泡" + Math.round(how.Random.range(1, 3)) + "_png";
        this.addChild(paopao);
        egret.Tween.get(paopao, { onChange: this.onChange, onChangeObj: this }).to({ y: -76 }, GameConfig.paopaoNandu[this.nandu].paopaoMoveTime);
        paopao.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onPaopao, this);
    }
    private onPaopao(event: egret.TouchEvent): void {
        var paopao = event.currentTarget as eui.Image;
        egret.Tween.removeTweens(paopao);
        var paopaoName = paopao.source.toString().substring(0, 3);
        var mc = this.createMC(paopaoName + "Ani", paopaoName, true, 1);
        mc.addEventListener(egret.Event.COMPLETE, () => {
            if (mc.parent) {
                this.removeChild(mc);
            }
            if (this.paopaoCount == 10) {
                this.win();
            }
        }, this);
        mc.x = paopao.x - 156;
        mc.y = paopao.y - 140;//134
        this.addChild(mc);
        this.removeChild(paopao);
        this.paopaoCount++;
        if (this.paopaoCount == 10) {
            this.stop();
        }
    }
    private onChange(event: egret.Event, aa): void {
        var tween = event.currentTarget as egret.Tween;
        var paopao = tween["_target"] as eui.Image;
        for (var i = 0; i < this.zhangaiGroup.numChildren; i++) {
            var zhangai = this.zhangaiGroup.getChildAt(i);
            var zhangaiGlobalPoint = zhangai.localToGlobal();
            var zhangaiRectangle = new egret.Rectangle(zhangaiGlobalPoint.x, zhangaiGlobalPoint.y, zhangai.width, zhangai.height);
            var paopaoRectangle = new egret.Rectangle(paopao.x, paopao.y, paopao.width, paopao.height);
            if (zhangaiRectangle.intersects(paopaoRectangle)) {
                this.loss();
                break;
            }
        }
    }
    private loss(): void {
        this.stop();
        var mc = this.createFullMc("homeGame1Loss", "游戏3失败", 0x7fb0bd);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 0, 1, false);
        }, this);
        this.stopTime();
    }
    private win(): void {
        this.stop();
        var mc = this.createFullMc("homeGame1Win", "游戏3成功", 0x7fb0bd);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 0, 1, true);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
    public onDestroy(): void {
        super.onDestroy();
        this.stop();
    }
    private stop(): void {
        egret.Tween.removeAllTweens();
        egret.clearInterval(this.timeID);
    }
}