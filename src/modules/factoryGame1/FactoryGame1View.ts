/**
 * 工程第二个游戏界面
 */
class FactoryGame1View extends base.Scene {
    public buttonsGroup: eui.Group;
    private currentStep: number = 0;
    private currentPercent: number = 0;
    private ani: egret.MovieClip;
    private data = ["第一步_png", "第二步_png", "第三步_png"];
    public get resourceList(): Array<string> {
        return ["factoryGame1"];
    }
    public constructor() {
        super();
        this.skinName = "FactoryGame1Skin";
    }
    public start(): void {
        this.createImage();
        this.createAni();
    }
    private createAni(): void {
        var group = new eui.Group();
        var mc = this.createMC("qingxiAni", "清洗", true);
        mc.y = -mc.height;
        group.addChild(mc);
        group.width = mc.width;
        group.height = group.height;
        group.right = 0;
        group.verticalCenter = 0;
        this.addChild(group);
        mc.gotoAndPlay(45);
        this.ani = mc;
        mc.visible = false;
        this.ani.addEventListener(egret.Event.LOOP_COMPLETE, () => {
            this.ani.visible = false;
        }, this);
    }
    private playAni(): void {
        this.ani.visible = true;
    }
    private createImage(): void {
        var group = this.buttonsGroup.getChildAt(this.currentStep) as eui.Group;
        var image = group.getChildAt(0) as eui.Image;
        image.source = this.data.length == 1 ? this.data[0] : this.data.splice(Math.round(how.Random.range(0, this.data.length - 1)), 1)[0];
        group.visible = true;
        this.setMask();
        group.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGroupTouch, this);
    }
    private setMask(): void {
        var group = this.buttonsGroup.getChildAt(this.currentStep) as eui.Group;
        var fillIcon = group.getChildAt(1) as eui.Image;
        if (group.numChildren == 3) {
            group.removeChildAt(2);
        }
        fillIcon.mask = null;
        var sector = how.DisplayUtils.getSector(fillIcon.width / 2, 0, this.currentPercent / 100 * 360);
        sector.x += fillIcon.width / 2;
        sector.y += fillIcon.height / 2;
        sector.touchEnabled = false;
        group.addChild(sector);
        fillIcon.mask = sector;
    }
    protected setUpdate(): void {
        this.useUpdate = true;
        super.setUpdate();
    }
    public update(): void {
        if (this.currentStep < 3) {
            this.currentPercent -= GameConfig.fasheJinduDijian;
            this.currentPercent = this.currentPercent < 0 ? 0 : this.currentPercent;
            this.setMask();
        }
    }
    private onGroupTouch(event: egret.TouchEvent): void {
        this.playAni();
        this.currentPercent += GameConfig.fasheJinduStep;
        this.currentPercent = this.currentPercent > 100 ? 100 : this.currentPercent;
        this.setMask();
        if (this.currentPercent >= 100) {
            var group = event.currentTarget as eui.Group;
            group.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onGroupTouch, this);
            this.currentPercent = 0;
            this.currentStep++;
            if (this.currentStep < 3) {
                this.createImage();
            }
            else {
                this.win();
            }
        }
    }
    private win(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame1Win", "游戏8成功", 0xe9c7b5, 2);
        mc.x -= 50;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 2, 1, true);
        }, this);
        this.stopTime();
    }
    private loss(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame1Loss", "游戏8失败", 0xe9c7b5, 2);
        mc.x -= 50;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 2, 1, false);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
}