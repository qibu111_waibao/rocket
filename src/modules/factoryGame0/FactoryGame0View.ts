/**
 * 工程第一个游戏界面
 */
class FactoryGame0View extends base.Scene {
    public shoesGroup: eui.Group;
    /**
     * 不合格鞋子数量
     */
    private buhegeCount: number = 0;
    public get resourceList(): Array<string> {
        return ["factoryGame0"];
    }
    public constructor() {
        super();
        this.skinName = "FactoryGame0Skin";
    }
    public awake(): void {
        this.initImages();
    }
    private initImages(): void {
        for (var i = 0; i < this.shoesGroup.numChildren; i++) {
            var isJige: boolean = !!Math.round(how.Random.range(0, 1));
            var shoeIndex: number = Math.round(how.Random.range(1, 2));
            var jigeString = isJige ? "及格鞋" : "不及格鞋";
            var group = (this.shoesGroup.getChildAt(i) as eui.Group);
            var image = group.getChildAt(0) as eui.Image;
            group.getChildAt(1).visible = false;
            image.source = jigeString + shoeIndex + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onImageTouch, this);
            if (!isJige) {
                this.buhegeCount++;
            }
        }
        var randomImage = ((this.shoesGroup.getChildAt(Math.round(how.Random.range(0, 5))) as eui.Group).getChildAt(0) as eui.Image);
        if (randomImage.source.toString().indexOf("不及格") != 0) {
            randomImage.source = "不及格鞋" + Math.round(how.Random.range(1, 2)) + "_png";//保证至少有1个不及格鞋
            this.buhegeCount++;
        }
    }
    private onImageTouch(event: egret.TouchEvent): void {
        var image = event.currentTarget as eui.Image;
        if (image.source.toString().indexOf("不及格") == 0 && !image.parent.getChildAt(1).visible) {
            image.parent.getChildAt(1).visible = true;
            this.buhegeCount--;
            if (this.buhegeCount == 0) {
                this.win();
            }
        }
    }
    private win(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame0Win", "游戏5成功", 0x0b9444);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 2, 0, true);
        }, this);
        this.stopTime();
    }
    private loss(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("factoryGame0Loss", "游戏5失败", 0x0b9444);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 2, 0, false);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
}