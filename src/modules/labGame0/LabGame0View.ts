/**
 * 实验室第一个游戏界面
 */
class LabGame0View extends base.Scene {
    public sanjiao: eui.Image;
    public successArea: eui.Rect;
    public shuidiImg: eui.Image;
    public rongye0: eui.Image;
    public rongye1: eui.Rect;
    public rongye2: eui.Rect;
    public diguanButton: how.Button;
    private successCount: number = 0;
    private isShuidiRun: boolean = false;
    private ani: egret.MovieClip;
    public get resourceList(): Array<string> {
        return ["labGame0"];
    }
    public constructor() {
        super();
        this.skinName = "LabGame0Skin";
    }
    public start(): void {
        this.startSanjiao();
        this.createAni();
        this.diguanButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDiguanButton, this);
    }
    private startSanjiao(): void {
        egret.Tween.get(this.sanjiao, { loop: true }).to({ y: this.sanjiao.parent.height - this.sanjiao.height }, GameConfig.sanjiaoTime / 2).to({ y: 0 }, GameConfig.sanjiaoTime / 2);
    }
    private checkLoss(): boolean {
        var pos = this.sanjiao.y + this.sanjiao.height / 2;
        return pos >= this.successArea.y && pos <= this.successArea.y + this.successArea.height;
    }
    private onDiguanButton(): void {
        if (this.checkLoss()) {
            if (!this.isShuidiRun) {
                var shuidi: eui.Image = new eui.Image();
                shuidi.source = this.shuidiImg.source;
                var point = this.shuidiImg.localToGlobal();
                shuidi.x = point.x;
                shuidi.y = point.y;
                this.addChild(shuidi);
                this.isShuidiRun = true;
                egret.Tween.get(shuidi).to({ y: this["rongye" + this.successCount].localToGlobal().y + this["rongye" + this.successCount].height - this.shuidiImg.height }, 300).call(() => {
                    this["rongye" + this.successCount].visible = true;
                    this.isShuidiRun = false;
                    this.removeChild(shuidi);
                    this.successCount++;
                    if (this.successCount == 3) {
                        this.win();
                    }
                    else {
                        this.ani.gotoAndPlay(0);
                    }
                }, this);
            }
        }
        else {
            this.loss();
        }
    }
    private createAni(): void {
        var group = new eui.Group();
        var mc = this.createMC("labGame0Drink", "游戏1喝东西", true, 1);
        group.addChild(mc);
        group.bottom = 410;
        group.horizontalCenter = 80;
        this.addChild(group);
        mc.gotoAndPlay(45);
        this.ani = mc;
    }
    private loss(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame0Loss", "游戏1失败", 0x1584be);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 1, 0, false);
        }, this);
        this.stopTime();
    }
    private win(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame0Win", "游戏1成功", 0x1584be);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 1, 0, true);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
    public onDestroy(): void {
        super.onDestroy();
        egret.Tween.removeAllTweens();
    }
}