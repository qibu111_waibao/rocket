/**
 * 主界面
 */
class MainView extends base.Scene {
    public hroadImg: eui.Image;
    public troadImg: eui.Image;
    public gongchangGroup: eui.Group;
    public shiyanshiGroup: eui.Group;
    public homeIcon: eui.Image;
    public factoryIcon: eui.Image;
    public launchIcon: eui.Image;
    public labIcon: eui.Image;
    public tipGroup: eui.Group;
    public blackGroup: eui.Group;
    public iknowButton: how.Button;
    public resumeButton: how.Button;
    public homeGroup: eui.Group;
    public labGroup: eui.Group;
    public factoryGroup: eui.Group;
    public rocketGroup: eui.Group;
    public homeImg: eui.Image;
    public factoryImg: eui.Image;
    public launchImg: eui.Image;
    public labImg: eui.Image;

    public get resourceList(): Array<string> {
        return ["main"];
    }
    public constructor() {
        super();
        this.skinName = "MainSkin";
    }
    public awake(): void {
        var gongchang = this.createMC("gongchang", "工厂");
        gongchang.x = -140;
        gongchang.y = -200;
        this.gongchangGroup.addChild(gongchang);

        var shiyanshi = this.createMC("shiyanshi", "实验室");
        shiyanshi.x = -50;
        shiyanshi.y = -100;
        this.shiyanshiGroup.addChild(shiyanshi);
    }
    public start(): void {
        this.playTween(this.homeIcon);
        this.playTween(this.factoryIcon);
        this.playTween(this.launchIcon);
        this.playTween(this.labIcon);
        this.iknowButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onIKnowButton, this);
        this.resumeButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResumeButton, this);
        this.homeGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onHomeGroup, this);
        this.labGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onLabGroup, this);
        this.factoryGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onFactoryGroup, this);
        this.rocketGroup.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRocketGroup, this);

        mouse.setButtonMode(this.homeImg, true);
        mouse.setButtonMode(this.factoryImg, true);
        mouse.setButtonMode(this.launchImg, true);
        mouse.setButtonMode(this.labImg, true);
    }
    private setTeachVisible(showTeach: boolean): void {
        this.blackGroup.visible = this.tipGroup.visible = showTeach;
        if (showTeach) {
            for (var i = 0; i < 5; i++) {
                let lineImg = this["line" + i] as eui.Image;
                let tipText = this["tipText" + i] as eui.Image;
                lineImg.visible = false;
                tipText.alpha = 0;
            }
            this.iknowButton.alpha = 0;
            this.playTeachTweens(0);
        }
    }
    private playTeachTweens(index: number) {
        if (index < 5) {
            var lineImg = this["line" + index] as eui.Image;
            var tipText = this["tipText" + index] as eui.Image;
            var oldWidth = lineImg.width;
            lineImg.width = 0;
            lineImg.visible = true;
            egret.Tween.get(lineImg).to({ width: oldWidth }, GameConfig.lineTime).call(() => {
                egret.Tween.get(tipText).to({ alpha: 1 }, GameConfig.tipTextTime).call(this.playTeachTweens, this, [index + 1]);
            }, this);
        }
        else {
            egret.Tween.get(this.iknowButton).to({ alpha: 1 }, GameConfig.iknowTime);
        }
    }
    private onIKnowButton(): void {
        egret.Tween.get(this.tipGroup).to({ alpha: 0 }, 300).call(() => {
            this.tipGroup.visible = false;
        }, this);
        egret.Tween.get(this.blackGroup).to({ alpha: 0 }, 300).call(() => {
            this.blackGroup.visible = false;
        }, this);
    }
    public playTween(icon: eui.Image): void {
        icon.rotation = -30;
        egret.Tween.get(icon, { loop: true }).to({ rotation: 30 }, 1500).to({ rotation: -30 }, 1500);
    }
    protected setUpdate(): void {
        this.useUpdate = true;
        super.setUpdate();
    }
    public update(): void {
        this.hroadImg.left = this.troadImg.x + this.troadImg.height / 2 - 1;
    }
    private onResumeButton(): void {
        this.report("onResumeButton");
    }
    private onHomeGroup(): void {
        this.report("addSayModule", 0);
    }
    private onLabGroup(): void {
        this.report("addSayModule", 1);
    }
    private onFactoryGroup(): void {
        this.report("addSayModule", 2);
    }
    private onRocketGroup(): void {
        this.report("addSayModule", 3);
    }
    public onDestroy(): void {
        super.onDestroy();
        egret.Tween.removeAllTweens();
    }
}