/**
 * 主界面模块
 */
class MainModule extends how.module.TSceneModule<MainView, any> {
    public showTeach: boolean = true;
    public showSay: { sayType: number, gameData: { isWin: boolean, gameIndex: number, nandu: number } };
    public static request: any = {
        onResumeButton: "onResumeButton",
        addSayModule: "addSayModule"
    }
    public onResumeButton(isWin: boolean, nandu: number = -1, type: number = 0): void {
        this.moduleManager.initModule(ResumeModule, ResumeView, null, { addScore: isWin ? GameConfig.baseScore * (nandu + 1) : 0, type: type });
    }
    public addSayModule(type: number, gameData: { isWin: boolean, gameIndex: number, nandu: number } = null): void {
        if (!gameData || gameData.nandu == 0) {
            this.moduleManager.initModule(SayModule, SayView, null, { type: type, gameData: gameData });
        }
        else {
            this.onResumeButton(gameData.isWin, gameData.nandu, type);
        }
    }
    public guiAwake(): void {
        this.callUI("setTeachVisible", this.showTeach);
        if (this.showSay) {
            this.addSayModule(this.showSay.sayType, this.showSay.gameData);
            this.showSay = null;
        }
    }
    public onDestroy(): void {
        super.onDestroy();
    }
}