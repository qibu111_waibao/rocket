/**
 * 关卡介绍界面
 * @author 袁浩
 */
class SayView extends base.Window {
    public gameText0: eui.Label;
    public gameText1: eui.Label;
    public gameIcon0: eui.Image;
    public gameIcon1: eui.Image;
    public viewStack: eui.ViewStack;
    public closeButton0: how.Button;
    public closeButton1: how.Button;
    public closeButton2: how.Button;
    public closeButton3: how.Button;
    public closeButton4: how.Button;
    public nextButton0: how.Button;
    public preButton1: how.Button;
    public nextButton1: how.Button;
    public preButton2: how.Button;
    public nextButton2: how.Button;
    public preButton3: how.Button;//下一关
    public preButton4: how.Button;
    public nextButton3: how.Button;//继续挑战
    public gameoverLabel: eui.Label;
    private gameIndex: number;
    private textData: any;
    public roleImg: eui.Image;
    private type: number;
    public rectBg: eui.Rect;
    public get resourceList(): Array<string> {
        return ["say"];
    }
    public constructor() {
        super();
        this.skinName = "SaySkin";
    }
    public awake(): void {
        this.textData = RES.getRes("Text_json");
    }
    public start(): void {
        if (this.type != 3) {
            this.gameText0.text = this.textData["gameSay"][this.type][0];
            this.gameText1.text = this.textData["gameSay"][this.type][1];
        }
        this.closeButton0.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.closeButton4.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseButton, this);
        this.gameIcon0.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.viewStack.selectedIndex = 2; this.gameIndex = 0; this.setText(0); }, this);
        this.gameIcon1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.gameIndex = 1; this.setText(0); this.viewStack.selectedIndex = 2 }, this);
        this.nextButton0.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.viewStack.selectedIndex = 1; }, this);
        this.preButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.viewStack.selectedIndex = 1 }, this);
        this.nextButton1.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { if (this.type != 3) { this.viewStack.selectedIndex = 3; this.setText(1); } else { this.onStartGame(this.type, this.gameIndex) } }, this);
        this.preButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.viewStack.selectedIndex = 2 }, this);
        this.preButton4.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.viewStack.selectedIndex = 3 }, this);

        this.nextButton2.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.onStartGame(this.type, this.gameIndex) }, this);
        this.nextButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, () => { this.onStartGame(this.type, this.gameIndex) }, this);
        this.preButton3.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onNextGame, this);
    }
    public initUI(type: number, gameData: { isWin: boolean, gameIndex: number, nandu: Number }) {
        this.type = type;
        if (gameData) {
            this.gameIndex = gameData.gameIndex;
        }
        if (this.type == 2 && this.gameIndex == 1 || (gameData && !gameData.isWin)) {//如果是最后一关或者失败，下一关隐藏
            this.preButton3.visible = false;
            this.viewStack.selectedIndex = 4;
            this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.complete : this.textData.gameOver.loss;
        }
        else if (gameData) {
            this.viewStack.selectedIndex = 4;
            if (this.gameIndex == 1) {
                this.preButton3.visible = false;
                this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.getResume : this.textData.gameOver.loss;
                ResumeModule.teachComplete[this.type] = gameData.isWin;//每关第二个游戏完成都会获得证书
            }
            else {
                this.gameoverLabel.text = gameData.isWin ? this.textData.gameOver.win : this.textData.gameOver.loss;
            }
        }
        if (this.type == 3) {
            this.preButton1.visible = false;
            (this.nextButton1.getChildAt(1) as eui.Image).source = "开始游戏_png";
        }
        for (var i = 0; i < 4; i++) {
            var closeButton = this["closeButton" + i];
            if (closeButton) {
                (closeButton.getChildAt(0) as eui.Image).source = "按钮" + type + "_png";
            }
            var preButton = this["preButton" + i];
            if (preButton) {
                (preButton.getChildAt(0) as eui.Image).source = "按钮" + type + "_png";
            }
            var nextButton = this["nextButton" + i];
            if (nextButton) {
                (nextButton.getChildAt(0) as eui.Image).source = "按钮" + type + "_png";
            }
        }
        if (type != 3) {
            this.gameIcon0.source = "图标_game" + type + "_0_png";
            this.gameIcon1.source = "图标_game" + type + "_1_png";
        }
        else {
            this.viewStack.selectedIndex = 2;
            this.gameIndex = 0;
            this.setText(0);
        }
        if (type == 0) {
            this.roleImg.source = "对话框_npcmainrole_png";
            this.rectBg.fillColor = 0xdff3fd;
        }
        else if (type == 1) {
            this.roleImg.source = "对话框_npclab_png";
            this.rectBg.fillColor = 0xCFEBEB;
        }
        else if (type == 2) {
            this.roleImg.source = "对话框_npcfactory_png";
            this.rectBg.fillColor = 0x97ABC7;
        }
        else if (type == 3) {
            this.roleImg.source = "对话框_npclauch_png";
            this.rectBg.fillColor = 0xA8AED8;
        }
    }
    private onStartGame(type: number, gameIndex: number, nandu: number = 0): void {
        if (type == 0) {
            this.report("onStartHomeGame" + gameIndex, nandu);
        }
        else if (type == 1) {
            this.report("onStartLabGame" + gameIndex, nandu);
        }
        else if (type == 2) {
            this.report("onStartFactoryGame" + gameIndex, nandu);
        }
        else if (type == 3) {
            // this.viewStack.selectedIndex = 5;
            this.createRandomGame();
        }
        if (type != 3) {
            how.WindowManager.getInstance().closeAll();
        }
    }
    private createRandomGame(): void {
        var types = [0, 1, 2];
        for (var i = 0; i < 3; i++) {
            let type = types.splice(Math.floor(how.Random.range(0, types.length)), 1)[0];
            let gameIndex = Math.floor(how.Random.range(0, 2));
            // var image = this["randomGameIcon" + i] as eui.Image;
            // image.source = "图标_game" + type + "_" + gameIndex + "_png";
            ResumeModule.currentNandu = ResumeModule.currentNandu > GameConfig.nanduTime.length - 1 ? GameConfig.nanduTime.length - 1 : ResumeModule.currentNandu;
            this.onStartGame(type, gameIndex, ResumeModule.currentNandu);
            break;
            // image.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            //     this.onStartGame(type, gameIndex, 1);
            // }, this);
        }
    }
    private onNextGame(): void {
        if (this.gameIndex == 0) {
            this.gameIndex++;
        }
        else {
            this.gameIndex = 0;
            this.type++;
        }
        // this.onStartGame(this.type, this.gameIndex);
        this.setText(0); this.viewStack.selectedIndex = 2;
    }
    private setText(index: number): void {
        this["text" + index].text = this.textData["say"][this.type][this.gameIndex][index];
    }
}