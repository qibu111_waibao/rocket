/**
 * 关卡介绍模块
 */
class SayModule extends how.module.TWindowModule<SayView, any> {
    public type: number;
    public gameData: { isWin: boolean, gameIndex: number, nandu: number };
    public static request: any = {
        onStartHomeGame0: "onStartHomeGame0",
        onStartHomeGame1: "onStartHomeGame1",
        onStartLabGame0: "onStartLabGame0",
        onStartLabGame1: "onStartLabGame1",
        onStartFactoryGame0: "onStartFactoryGame0",
        onStartFactoryGame1: "onStartFactoryGame1"
    }
    public onStartHomeGame0(nandu: number): void {
        this.moduleManager.initModule(HomeGame0Module, HomeGame0View, null, { nandu: nandu });
    }
    public onStartHomeGame1(nandu: number): void {
        this.moduleManager.initModule(HomeGame1Module, HomeGame1View, null, { nandu: nandu });
    }
    public onStartLabGame1(nandu: number): void {
        this.moduleManager.initModule(LabGame1Module, LabGame1View, null, { nandu: nandu });
    }
    public onStartLabGame0(nandu: number): void {
        this.moduleManager.initModule(LabGame0Module, LabGame0View, null, { nandu: nandu });
    }
    public onStartFactoryGame0(nandu: number): void {
        this.moduleManager.initModule(FactoryGame0Module, FactoryGame0View, null, { nandu: nandu });
    }
    public onStartFactoryGame1(nandu: number): void {
        this.moduleManager.initModule(FactoryGame1Module, FactoryGame1View, null, { nandu: nandu });
    }
    public guiAwake(): void {
        this.callUI("initUI", this.type, this.gameData);
    }
}