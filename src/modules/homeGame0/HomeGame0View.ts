/**
 * 家第一个游戏界面
 */
class HomeGame0View extends base.Scene {
    public realList: eui.Group;
    public imageList: eui.Group;
    public timeRect: eui.Rect;
    public get resourceList(): Array<string> {
        return ["homeGame0"];
    }
    public constructor() {
        super();
        this.skinName = "HomeGame0Skin";
    }
    public start(): void {
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
        this.initImages();
    }
    private initImages(): void {
        var data = [1, 2, 3, 4, 5, 6];
        while (data.length) {
            var image = this.imageList.getChildAt(6 - data.length) as eui.Image;
            image.source = "拼图" + data.splice(Math.floor(how.Random.range(0, data.length)), 1) + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onImageBegin, this);
        }
        for (var i = 0; i < 6; i++) {
            var image = this.imageList.getChildAt(i) as eui.Image;
            image.x = Math.round(how.Random.range(0, this.imageList.getChildAt(1).x));
            image.y = Math.round(how.Random.range(0, this.imageList.getChildAt(4).y));
        }
    }
    private dragImgData: { img: eui.Image, imagePoint: egret.Point, stagePoint: egret.Point };
    private onImageBegin(event: egret.TouchEvent): void {
        var image = event.currentTarget as eui.Image;
        var imagePoint = image.localToGlobal();
        this.addChild(image);
        image.x = imagePoint.x;
        image.y = imagePoint.y;
        this.dragImgData = { img: image, imagePoint: imagePoint, stagePoint: new egret.Point(event.stageX, event.stageY) };
        egret.Tween.removeTweens(image);
    }
    private onTouchMove(event: egret.TouchEvent): void {
        if (this.dragImgData) {
            var offsetX = event.stageX - this.dragImgData.stagePoint.x;
            var offsetY = event.stageY - this.dragImgData.stagePoint.y;
            this.dragImgData.img.x = this.dragImgData.imagePoint.x + offsetX;
            this.dragImgData.img.y = this.dragImgData.imagePoint.y + offsetY;
        }
    }
    private onTouchEnd(event: egret.TouchEvent): void {
        if (this.dragImgData) {
            this.checkPoint();
        }
        this.dragImgData = null;
    }
    private sucessImages: eui.Image[] = [];
    private checkPoint(): void {
        var dragImg: eui.Image = this.dragImgData.img;
        for (var i = 0; i < this.realList.numElements; i++) {
            var image: eui.Image = this.realList.getChildAt(i) as eui.Image;
            var globalPoint = image.localToGlobal();
            if (Math.abs(globalPoint.x - dragImg.x) <= GameConfig.pingtuOffset && Math.abs(globalPoint.y - dragImg.y) <= GameConfig.pingtuOffset) {
                egret.Tween.get(dragImg).to({ x: globalPoint.x, y: globalPoint.y }, 500);
                if (image.source == dragImg.source) {
                    if (this.sucessImages.indexOf(dragImg) == -1) {
                        this.sucessImages.push(dragImg);
                    }
                    if (this.sucessImages.length == 6) {
                        this.win();
                    }
                }
                else {
                    if (this.sucessImages.indexOf(dragImg) != -1) {
                        this.sucessImages.splice(this.sucessImages.indexOf(dragImg), 1);
                    }
                }
                return;
            }
        }
        if (this.sucessImages.indexOf(dragImg) != -1) {
            this.sucessImages.splice(this.sucessImages.indexOf(dragImg), 1);
        }
    }
    private win(): void {
        var mc = this.createFullMc("homeGame0Win", "游戏9成功", 0xf48b88, 2);
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 0, 0, true);
        }, this);
        this.stopTime();
    }
    private loss(): void {
        var mc = this.createFullMc("homeGame0Loss", "游戏9失败", 0xf48b88);
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 0, 0, false);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
    public onDestroy(): void {
        super.onDestroy();
        this.stage.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.removeEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
    }
}