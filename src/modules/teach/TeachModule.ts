/**
 * 教程模块
 */
class TeachModule extends how.module.TSceneModule<TeachView, any> {
    public static request: any = {
        onTeachButton: "onTeachButton",
        onGotoButton:"onGotoButton"
    }
    public onTeachButton(): void {
        this.moduleManager.initModule(TeachWindowModule, TeachWindowView);
    }
    public onGotoButton(): void {
        this.moduleManager.initModule(MainModule, MainView);
    }
}