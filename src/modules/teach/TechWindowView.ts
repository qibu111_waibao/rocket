/**
 * 教程窗口界面
 * @author 袁浩
 */
class TeachWindowView extends base.Window {
    public teachLabel: eui.Label;
    public constructor() {
        super();
        this.skinName = "TeachWindowSkin";
    }
    public start(): void {
        this.teachLabel.text = RES.getRes("Text_json").teach;
    }
}