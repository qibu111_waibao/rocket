/**
 * 教程界面
 * @author 袁浩
 */
class TeachView extends base.Scene {
    public helloButton: eui.Button;
    public teachButton: eui.Button;
    public gotoButton: eui.Button;
    public cunzhang: eui.Image;
    public constructor() {
        super();
        this.skinName = "TeachSkin";
    }
    public awake(): void {
        this.playCunzhangTween();
        this.teachButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTeachButton, this);
        this.gotoButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGotoButton, this);
    }
    private playButtonsTween(button: eui.Button, index: number): void {
        egret.Tween.get(button).wait(index * GameConfig.cunzhangButtonsTime).to({ alpha: 1 }, GameConfig.cunzhangButtonsTime);
    }
    private playCunzhangTween(): void {
        this.helloButton.alpha = 0;
        this.teachButton.alpha = 0;
        this.gotoButton.alpha = 0;
        this.cunzhang.verticalCenter = this.stage.stageHeight / 2 + this.cunzhang.height / 2;
        egret.Tween.get(this.cunzhang).to({ verticalCenter: 0 }, 600, egret.Ease.circOut).call(() => {
            this.playButtonsTween(this.helloButton, 0);
            this.playButtonsTween(this.teachButton, 1);
            this.playButtonsTween(this.gotoButton, 2);
        }, this);
    }
    private onTeachButton(): void {
        this.report("onTeachButton");
    }
    private onGotoButton(): void {
        egret.Tween.removeAllTweens();
        this.helloButton.alpha = 0;
        this.teachButton.alpha = 0;
        this.gotoButton.alpha = 0;
        egret.Tween.get(this.cunzhang).to({ verticalCenter: -this.stage.stageHeight / 2 - this.cunzhang.height / 2 }, 600, egret.Ease.circIn).call(() => {
            this.report("onGotoButton");
        }, this);
    }
    public onDestroy(): void {
        super.onDestroy();
    }
}