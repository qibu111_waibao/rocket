/**
 * 主界面模块
 */
class WelcomeModule extends how.module.TSceneModule<WelcomeView, any> {
    public static request: any = {
        onStartButton: "onStartButton"
    }
    public onStartButton(): void {
        this.moduleManager.initModule(TeachModule, TeachView);
    }
}