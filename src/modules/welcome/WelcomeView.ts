/**
 * 欢迎界面
 * @author 袁浩
 */
class WelcomeView extends base.Scene {
    public startButton: eui.Button;
    public constructor() {
        super();
        this.skinName = "WelcomeSkin";
    }
    public start(): void {
        this.startButton.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStartButton, this);
    }
    public onStartButton(): void {
        this.report("onStartButton");
    }
}