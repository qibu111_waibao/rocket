/**
 * 实验室第二个游戏界面
 */
class LabGame1View extends base.Scene {
    public chazuoGroup: eui.Group;
    public dengpaoGroup: eui.Group;
    public get resourceList(): Array<string> {
        return ["labGame1"];
    }
    public constructor() {
        super();
        this.skinName = "LabGame1Skin";
    }
    public awake(): void {
        this.initChazuo();
        this.initDengpao();
        this.stage.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onTouchMove, this);
        this.stage.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchEnd, this);
        this.checkIsWin();
    }
    private initChazuo(): void {
        var data = [1, 2, 3, 4, 0, 0];
        while (data.length) {
            var image = this.chazuoGroup.getChildAt(6 - data.length) as eui.Image;
            var index = data.splice(Math.floor(how.Random.range(0, data.length)), 1)[0];
            image.source = "插座" + index + "_png";
            image.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onImageBegin, this);
            mouse.setButtonMode(image, true);
            if (index == 0) {
                image.y = 175;
            }
            else {
                image.y = 0;
            }
        }
    }
    private initDengpao(): void {
        var data = [1, 2, 3, 4, 0, 0];
        while (data.length) {
            var group = this.dengpaoGroup.getChildAt(6 - data.length) as eui.Group;
            var image = group.getChildAt(0) as eui.Image;
            var index = data.splice(Math.floor(how.Random.range(0, data.length)), 1)[0];
            if (index != 0) {
                image.source = "灯泡_没亮" + index + "_png";
            }
            else {
                image.source = "";
            }
        }
    }
    private dragImgData: { img: eui.Image, imagePoint: egret.Point, stagePoint: egret.Point };
    private onImageBegin(event: egret.TouchEvent): void {
        var image = event.currentTarget as eui.Image;
        var imagePoint = image.localToGlobal();
        this.addChild(image);
        image.x = imagePoint.x;
        image.y = imagePoint.y;
        this.dragImgData = { img: image, imagePoint: imagePoint, stagePoint: new egret.Point(event.stageX, event.stageY) };
        egret.Tween.removeTweens(image);
    }
    private onTouchMove(event: egret.TouchEvent): void {
        if (this.dragImgData) {
            var offsetX = event.stageX - this.dragImgData.stagePoint.x;
            // var offsetY = event.stageY - this.dragImgData.stagePoint.y;
            this.dragImgData.img.x = this.dragImgData.imagePoint.x + offsetX;
            // this.dragImgData.img.y = this.dragImgData.imagePoint.y + offsetY;
        }
    }
    private onTouchEnd(event: egret.TouchEvent): void {
        if (this.dragImgData) {
            this.checkPoint();
        }
        this.dragImgData = null;
    }
    private checkPoint(): void {
        var dragImg: eui.Image = this.dragImgData.img;
        var minDistance = Number.MAX_VALUE;
        var minImage: eui.Image;
        var oldPoint = this.chazuoGroup.globalToLocal(this.dragImgData.imagePoint.x, this.dragImgData.imagePoint.y);
        var nowPoint = new egret.Point(dragImg.x, dragImg.y + dragImg.height);
        this.chazuoGroup.addChild(dragImg);
        dragImg.x = oldPoint.x;
        dragImg.y = oldPoint.y;
        for (var i = 0; i < this.chazuoGroup.numChildren; i++) {
            var image = this.chazuoGroup.getChildAt(i) as eui.Image;
            var globalPoint = image.localToGlobal();
            globalPoint.y += image.height;
            var distance = egret.Point.distance(globalPoint, nowPoint);
            if (distance < minDistance) {
                minImage = image;
            }
            minDistance = distance < minDistance ? distance : minDistance;
        }
        dragImg.x = minImage.x;
        minImage.x = oldPoint.x;
        dragImg.y = dragImg.source == "插座0_png" ? 175 : 0;
        minImage.y = minImage.source == "插座0_png" ? 175 : 0;
        this.checkIsWin();
    }
    private checkIsWin(): void {
        var chazuos: eui.Image[] = [];
        for (var i = 0; i < this.chazuoGroup.numChildren; i++) {
            chazuos.push(this.chazuoGroup.getChildAt(i) as eui.Image);
        }
        chazuos.sort((a: eui.Image, b: eui.Image): number => {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x > b.x) {
                return 1;
            }
            else {
                return 0;
            }
        });
        var successCount: number = 0;
        for (var i = 0; i < this.dengpaoGroup.numChildren; i++) {
            var group = this.dengpaoGroup.getChildAt(i) as eui.Group;
            var image = group.getChildAt(0) as eui.Image;
            var source = image.source.toString();
            if (image.source) {
                var index = source.substring(source.indexOf("亮") + 1, source.lastIndexOf("_"));
                if (chazuos[i].source == "插座" + index + "_png") {
                    image.source = "灯泡_亮" + index + "_png";
                    successCount++;
                }
                else {
                    image.source = "灯泡_没亮" + index + "_png";
                }
            }
        }
        if (successCount == 4) {
            this.win();
        }
    }
    private loss(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame1Loss", "游戏2失败", 0x3f0c3f);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 1, 1, false);
        }, this);
        this.stopTime();
    }
    private win(): void {
        egret.Tween.removeAllTweens();
        var mc = this.createFullMc("labGame1Win", "游戏2成功", 0x3f0c3f);
        mc.x -= 114;
        mc.y -= 152;
        mc.addEventListener(egret.Event.COMPLETE, () => {
            this.report("onGameOver", 1, 1, true);
        }, this);
        this.stopTime();
    }
    protected onTimeOver(): void {
        this.loss();
    }
}