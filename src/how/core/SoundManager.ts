module how {
    /**
     * 声音管理器
     * 必须要预加载所有声音文件
     */
    export class SoundManager {
        private static music: egret.SoundChannel;
        private static effectList: egret.SoundChannel[] = [];
        private static _musicVolume: number = 1;
        public static get musicVolume(): number {
            return this._musicVolume;
        }
        private static currentMusicSource: string;
        public static set musicVolume(value: number) {
            this._musicVolume = value;
            if (how.Utils.isIOSNative()) {
                // if (this.musicVolume > 0 && this.currentMusicSource) {
                //     this.playMusic(this.currentMusicSource);
                //     // if (window["webkit"]) {
                //     //     window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playMusic", 2: "bg_mp3" }));
                //     // }
                //     // else {
                //     //     window["sound"]("playMusic", "bg_mp3");
                //     // }
                // }
                // else {
                //     this.stopMusic();
                //     // if (window["webkit"]) {
                //     //     window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "stopMusic", 2: "" }));
                //     // }
                //     // else {
                //     //     window["sound"]("stopMusic");
                //     // }
                // }
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "setVolume", 2: value }));
                }
                else {
                    window["sound"]("setVolume", value);
                }
            }
            else if (this.music) {
                this.music.volume = value;
            }
        }
        private static _effectVolume: number = 1;
        public static get effectVolume(): number {
            return this._effectVolume;
        }
        public static set effectVolume(value: number) {
            this._effectVolume = value;
            for (var i: number = 0; i < this.effectList.length; i++) {
                this.effectList[i].volume = value;
            }
        }
        private static musicPostion: number;
        private static musicSource: string;
        private static musicLoop: boolean;
        private static isInited: boolean;
        private static init(): void {
            if (!this.isInited) {
                this.isInited = true;
                EventManager["getInstance"](this).addEventListener(Application.APPEVENT_PAUSE, this.onAppPause, this);
                EventManager["getInstance"](this).addEventListener(Application.APPEVENT_RESUME, this.onAppResume, this);
            }
        }
        private static onAppPause(): void {
            if (egret.Capabilities.isMobile && !how.Utils.isIOSNative()) {
                if (this.music) {
                    this.musicPostion = this.music.position;
                    this.music.stop();
                    this.music = null;
                }
                this.stopAllEffects();
            }
        }
        private static onAppResume(): void {
            if (egret.Capabilities.isMobile && !how.Utils.isIOSNative()) {
                if (this.musicSource) {
                    this.playMusic(this.musicSource, this.musicLoop, this.musicPostion);
                }
            }
        }
        /**
         * 播放音乐，建议用mp3格式
         * @param source 相对于resource的音乐资源路径
         * @param loop 是否循环播放
         */
        public static playMusic(source: string, loop: boolean = true, startTime: number = 0): void {
            this.init();
            this.currentMusicSource = source;
            if (how.Utils.isIOSNative()) {
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playMusic", 2: source }));
                }
                else {
                    window["sound"]("playMusic", source);
                }
            }
            else {
                if (this.music) {
                    this.music.stop();
                }
                this.musicSource = source;
                this.musicLoop = loop;
                var sound: egret.Sound = RES.getRes(source);
                if (sound) {
                    sound.type = egret.Sound.MUSIC;
                    this.music = sound.play(startTime, loop ? 0 : 1);
                    this.music.volume = this._musicVolume;
                    this.music.once(egret.Event.SOUND_COMPLETE, (event: Event): void => {
                        this.music = null;
                    }, this);
                }
                else {
                    RES.getResAsync(source, (data, key) => {
                        data.type = egret.Sound.MUSIC;
                        if (this.music) {
                            this.music.stop();
                        }
                        this.music = data.play();
                        this.music.volume = this.musicVolume;
                    }, this);
                }
            }
        }
        /**
         * 停止播放音乐
         */
        public static stopMusic(): void {
            if (how.Utils.isIOSNative()) {
                if (window["webkit"]) {
                    window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "stopMusic", 2: "" }));
                }
                else {
                    window["sound"]("stopMusic");
                }
            }
            else {
                this.musicSource = null;
                this.music.stop();
                this.music = null;
            }
        }
        /**
         * 播放音效，建议用mp3格式
         * @param source 相对于resource的音效资源路径
         * @param loop 是否循环播放
         */
        public static playEffect(source: string, loop: boolean = false): void {
            if (how.Utils.isIOSNative()) {
                if (this.effectVolume) {
                    if (window["webkit"]) {
                        window["webkit"].messageHandlers.sound.postMessage(JSON.stringify({ 1: "playSound", 2: source }));
                    }
                    else {
                        window["sound"]("playSound", source);
                    }
                    return;
                }
            }
            else {
                this.init();
                var sound: egret.Sound = RES.getRes(source);
                if (sound) {
                    sound.type = egret.Sound.EFFECT;
                    var effect: egret.SoundChannel = sound.play(0, loop ? 0 : 1);
                    effect.volume = this._effectVolume;
                    this.effectList.push(effect);
                    effect.once(egret.Event.SOUND_COMPLETE, (event: Event): void => {
                        var index = this.effectList.indexOf(effect);
                        if (index != -1) {
                            this.effectList.splice(index, 1);
                        }
                    }, this);
                }
                else {
                    RES.getResAsync(source, (data, key) => {
                        if (data) {
                            data.type = egret.Sound.EFFECT;
                            var effect: egret.SoundChannel = data.play(0, loop ? 0 : 1);
                            effect.volume = this._effectVolume;
                            this.effectList.push(effect);
                            effect.once(egret.Event.SOUND_COMPLETE, (event: Event): void => {
                                var index = this.effectList.indexOf(effect);
                                if (index != -1) {
                                    this.effectList.splice(index, 1);
                                }
                            }, this);
                        }
                    }, this);
                }
            }
        }
        /**
         * 停止播放所有音效
         */
        public static stopAllEffects(): void {
            while (this.effectList.length) {
                this.effectList.shift().stop();
            }
        }
    }
}