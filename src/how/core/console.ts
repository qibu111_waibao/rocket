var useConsoleLog: boolean = true;
if(egret.Capabilities.runtimeType == egret.RuntimeType.WEB) {
    window.onerror = function(msg,path,line) {
        how.Log.error(msg);
        how.Log.error("    at " + path + ":" + line);
    }
}
/**
 * 快捷的打印消息到控制台
 * @param message {any} 要打印的数据
 */
function trace(message: any): string {
    if(!useConsoleLog) {
        return;
    }
    var msg: any = message;
    if(msg instanceof egret.Rectangle) {
        msg = "{ x=" + msg.x + ",y=" + msg.y + ",width=" + msg.width + ",height=" + msg.height + " }";
    }
    else if(msg instanceof egret.Point) {
        msg = "{ x=" + msg.x + ",y=" + msg.y + " }";
    }
    else if(Array.isArray(msg)) {
        console.log(msg);
        how.Log.trace(msg);
    }
    else if(typeof (message) == "object") {
        try {
            console.log(JSON.stringify(msg));
            how.Log.trace(msg);
        }
        catch(e) {
            console.log(msg);
            how.Log.trace(msg);
        }
    }
    else {
        console.log(msg);
        how.Log.trace(msg);
    }
    return msg;
}
/**
* 快捷的抛错误消息到控制台
* @param error {any} 要抛出的错误
*/
function error(error: any): string {
    if(!useConsoleLog) {
        return;
    }
    console.error(error);
    how.Log.error(error);
    return error;
}
/**
 * 快捷的抛出警告消息到控制台
 */
function warn(warn: any): string {
    if(!useConsoleLog) {
        return;
    }
    console.warn(warn);
    how.Log.warn(warn);
    return warn;
}