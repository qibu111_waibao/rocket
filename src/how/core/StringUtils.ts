module how {
	/**
	 * 字符串工具类
	 * @author 袁浩
	 *
	 */
    export class StringUtils {
        public constructor() {
        }
		/**
		 * 类似C#中的格式化字符串函数
		 * 例：format("Hello {0}",world)
		 * @param str {string} 要格式化的字符串
		 * @param args {Array<any>} 参数列表
		 * @returns {string} 格式化之后的字符串
		 */
        public static format(str: string, ...args: Array<any>): string {
            var result: string = str;
            for (var i: number = 0; i < args.length; i++) {
                result = result.replace("{" + i + "}", args[i].toString());
            }
            return result;
        }
        /**
         * 判断是否是空字符串，null、undefined和""都会返回true
         * @returns {boolean} 是否是空字符串
         */
        public static isEmpty(value: string): boolean {
            return value == null || value == undefined || value.length == 0;
        }
        /**
         * 去左右两端空格
         */
        public static trim(str): string {
            return str.replace(/(^\s*)|(\s*$)/g, "");
        }
        /**
          * 白鹭专用字符串转富文本
          * 要求格式：
          * @parma str {string} 传入的字符串
          * @parma type {number} 消息类型 4：系统消息 8：玩家喊话
          * [#去掉“0x”的六位颜色值（16进制）]文本  例如：[#F7E1C2]欢迎来到《米乐汇德州扑克》的游戏世界！
          */
        public static textToRichText(str: string, type: number): Array<egret.ITextElement> {
            var reStr = <Array<egret.ITextElement>>[];
            if (type == 4) {                                                                                 //判断是否是系统消息
                if (str == null) {
                    return;
                }
                if (str.indexOf("[") != -1) {                                                                //查找是否有“]” 有则为富文本消息
                    var strArr = str.split("[");                                                            //划分字符串转出字串组
                    for (var i = 0; i < strArr.length; i++) {                                                   //遍历字串组
                        if (i == 0) {                                                                         //字串组第一个位置必定是普通字符串通常为空
                            reStr.push({ text: strArr[i], style: {} });                                       //直接加人返回富文本数组
                        } else {
                            var color: string;
                            var nowtext: string;
                            if (strArr[i].charAt(0) == "#") {                                                //如果[]内第一个字符为# 则后面6位为十六进制色值
                                color = "0x" + strArr[i].substr(1, 6);
                                nowtext = strArr[i].substr(9);                                              //从第九位开始为正式文本内容
                            }
                            reStr.push({ text: nowtext, style: { "textColor": Number(color) } });            //将整理好的富文本加人返回富文本数组
                        }
                    }
                }
                else {                                                                                    //查找是否有“]” 没有则为普通文本消息
                    reStr.push({ text: str, style: {} });                                                    //直接加人返回富文本数组
                }
            }
            else {                                                                                        //此处为玩家喊话  暂未处理
                reStr.push({ text: str, style: {} });
            }
            return reStr;
        }
    }
}
