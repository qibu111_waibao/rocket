module how {
	/**
	 * 声音工具类
	 * @author 袁浩
	 *
	 */
    export class SoundUtils {
        public constructor() {
        }
		/**
		 * 音效音量
		 */
        public static effectVolume: number = 1;
        private static _musicVolume: number = 1;
        /**
        * 音乐音量
        */
        public static get musicVolume(): number {
            return this._musicVolume;
        }
        public static set musicVolume(value: number) {
            this._musicVolume = value;
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                egret.ExternalInterface.call("playMusic", "||" + this.musicVolume);
                return;
            }
            if (this.music) {
                this.music.volume = this.musicVolume;

            }
        }
        private static music: egret.SoundChannel;
        /**
         * 播放音乐，更换音乐需要先暂停
         */
        public static playMusic(path: string): void {
            // this.continueMusic();
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                var rsi: RES.ResourceItem = how.Application.resourceConfig.getResourceItem(path);
                if (!rsi) {
                    return;
                }
                var url: string = rsi.url;
                var argpath: string = how.Application.resourceVersion.getVirtualUrl(url);
                egret.ExternalInterface.call("playMusic", argpath + "||" + this.musicVolume);
                return;
            }
            if (RES.hasRes(path)) {
                var sound: egret.Sound = RES.getRes(path);
                if (sound) {
                    sound.type = egret.Sound.MUSIC;
                    this.music = sound.play();
                    this.music.volume = this.musicVolume;
                }
                else {
                    RES.getResAsync(path, (data, key) => {
                        data.type = egret.Sound.MUSIC;
                        if (this.music) {
                            this.music.stop();
                        }
                        this.music = data.play();
                        this.music.volume = this.musicVolume;
                    }, this);
                }
            }
            else {
                var sound: egret.Sound = new egret.Sound();
                sound.type = egret.Sound.MUSIC;
                sound.once(egret.Event.COMPLETE, (event: egret.Event) => {
                    sound.type = egret.Sound.MUSIC;
                    this.music = sound.play();
                    this.music.volume = this.musicVolume;
                }, this);
                sound.load(path);
            }
        }
        public static stopMusic(): void {
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                egret.ExternalInterface.call("playMusic", "||" + this.musicVolume);
                return;
            }
            if (!this.music) {
                return;
            }
            this.music.stop();
            this.music = null;
        }
        private static initSound(path: string, type: string) {
            var sound: egret.Sound = RES.getRes(path);
            if (!sound) {
                return;
            }
            sound.type = type;
            if (type == egret.Sound.MUSIC) {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    var rsi: RES.ResourceItem = how.Application.resourceConfig.getResourceItem(path);
                    if (!rsi) {
                        return;
                    }
                    var url: string = rsi.url;
                    var argpath: string = how.Application.resourceVersion.getVirtualUrl(url);
                    egret.ExternalInterface.call("playMusic", argpath + "||" + this.musicVolume);
                }
                else {
                    this.music = sound.play();
                    this.music.volume = this.musicVolume;
                }
            }
            else {
                if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                    var rsi: RES.ResourceItem = how.Application.resourceConfig.getResourceItem(path);
                    if (!rsi) {
                        return;
                    }
                    var url: string = rsi.url;
                    var argpath: string = how.Application.resourceVersion.getVirtualUrl(url);
                    egret.ExternalInterface.call("playSound", argpath + "||" + this.effectVolume);
                }
                else {
                    sound.play(0, 1).volume = this.effectVolume;
                }
            }
        }
        public static playEffect(path: string): void {
            if (egret.Capabilities.runtimeType == egret.RuntimeType.NATIVE) {
                var rsi: RES.ResourceItem = how.Application.resourceConfig.getResourceItem(path);
                if (!rsi) {
                    return;
                }
                var url: string = rsi.url;
                var argpath: string = how.Application.resourceVersion.getVirtualUrl(url);
                egret.ExternalInterface.call("playSound", argpath + "||" + this.effectVolume);
                return;
            }
            if (RES.hasRes(path)) {
                if (RES.getRes(path)) {
                    this.initSound(path, egret.Sound.EFFECT);
                }
                else {
                    RES.getResAsync(path, (data, key) => {
                        this.initSound(path, egret.Sound.EFFECT);
                    }, this);
                }
            }
            else {
                var sound: egret.Sound = new egret.Sound();
                sound.type = egret.Sound.EFFECT;
                sound.once(egret.Event.COMPLETE, (event: egret.Event) => {
                    sound.type = egret.Sound.EFFECT;
                    sound.play().volume = this.effectVolume;
                }, this);
                sound.load(path);
            }
        }

        private static _pauseVolume: number = -1;
        //进入游戏场暂停音乐
        public static pauseMusic(): void {
            this._pauseVolume = this.musicVolume;
            this.musicVolume = 0;
        }

        //进入主场景继续音乐
        public static continueMusic(): void {
            if (this._pauseVolume != -1) {
                this.musicVolume = this._pauseVolume;
                this._pauseVolume = -1;
            }
        }
    }
}