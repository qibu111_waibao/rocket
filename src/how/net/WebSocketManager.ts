module how {
	/**
	 * WebSocket管理器，收发websocket消息全部用这个类
	 * 接受消息用how.EventManager，因为用WebSocketManager是无效的
	 * 因为how.EventManager包含dispatcher而WebSocketManager没有，WebSocketManager是无法抛出事件的
	 * @author 袁浩
	 *
	 */
    export class WebSocketManager {
        private static _instance: WebSocketManager;
        private static _beginTimer: number;
        /**
         * WebSocket管理器
         */
        public constructor() {
            WebSocketManager._beginTimer = egret.getTimer();
            this.resetWebSocket();
        }
        /**
         * 重置websocket，之前请先断开连接
         */
        public resetWebSocket(): void {
            if (this.ws) {
                this.ws.removeEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this);
                this.ws.removeEventListener(egret.Event.CONNECT, this.onSocketOpen, this);
                this.ws.removeEventListener(egret.Event.CLOSE, this.onSocketClose, this);
                this.ws.removeEventListener(egret.IOErrorEvent.IO_ERROR, this.onSocketClose, this);
            }
            this.ws = new egret.WebSocket();
            this.ws.addEventListener(egret.ProgressEvent.SOCKET_DATA, this.onReceiveMessage, this);
            this.ws.addEventListener(egret.Event.CONNECT, this.onSocketOpen, this);
            this.ws.addEventListener(egret.Event.CLOSE, this.onSocketClose, this);
            this.ws.addEventListener(egret.IOErrorEvent.IO_ERROR, this.onSocketClose, this);
        }
        /**
         * WebSocket管理器单例
         */
        public static getInstance(): WebSocketManager {
            if (!this._instance) {
                this._instance = new WebSocketManager();
            }
            return this._instance;
        }
        private ws: egret.WebSocket;
        private _isConnecting: boolean = false;
        private _connected: boolean = false;
        /** 
        * 是否打印消息日志
        **/
        public useLog: boolean = true;
        /** 
        * 是否正在连接
        **/
        public get isConnecting(): boolean {
            return this._isConnecting;
        }
        /** 
        * 是否已经连接
        **/
        public get connected(): boolean {
            return this._connected;
        }

        /**
         * 重连间隔
         */
        private _connectSign: number = 0;
        private _intervalHost: string = "";
        private _intervalPort: number = 0;
        /** 
        * 连接到指定的主机和端口
        * @param host {string} 主机ip或者域名
        * @param port {number} 主机端口号
        **/
        public connect(host: string, port: number): void {
            if (this._connectSign != 0) {
                this._intervalHost = host;
                this._intervalPort = port;
                return;
            }

            this._connectSign = egret.setTimeout(this.onReconnect, this, 2000);

            if (!this._connected && !this._isConnecting) {
                this.resetWebSocket();
                this._isConnecting = true;
                this._connected = false;
                this.ws.connect(host, port);
                trace("准备连接网络...");
            }
        }

        //网络多次重连间隔
        private onReconnect(): void {
            this._connectSign = 0;
            if (this._intervalHost != "") {//如果存在保存的主机端口则connect
                this.connect(this._intervalHost, this._intervalPort);
                this._intervalHost = "";
            }
        }
        /** 
        * 关闭连接
        **/
        public close(): void {
            if (this.ws.connected) {
                this.ws.close();
                this._isConnecting = false;
                this._connected = false;
                // this.resetWebSocket();
            }
        }
        /** 
        * 发送数据给服务器
        * @param cmd {string} 命令
        * @param host {any} 数据
        **/
        public send(cmd: string, data?: any) {
            if (this.ws.connected) {
                data = data || {};
                var m = parseInt(cmd.split(",")[0]);
                var s = parseInt(cmd.split(",")[1]);
                var d = { m: m, s: s, d: data };
                if (this.useLog) {
                    trace("发送：" + d.m + "," + d.s + " 数据：" + JSON.stringify(data) + "  time：" + (egret.getTimer() - WebSocketManager._beginTimer));
                }
                this.ws.writeUTF(JSON.stringify(d));
                this.ws.flush();
            }
            else {
                trace("网络已断开，无法再发送消息");
                how.EventManager["getInstance"](this).dispatchEvent(egret.Event.CLOSE);
            }
        }

        private onReceiveMessage(event: egret.ProgressEvent): void {
            var result: string = this.ws.readUTF();
            //特判
            if (result.indexOf("43") != -1 && result.indexOf("106") != -1 && result.indexOf("ChipReqInfo") != -1) {
                result = result.substring(0, result.length - 4) + "]}}";
            }
            var resultData: any = JSON.parse(result);
            how.EventManager["getInstance"](this).dispatchEvent(event.type, result);
            if (this.useLog) {
                trace("收到：" + resultData.m + "," + resultData.s + " 数据：" + JSON.stringify(resultData.d) + "  time：" + (egret.getTimer() - WebSocketManager._beginTimer));
            }
            if (how.Utils.isArray(resultData.d) && (Application.version == "1.0.0" || Application.version == "1.0.0.0")) {
                warn("消息：resultData.m" + "," + resultData.s + "的数据接口为数组，在1.0.0.0版本中可能有BUG，请检查调试。");
            }
            how.EventManager["getInstance"](this).dispatchEvent(resultData.m + "," + resultData.s, resultData.d);
        }
        private onSocketOpen(event: egret.Event): void {
            this._isConnecting = false;
            this._connected = true;
            trace("网络已连接");
            how.EventManager["getInstance"](this).dispatchEvent(event.type);
        }
        private onSocketClose(event: egret.Event): void {
            if (this._connected || this._isConnecting) {
                this._isConnecting = false;
                this._connected = false;
                trace("网络已断开");
                how.EventManager["getInstance"](this).dispatchEvent(egret.Event.CLOSE);
                //                this.resetWebSocket();
            }
        }
    }
}
