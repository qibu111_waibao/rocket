module how.module {
    /**
    * 场景模块
    * @author 袁浩
    *
    */
    export class SceneModule extends Module {
        private removeAllPops: boolean;
        private cancelAllHttps: boolean;
        /**
         * 模块标签，用来区分模块类型
         */
        public tag: number = 1;
        /**
         * 场景模块
         * @param guiClass 模块类
         * @param dataClass 模块数据
         * @param removeAllPops 切换的时候是否移除所有的弹出层，默认为false
         * @param cancelAllHttps 切换的时候是否移除所有http请求
         */
        public constructor(guiClass: any = null,dataClass: any = null,removeAllPops: boolean = false,cancelAllHttps: boolean = true) {
            super(guiClass,dataClass);
            this.removeAllPops = removeAllPops;
            this.cancelAllHttps = cancelAllHttps;
        }
        /**
        * 初始化模块
        */
        public init(): void {
            super.init();
            Application.changeScene(this.gui,this.removeAllPops,this.cancelAllHttps);
        }
        private isBacked: boolean;
        /**
         * 当按下返回时，如果没有重写readyBack，则默认会初始化上一个模块
         */
        public onBack(): void {
            if(this.isBacked) {
                return;
            }
            if(WindowManager.getInstance().windowCount == 0) {
                if(this.readyBack()) {
                    this.isBacked = true;
                    this.moduleManager.initPreviousModule(this.tag);
                }
                else {
                    this.isBacked = false;
                }
            }
            else {
                how.WindowManager.getInstance().closeLast();
            }
        }
        /**
         * 当准备返回时，可以重写此函数并返回false来实现返回事件的拦截
         */
        protected readyBack(): boolean {
            return true;
        }
        /**
        * 移除模块
        */
        public destroy(): void {
            super.destroy();
            //            Application.changeScene(null,this.removeAllPops,this.cancelAllHttps);
        }
    }
}
