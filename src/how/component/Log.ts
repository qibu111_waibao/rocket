module how {
	/**
	 * 日志界面
	 * @author 袁浩
	 *
	 */
    export class Log extends eui.UILayer {
        private _label: eui.Label;
        /**
         * 日志普通文本
         */
        public get label(): eui.Label {
            return this._label;
        }
        public set label(value: eui.Label) {
            this._label = value;
        }
        private _htmlText: string = "";
        /**
         * 日志当前的html文本
         */
        public get htmlText(): string {
            return this._htmlText;
        }
        /**
         * HtmlTextParser解析最大行数
         */
        public maxLength: number = 40;
        public set htmlText(value: string) {
            this._htmlText = value;
            var textElementList: Array<egret.ITextElement> = (new egret.HtmlTextParser).parser(value);
            if(textElementList.length >= this.maxLength) {
                textElementList = textElementList.slice(textElementList.length - this.maxLength);
            }
            this.label.textFlow = textElementList;
        }
        public constructor() {
            super();
            this.touchChildren = this.touchEnabled = false;
            this.initChild();
        }
        private initChild(): void {
            var rect: eui.Rect = new eui.Rect();
            rect.fillColor = 0xFFFFFF;
            rect.fillAlpha = 0.2;
            rect.strokeWeight = 0;
            rect.percentWidth = rect.percentHeight = 100;
            this.addChild(rect);
            this._label = new eui.Label();
            this._label.wordWrap = true;
            this._label.bottom = 0;
            this._label.verticalAlign = egret.VerticalAlign.BOTTOM;
            this.addChild(this._label)
        }

        private static _useLog: boolean;
        public static get useLog(): boolean {
            return this._useLog;
        }
        /**
         * 隐藏/显示日志
         */
        public static set useLog(value: boolean) {
            if(!useConsoleLog) {
                return;
            }
            this._useLog = value;
            this.init();
            if(!value && this.log.stage) {
                this.log.parent.removeChild(this.log);
            }
            else if(value) {
                egret.MainContext.instance.stage.addChild(this.log);
            }
        }
        private static log: Log;
        private static init(): void {
            this.log = this.log || new Log();
            if(this.log.parent) {
                this.log.parent.setChildIndex(this.log,this.log.parent.numChildren);
            }
        }
        public static trace(message: any): void {
            this.init();
            this.log.htmlText += '<font color="#FFFFFF" size="18">' + message + '\n</font>';
        }
        public static warn(message: any): void {
            this.init();
            this.log.htmlText += '<font color="#ffff00" size="18">' + message + '\n</font>';
        }
        public static error(message: any): void {
            this.init();
            this.log.htmlText += '<font color="#ff0000" size="18">' + message + '\n</font>';
        }
    }
}
