module how {
    export class DateChooser extends eui.Component {
        public weekList: eui.List;
        public dayList: eui.List;
        private _selectedDate: Date;
        public get selectedDate(): Date {
            return this._selectedDate;
        }
        public set selectedDate(value: Date) {
            this._selectedDate = value;
            if (this.dayList) {
                this.onCreationComplete();
            }
        }
        /**
         * 一共有多少天
         */
        public get days(): number {
            return this.getDaysInMonth();
        }
        public constructor() {
            super();
            this._selectedDate = this._selectedDate || new Date();
            this.addEventListener(eui.UIEvent.CREATION_COMPLETE, this.onCreationComplete, this);
        }
        private onCreationComplete(): void {
            this.weekList.dataProvider = this.weekList.dataProvider || new eui.ArrayCollection(new Array(7));
            this.weekList.selectedIndex = this._selectedDate.getDay() == 0 ? 7 : this._selectedDate.getDay() - 1;
            this.weekList.touchEnabled = this.weekList.touchChildren = false;
            this.dayList.dataProvider = this.getDays();
        }
        /**
         * 刷新数据
         */
        public refresh(): void {
            this.onCreationComplete();
        }
        /**
         * 获取日期列表数据
         */
        protected getDays(): eui.ArrayCollection {
            var today = this._selectedDate.getDate();
            var fristWeek = this.getFirstWeek();
            fristWeek = fristWeek == 0 ? 6 : fristWeek - 1;
            var days: Date[] = new Array(42);
            var lastDayIndex = this.getDaysInMonth() + fristWeek - 1;
            for (var i = 0; i < 42; i++) {
                if (i < fristWeek || i > lastDayIndex) {
                    days[i] = null;
                }
                else {
                    days[i] = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth(), i - fristWeek + 1);
                }
            }
            return new eui.ArrayCollection(days);
        }
        /**
         * 获取当月第一天是周几
         */
        protected getFirstWeek(): number {
            var date = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth(), this._selectedDate.getDate());
            date.setDate(1);
            return date.getDay();
        }
        /**
         * 获取当月一共多少天
         */
        protected getDaysInMonth(): number {
            var date = new Date(this._selectedDate.getFullYear(), this._selectedDate.getMonth() + 1, this._selectedDate.getDate());
            date.setDate(0);
            return date.getDate();
        }
    }
}