module how {
	/**
	 * 序列帧动画组件，可加入到布局
	 * 官方提供的工具是egret.MovieClip类型的，当时感觉无法配合GUI来布局，所以这里提供了自定义的
	 * 目前看来，应该官方提供的也可以参与布局，尽管如此，动画组件还是用这个类，后续把官方支持集成进来就行
	 * 一轮播放完成会抛出egret.Event.COMPLETE事件
	 * @author 袁浩
	 *
	 */
    export class Animation extends eui.Image {
    	/**
    	 * 序列帧图集的配置文件
    	 */
        private _sourceJSON: string;
        public get sourceJSON(): string {
            return this._sourceJSON;
        }
        public set sourceJSON(value: string) {
            this._sourceJSON = value;
            this.getResources();
        }
        public sourceGroup: string = "";
        /**
         * 名称过滤，即图集中只有相关的名称资源才加入序列帧列表中
         */
        public nameReg: string = "demo\\d+_png";
        /**
         * 是否自动播放
         */
        public autoPlay: boolean = true;
        /**
         * 是否循环播放
         */
        public loop: boolean = true;
        /**
         * 动画帧频
         */
        public frameRate: number = 24;
        /**
         * 资源根目录
         */
        public static resourcePath = "resource";
        /**
         * 是否正在播放
         */
        public get isPlaying(): Boolean {
            return this._isPlaying;
        }
        private timerID: number;
        private frames: Array<string> = new Array<string>();
        private _currentFrame: number = -1;
        private completeEvent: egret.Event;
        private _isPlaying: Boolean = false;
        public constructor(sourceJSON?: string, nameReg?: string) {
            super();
            this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.onRemoved, this);
            this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAdded, this);
            this._sourceJSON = sourceJSON;
            this.nameReg = nameReg;//"demo\\d+_png"
        }
        private onRemoved(event: egret.Event): void {
            this.stop();
        }
        private onAdded(event: egret.Event): void {
            this.refresh();
        }
        public childrenCreated(): void {
            this.getResources();
        }
        private getResources(): void {
            if (this.sourceJSON) {
                var url: string = Animation.resourcePath + "/" + this.sourceJSON;
                RES.getResByUrl(url, this.onComplete, this);
            }
            else {
                var resourceList: RES.ResourceItem[] = RES.getGroupByName(this.sourceGroup);
                for (var i: number = 0; i < resourceList.length; i++) {
                    if (!this.nameReg || this.nameReg.length == 0 || new RegExp(this.nameReg).test(resourceList[i].name)) {
                        this.frames.push(resourceList[i].name);
                    }
                }
                this.frames.sort(
                    function (a: string, b: string): number {
                        var anum: number = parseInt(a.replace(/[^0-9]/ig, ""));
                        var bnum: number = parseInt(b.replace(/[^0-9]/ig, ""));
                        if (anum > bnum) {
                            return 1;
                        }
                        else if (anum < bnum) {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    });
            }
        }
        /**
         * 当图集资源加载完成时
         * @param data {string} 获取到的数据
         * @param url {string} 加载的地址
         * 
         */
        public onComplete(data: any, url: string): void {
            if (!data) {
                return;
            }
            this.frames = new Array<string>();
            var frames: Array<any> = data.frames;
            for (var key in frames) {
                if (!this.nameReg || this.nameReg.length == 0 || new RegExp(this.nameReg).test(key)) {
                    this.frames.push(key);
                }
            }
            this.frames.sort(
                function (a: string, b: string): number {
                    var anum: number = parseInt(a.replace(/[^0-9]/ig, ""));
                    var bnum: number = parseInt(b.replace(/[^0-9]/ig, ""));
                    if (anum > bnum) {
                        return 1;
                    }
                    else if (anum < bnum) {
                        return -1;
                    }
                    else {
                        return 0;
                    }
                });
            this.refresh();
        }
        /**
         * 播放动画
         */
        public play(): void {
            if (this._isPlaying) {//如果正在播放则先停止
                this.stop();
            }
            this.timerID = egret.setInterval(this.onInterval, this, 1000 / this.frameRate);
            this._isPlaying = true;
        }
        /**
         * 停止动画
         */
        public stop(): void {
            this._currentFrame = 0;
            egret.clearInterval(this.timerID);
            this._isPlaying = true;
        }
        /**
         * 帧动画当前所在帧帧
         */
        public get currentFrame(): number {
            return this._currentFrame;
        }
        /**
         * 刷新动画
         */
        public refresh(): void {
            this.stop();
            if (this.autoPlay) {
                this.onInterval();
                this.play();
            }
            else {
                this.source = this.frames[this.currentFrame];
            }
        }
        /**
         * 播放下一帧
         */
        public playNext(): void {
            this.onInterval();
        }
        private onInterval(): void {
            if (how.Utils.checkRealVisible(this)) {
                this.source = this.frames[this.currentFrame];
                this._currentFrame++;
                if (this._currentFrame == this.frames.length) {
                    if (this.loop) {
                        this._currentFrame = 0;
                    }
                    else {
                        this.stop();
                    }
                    if (!this.completeEvent) {
                        this.completeEvent = new egret.Event(egret.Event.ENDED);
                    }
                    this.dispatchEvent(this.completeEvent);//抛出一轮播放完成事件
                }
            }
        }
    }
}
