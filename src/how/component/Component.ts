module how {
	/**
	 * how组件工具类
	 * @author 袁浩
	 *
	 */
    export class ComponentUtils {

        /**
         * how组件工具类，提供批量初始化组件功能
         */
        public constructor() {
        }
        /**
         * 初始化how组件
         */
        public static init(alertSkin: string, dialogSkin: string, bannerSkin: string, noticeSkin: string, loaddingSkin: string, callBack?: () => void, thisObject: any = null): void {
            if (!how.HowMain.themeTogether) {
                var resourceLoader: ResourceLoader = new ResourceLoader();
                resourceLoader.loadGroups([alertSkin, dialogSkin, bannerSkin, noticeSkin, loaddingSkin], () => {
                    Alert.init(alertSkin);
                    Dialog.init(dialogSkin);
                    Banner.init(bannerSkin);
                    Notice.init(noticeSkin);
                    Loadding.init(loaddingSkin);
                    callBack.apply(thisObject);
                }, thisObject);
            }
            else {
                Alert.init(alertSkin);
                Dialog.init(dialogSkin);
                Banner.init(bannerSkin);
                Notice.init(noticeSkin);
                Loadding.init(loaddingSkin);
                callBack.apply(thisObject);
            }
        }
    }
}
