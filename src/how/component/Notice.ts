module how {
	/**
	 * 公告组件，用一个存在一定时间的横幅来显示重要的消息。
	 * 如果文字内容过长，会自左向右滚动，直到显示完毕。
	 * 一定时间会自动消失，如果期间有多条消息需要展示，那么会排队等待上一个消失，也没有交互产生。
	 * @author 王锡铜
	 *
	 */
    export class Notice extends eui.Component {
        private chatLabel: eui.Label;
        private masstype: number;
        private labelScroller: eui.Scroller;
        private message: string | Array<egret.ITextElement>;
        private okHandler: Function;
        private thisObject: any;
        private timeOut: number;
        public isShow: boolean;//是否正在播放
        public constructor(message: string | Array<egret.ITextElement>, okHandler?: Function, thisObject?: any, type: number = 8) {
            super();
            this.left = this.right = this.top = this.bottom = 0;
            this.skinName = how.Notice.skinName;
            this.message = message;
            this.okHandler = okHandler;
            this.thisObject = thisObject;
            this.masstype = type;
            this.touchEnabled = false;
            this.touchEnabled = this.touchChildren = false;
        }

        public childrenCreated(): void {
            // this.chatLabel.textFlow = how.StringUtils.textToRichText(this.message, this.masstype);
            if ((typeof this.message) == "string") {
                this.chatLabel.text = this.message as string;
            }
            else {
                this.chatLabel.textFlow = this.message as egret.ITextElement[];
            }
            this.timeStart();
        }
        /**
         * 计时开始
         */
        public timeStart(): void {
            this.isShow = true;
            // if (this.chatLabel.width > this.width - this.labelScroller.right - this.labelScroller.left) {//如果label的长度大于控件长度 则 使用tween
            var runTime = this.chatLabel.text.length * how.Notice.everyWordTime;//以字符数量计算滚动时间
            this.chatLabel.x = this.width - this.labelScroller.right - this.labelScroller.left;
            var tw = egret.Tween.get(this.chatLabel);
            tw.to({ x: -this.chatLabel.width }, runTime);
            this.timeOut = egret.setTimeout(function () {
                this.close();
            }, this, runTime);
            // } else {
            //     this.timeOut = egret.setTimeout(function () {
            //         this.close();
            //     }, this, how.Notice.delay);
            // }
        }
        /**
         * 计时结束
         */
        public close(): void {
            this.isShow = false;
            Application.closeWindow(this);
            egret.clearTimeout(this.timeOut);
            how.Notice.noticeList.shift();
            if (this.okHandler) {
                this.okHandler.apply(this.thisObject);
            }
        }

        private static delay: number;
        private static skinName: any;
        private static everyWordTime: number;
        /**
        * 初始化公告，在游戏启动的时候调用
        * @param skinName {string} 公告皮肤
        * @param delay {number} 无需滚动的情况下 Notice的持续时间
        * @param everyWordTime {number} 需要滚动的情况下 每个字符的时间
        */
        public static init(skinName: any, delay: number = 5000, everyWordTime: number = 500): void {
            this.skinName = skinName;
            this.delay = delay;
            this.everyWordTime = everyWordTime;
            this.noticeList = new Array<Notice>();
        }
        /**
         * Notice队列
         */
        public static noticeList: Array<Notice>;
        /**
         * 显示一个Notice
         * @param massage {string} 公告内容
         * @param massType {number} 消息类型 4：系统消息（优先级高）  8：喊话消息（优先级低）
         * @param okHandler {Function} 显示完成后的回调函数
         * @param thisObject {any} 上下文
         */
        public static show(massage: string | Array<egret.ITextElement>, massType: number = 8, okHandler?: Function, thisObject?: any): how.Notice {
            if (!this.skinName) {
                warn("Notic公告未初始化，将不会被显示，请先调用how.Notice.init()。");
            }
            if (massage == null) {
                return;
            }
            var notice: how.Notice = new how.Notice(massage, function () {
                if (okHandler) {
                    okHandler.apply(thisObject);
                }
                if (this.noticeList[0]) {  //如果队列中还有 则继续播放
                    var timeOut = egret.setTimeout(function () {
                        Application.addWindow(this.noticeList[0], false, false, false, false);
                    }, this, 500);
                }
            }, this, massType);
            //notice优先级判断
            if (massType == 8) {
                this.noticeList.push(notice);//notice插入队列末端
            } else {
                this.noticeList.splice(1, 0, notice);//notice插入队列前端
            }
            if (!this.noticeList[0].isShow) {//如果队列中第一个位置处的notice 没有正在播放  则播放  否则只添加进入队列
                Application.addWindow(this.noticeList[0], false, false, false, false);
            }
            return notice;
        }
    }
}
