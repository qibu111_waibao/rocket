/**
* 主类，游戏入口
* @author 袁浩
*/
class Main extends how.HowMain {
    public constructor() {
        var groups: Array<string> = ["preload"];
        super("zhajinhua", LoadingScene, "default.res.json", groups, 960, 640, LoaddingWindow);
        how.Animation.resourcePath = "resource/packs";
        //以下代码注释掉就可以在PC浏览器自适应无黑边了
        // if (!egret.Capabilities.isMobile) {
        //     egret.MainContext.instance.stage.scaleMode = egret.StageScaleMode.SHOW_ALL;
        // }
    }
    protected initSound(): void {
        this.once(egret.TouchEvent.TOUCH_TAP, () => {
            how.SoundManager.playEffect("windowopen_mp3");
            // how.SoundManager.stopAllEffects();
        }, this);
    }
    public start(): void {
        how.ComponentUtils.init("public.AlertSkin", "public.DialogSkin", "public.BannerSkin", "public.NoticeSkin", "public.LoaddingSkin", this.afterStart, this);//初始化通用组
    }
    public afterStart(): void {
        mouse.enable(this.stage);//初始化鼠标系统
        mouse.setMouseMoveEnabled(true);
        this.initSound();//初始化音乐
        this.initLocalStorage();//初始化本地数据
        this.stage.maxTouches = 1;//多点触摸禁止
        this.moduleManager.initModule(WelcomeModule, WelcomeView);
    }
    /*初始化本地数据，音乐，音效，震动，自动登录等*/
    private initLocalStorage(): void {
    }
    /**
    * 子类继承获取加载进度
    */
    protected onLoaddingProgress(percent: number, current: number, total: number): void {
        var loadingUI: LoadingScene = <LoadingScene>this.loadingUI;
        loadingUI.setProgress(percent, current, total);
    }
    /**
     * 当所有资源组加载完成
     */
    protected onAllGroupComplete(): void {
        var loadingUI: LoadingScene = <LoadingScene>this.loadingUI;
        // loadingUI.setText(LanguageConfig.logining);//初始化样式
    }
}